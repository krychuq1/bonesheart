(function() {
    'use strict';

    angular
        .module('sevenLottos', [
            'ngAnimate',
            'ngCookies',
            'ngSanitize',
            'ngMessages',
            'ngAria',
            'ngResource',
            'ui.router',
            'ngMaterial',
            'angular-svg-round-progressbar',
            'slickCarousel',
            'ngDialog',
            'insightWebAngular',
            'iso.directives',
            'md.data.table',
            'bt.owl-carousel',
            'tmh.dynamicLocale'
        ]);

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('SendMessageModalController', SendMessageModalController);

    /** @ngInject */
    function SendMessageModalController() {
        //var vm = this;
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('CallbackModalController', CallbackModalController);

    /** @ngInject */
    function CallbackModalController() {
        //var vm = this;
    }

})();

(function() {
    'use strict';

    WithdrawModalController.$inject = ["$scope", "$timeout", "$log"];
    angular
        .module('sevenLottos')
        .controller('WithdrawModalController', WithdrawModalController);

    /** @ngInject */
    function WithdrawModalController($scope, $timeout, $log) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            amount : '',
            instrument1 : '', //BIC
            instrument : '' //IBAN
        };

        /**
         * Functions in the controller
         */
        vm.requestWithdrawal = requestWithdrawal;

        /**
         * Saves the data from the defined fields above
         */
        function requestWithdrawal()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.withdrawForm.$valid)
            {
                $log.debug('withdraw form is valid')
                // Carry out the saving of the data
                
                // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
                vm.processing = false;
            }
            else
            {
                $log.debug('withdraw form is not valid')
                vm.processing = false;
            }
        }
    }

})();

(function() {
    'use strict';

    WagerLimitsModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('WagerLimitsModalController', WagerLimitsModalController);

    /** @ngInject */
    function WagerLimitsModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        /**
         * Fields in the modal
         */
        vm.fields = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        /**
         * The current values for a users limits
         */
        vm.defaults = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        }

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getWagerLimits()
                .then(getLimitsSuccess, getLimitsFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getLimitsSuccess(response) {
                vm.defaults = {
                    daily: response.daily.limit,
                    weekly: response.weekly.limit,
                    monthly: response.monthly.limit,
                    semiannual: response.semiannual.limit
                }
            }

            function getLimitsFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.wagerLimitForm.$valid)
            {
               $iwAccount.setWagerLimits(vm.fields).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('WagerLimitForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('wagerLimitForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal() {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
    'use strict';

    UpdatePasswordModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('UpdatePasswordModalController', UpdatePasswordModalController);

    /** @ngInject */
    function UpdatePasswordModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            oldPassword : '',
            newPassword : '',
            confirmPassword : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updatePasswordForm.$valid)
            {
                $log.debug('UpdatePassword form is valid')
                // Carry out the saving of the data
                
                $iwAccount.updatePassword(vm.fields.oldPassword, vm.fields.newPassword, vm.fields.confirmPassword).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateEmail Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdatePassword form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

    }

})();

(function() {
    'use strict';

    UpdateEmailModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('UpdateEmailModalController', UpdateEmailModalController);

    /** @ngInject */
    function UpdateEmailModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            email : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial email address
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getProfile()
                .then(getEmailSuccess, getEmailFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getEmailSuccess(response) {
                vm.fields = {
                    email: response.data.eMail
                }
            }

            function getEmailFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updateEmailForm.$valid)
            {
                // Carry out the saving of the data
                $iwAccount.updateEmail(vm.fields.email).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateEmail Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdateEmail form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
    'use strict';

    UpdateAddressModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('UpdateAddressModalController', UpdateAddressModalController);

    /** @ngInject */
    function UpdateAddressModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

		/**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            address : '',
            city 	: '',
            zip 	: '',
            phone   : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial email address
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getProfile()
                .then(getAddressSuccess, getAddressFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getAddressSuccess(response) {
                vm.fields = {
                    address : response.data.address,
                    city    : response.data.city,
                    zip     : response.data.zip,
                    phone   : response.data.phone
                }
            }

            function getAddressFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updateAddressForm.$valid)
            {
                $log.debug('UpdateAddress form is valid')
                // Carry out the saving of the data
                $iwAccount.updateProfile(vm.fields).then(
                    function(){
                        // The query was successful
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateAddress Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdateAddress form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
    'use strict';

    TransactionHistoryModalController.$inject = ["$iwAccount", "moment"];
    angular
        .module('sevenLottos')
        .controller('TransactionHistoryModalController', TransactionHistoryModalController);

    /** @ngInject */
    function TransactionHistoryModalController($iwAccount, moment) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        vm.transactions = [];

        /**
         * Grab the latest data
         */
        function init()
        {
            vm.processing = false;

            var startDate = moment().subtract(60, 'days').unix();
            var endDate = moment().unix();

            $iwAccount.getTransactionHistory(startDate, endDate)
                .then(transactionsSuccess, transactionsError)
                .finally(function() {
                    vm.processing = false;
                });

            function transactionsSuccess(response) {
                vm.transactions = response.transactionDetailsList;
            }

            function transactionsError(response) {
                
            }
        }

        init();

    }

})();

(function() {
    'use strict';

    SelfExclusionModalController.$inject = ["$scope", "$timeout", "$log"];
    angular
        .module('sevenLottos')
        .controller('SelfExclusionModalController', SelfExclusionModalController);

    /** @ngInject */
    function SelfExclusionModalController($scope, $timeout, $log) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        vm.cooloffs = [
            { name : "12 hrs", timePeriod : 12 },
            { name : "1 day", timePeriod : 1 },
            { name : "7 days", timePeriod : 7 },
            { name : "30 days", timePeriod : 30 },
            { name : "60 days", timePeriod : 60 },
            { name : "180 days", timePeriod : 180 }
        ];

        /**
         * Fields in the modal
         */
        vm.fields = {
            cooloffperiod : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.selfExclusionForm.$valid)
            {
                $log.debug('selfExclusion form is valid')
                // Carry out the saving of the data
                
                

                // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
                vm.processing = false;
            }
            else
            {
                $log.debug('selfExclusion form is not valid')
                vm.processing = false;
            }
        }

    }

})();

(function() {
    'use strict';

    PurchaseHistoryModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount", "moment"];
    angular
        .module('sevenLottos')
        .controller('PurchaseHistoryModalController', PurchaseHistoryModalController);

    /** @ngInject */
    function PurchaseHistoryModalController($scope, $timeout, $log, $iwAccount, moment) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        vm.purchases = [];

        /**
         * Grab the latest data
         */
        function init() {
            vm.processing = true;

            var startDate = moment().subtract(60, 'days');
            var endDate = moment();

            // Load data
            $iwAccount.getPurchaseHistory(startDate.format(), endDate.format())
                .then(fetchSuccess, fetchFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function fetchSuccess(response) {
                vm.purchases = response.data.response.playSlips;
            }

            function fetchFailed() {
                //$log.log('Failed', response);
            }
        }

        init();

    }

})();

(function() {
    'use strict';

    LossLimitsModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('LossLimitsModalController', LossLimitsModalController);

    /** @ngInject */
    function LossLimitsModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        /**
         * Fields in the modal
         */
        vm.fields = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        /**
         * The current values for a users limits
         */
        vm.defaults = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        }

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getLossLimits()
                .then(getLimitsSuccess, getLimitsFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getLimitsSuccess(response) {
                vm.defaults = {
                    daily: response.daily.limit,
                    weekly: response.weekly.limit,
                    monthly: response.monthly.limit,
                    semiannual: response.semiannual.limit
                }
            }

            function getLimitsFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.lossLimitForm.$valid)
            {
                $iwAccount.setLossLimits(vm.fields).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('LossLimitForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('lossLimitForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
    'use strict';

    DepositLimitsModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('DepositLimitsModalController', DepositLimitsModalController);

    /** @ngInject */
    function DepositLimitsModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        /**
         * Fields in the modal
         */
        vm.fields = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        /**
         * The current values for a users limits
         */
        vm.defaults = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getDepositLimits()
                .then(getLimitsSuccess, getLimitsFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getLimitsSuccess(response) {
                vm.defaults = {
                    daily: response.daily.limit,
                    weekly: response.weekly.limit,
                    monthly: response.monthly.limit,
                    semiannual: response.semiannual.limit
                }
            }

            function getLimitsFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.depositLimitForm.$valid)
            {
               $iwAccount.setDepositLimits(vm.fields).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('DepositLimitForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('depositLimitForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
    'use strict';

    DepositModalController.$inject = ["$scope", "$timeout", "$log", "$iwPayments", "$iwAccount", "$window"];
    angular
        .module('sevenLottos')
        .controller('DepositModalController', DepositModalController);

    /** @ngInject */
    function DepositModalController($scope, $timeout, $log, $iwPayments, $iwAccount, $window) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;


        vm.account = $iwAccount.getAccount();


        /**
         * Fields in the modal
         */
        vm.fields = {
            amount          : "",
            card_name       : "",
            card_number     : "",
            exp_month       : "",
            exp_year        : "",
            cvv             : ""
        };

        vm.months = ["01","02","03","04","05","06","07","08","09","10","11","12"];

        /**
         * Functions in the controller
         */
        vm.deposit = deposit;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = false;

            // Load Instruments goes here

        }

        /**
         * Saves the data from the defined fields above
         */
        function deposit()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.depositForm.$valid)
            {
                var request = {
                    amount: vm.fields.amount,
                    cardNumber: vm.fields.card_number,
                    expirationMonth: vm.fields.exp_month,
                    expirationYear: vm.fields.exp_year,
                    cvv: vm.fields.cvv,
                    firstName: vm.account.profile.firstName,
                    lastName: vm.account.profile.lastName,
                    redirectionURL: $window.location.href
                };

               $iwPayments.deposit('wirecard', request).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        $timeout(function() {
                            closeModal();
                        }, 1000);
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('DepositForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('depositForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();

(function() {
  'use strict';

  ResultsPageController.$inject = ["$log", "SingleLineProducts", "$filter", "$rootScope", "$mdMedia", "$state"];
  angular
    .module('sevenLottos')
    .controller('ResultsPageController', ResultsPageController);

  /** @ngInject */
  function ResultsPageController($log, SingleLineProducts, $filter, $rootScope,$mdMedia,$state) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;

    vm.results = [];

    //go to function
    vm.goTo = function (path,results) {
      var data = {
        name : results[0].lotteryName,
        history : results
      };
      //localStorage.setItem('historyItem',JSON.stringify(data));
      $state.go(path,data);
    };


    // Load the results from service
    SingleLineProducts.results().then(
      function success(response) {
        // Set results
        vm.results = response;
        $log.log(vm.results);
      },
      function error(response) {
        // vm.results = response;
        $log.log('Results error', response);
        //$log.log(vm.results);
      }
    );

    //format the date to be more readable
    vm.formatDate = function (/* lottery */) {
      //var date = new Date(lottery.nextDrawDateTimeList[lottery.nextDrawDateTimeList.length - 1]);
        //var yyyy = date.getFullYear().toString();
        //var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
        //var dd = date.getDate().toString();
        //return yyyy + '/' +  (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
    };

    // Function to calculate time difference between dates - dd/hh/mm
    vm.calculateTime = function(lottery) {
      //console.log(lottery.lotteryName, lottery.nextDrawDateTimeList[0]);
      var today = new Date(); // today's date
      var nextDraw = new Date(lottery.nextDrawDateTimeList[0]); // next draw date for the lottery
      var difference = nextDraw.getTime() - today.getTime();

      //calculate days
      var daysLeft = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysLeft * 1000 * 60 * 60 * 24;
      daysLeft = (daysLeft.toString().length < 2) ? "0"+ daysLeft : daysLeft;

      //calcualte hours
      var hoursLeft = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursLeft * 1000 * 60 * 60;
      hoursLeft = (hoursLeft.toString().length < 2) ? "0"+ hoursLeft : hoursLeft;

      //calculate minutes
      var minutesLeft = Math.floor(difference / 1000 / 60);
      difference -= minutesLeft * 1000 * 60;
      minutesLeft = (minutesLeft.toString().length < 2) ? "0"+ minutesLeft : minutesLeft;
      //console.log(daysLeft, hoursLeft, minutesLeft);

      return daysLeft + " : " + hoursLeft + " : " + minutesLeft;
    };
  }

})();

(function() {
    'use strict';

    MainController.$inject = ["$log", "$mdSidenav", "$iwProducts"];
    angular
        .module('sevenLottos')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($log, $mdSidenav, $iwProducts) {
        var vm = this;

        vm.navOpen = true;


        // Featured lottery
        vm.featuredLottery = {};

        // 
        vm.featuredSyndicate = {};

        init();

        function init() {

            // Setup the featured syndicate
            if($iwProducts.isLoaded) {
                setFeaturedSyndicate();
            }
            else {
                $iwProducts.Syndicates.load().then(setFeaturedSyndicate);
            }
        }

        /**
         * Set the featured syndicate
         * 
         * @return {void}
         */
        function setFeaturedSyndicate() {

            // Get a list of all syndicates
            var syndicates = $iwProducts.Syndicates.all();

            // Get the cheapest (last one)
            if(syndicates.length > 0) {
                vm.featuredSyndicate = syndicates[syndicates.length - 1];
            }

            vm.featuredLottery = $iwProducts.SingleLines.get(10);
        }



        vm.closeMenu = function() {
            $mdSidenav('mobile-payslip').close();
        }

        vm.toggle = function() {
            $mdSidenav('mobile-payslip').toggle();
        };
    }

})();

(function() {
    'use strict';

    PlayPageController.$inject = ["$log", "IWConstants", "$filter", "ModalService", "SingleLineProducts", "SyndicateProducts", "$stateParams", "$iwCart", "$iwProducts"];
    angular
        .module('sevenLottos')
        .controller('PlayPageController', PlayPageController);

    /** @ngInject */
    function PlayPageController($log, IWConstants, $filter, ModalService, SingleLineProducts, SyndicateProducts, $stateParams, $iwCart, $iwProducts) {
        var vm = this;

        //$log.log('On play page', $stateParams);

        vm.products = [];
        vm.syndicates = [];
        vm.games = [];
        vm.play = playSingle;
        vm.playSyndicate = playSyndicate;
        vm.setFilter = setFilter;
        vm.setOrder = setOrder;

        vm.sortType = '_jackpot';
        vm.sortReverse = true;
        vm.sortFilter = '';

        vm.cart = $iwCart.getCart();

        vm.productTypes = IWConstants.PRODUCT_TYPES;

        vm.lottoConfig = {
            european: [
                'Euro Jackpot', 'La Primitiva', 'Euro Millions', 'German Lotto 6aus49', 'UK National Lottery', 'Irish Lotto', 'UK Thunderball', 'France Loto', 'El Gordo de la Primitiva', 'Super 6', 'Glücksspirale', 'Austria 6/45', 'Spiel 77', 'MegaSena', 'SuperEnalotto', 'Swedish Lotto', 'Polish Lotto', 'Polish Lotto Plus', 'Lotería Nacional', 'Finland Lotto', 'BonoLoto'
            ],
            huge: [
                'Euro Jackpot', 'Megamillions', 'Powerball', 'Euro Millions', 'Powerball Australia', 'El Gordo de la Primitiva', 'SuperEnalotto', 'Powerball (South Africa)'
            ],
            odds: [
                'Irish Lotto', 'UK Thunderball', 'France Loto', 'Spiel 77', 'California SuperLottoPlus', 'New York Lotto', 'Florida Lotto', 'Hot Lotto', 'Hoosier Lotto', 'Canadian Lotto 6/49', 'Polish Lotto', 'LOTTO 6/49 (South Africa)'
            ]
        };


        // Load products
        $iwProducts.load().then(function success() {

            // 
            vm.syndicates = SyndicateProducts.all();
            vm.products = SingleLineProducts.all();
            vm.games = vm.syndicates.concat(vm.products);

            // Loop through each game and add filter options
            angular.forEach(vm.games, function(game) {

                // Filter classes for isotop sorting
                //var filterClasses = [];

                // Add values for sorting and filtering
                if(game.productType === IWConstants.PRODUCT_TYPES.SYNDICATE) {
                    game._jackpot = game.jackpot.value / 100000;
                    game._date = game.drawDate;
                    //filterClasses.push('syndicate');
                }
                else if(game.productType === IWConstants.PRODUCT_TYPES.SINGLE_LINE) {
                    game._jackpot = game.jackpot.local.value / 100000;
                    game._date = game.drawDates.next;

                    // 
                    //filterClasses.push('singleline');

                    // Huge jackpot filter values
                    if(vm.lottoConfig.huge.indexOf(game.name) > -1) {
                        game._huge = true;
                        //filterClasses.push('huge');
                    }
                    else {
                        game._huge = false;
                    }

                    // European filter values
                    if(vm.lottoConfig.european.indexOf(game.name) > -1) {
                        game._european = true;
                        //filterClasses.push('european');
                    }
                    else {
                        game._european = false;
                    }

                    // Great odd filter values
                    if(vm.lottoConfig.odds.indexOf(game.name) > -1) {
                        game._odds = true;
                        //filterClasses.push('odds');
                    }
                    else {
                        game._odds = false;
                    }
                }

                // Set filter classes for isotop
                //game._classes = filterClasses.join(' ');
            });

            $log.log(vm.games);

            // Open modal if we need to
            if(angular.isDefined($stateParams.productId) && $stateParams.productId) {
                ModalService.singleLine($stateParams.productId);
            }
        });


        // // Load single line product
        // SingleLineProducts.load().then(function success(response) {

        //     // Set the products
        //     vm.products = SingleLineProducts.all();

        //     // 
        //     angular.forEach(vm.products, function(product) {
        //         $log.log(product);
        //         product._jackpot = product.jackpot.local.value / 100000;
        //         product._date = product.drawDates.next
        //     });

        //     // Open modal if we need to
        //     if(angular.isDefined($stateParams.productId) && $stateParams.productId) {
        //         ModalService.singleLine($stateParams.productId);
        //     }
        // });

        // // Load syndicate products
        // SyndicateProducts.load().then(
        //     function success(response) {
        //         //$log.log(response);
        //         vm.syndicates = SyndicateProducts.all();
        //     },
        //     function error(response) {
        //         //$log.log(response);
        //     }
        // );

        function playSingle(productId) {
            ModalService.singleLine(productId);
        }

        function playSyndicate(productId) {
            $log.log('Playing syndicate', productId);
        }

        function setOrder(order) {
            if(order == 'next') {
                vm.sortType = '_date';
            }
            else if(order == 'name') {
                vm.sortType = 'name';
            }
            else if(order == 'jackpot') {
                vm.sortType = '_jackpot';
            }
            vm.sortReverse = !vm.sortReverse;
        }

        function setFilter(filter) {
            if(filter == 'syndicates') {
                vm.sortFilter = { productType: IWConstants.PRODUCT_TYPES.SYNDICATE };
            }
            else if(filter == 'single_line') {
                vm.sortFilter = { productType: IWConstants.PRODUCT_TYPES.SINGLE_LINE };
            }
            else if(filter == 'european') {
                vm.sortFilter = { _european: true };
            }
            else if(filter == 'huge') {
                vm.sortFilter = { _huge: true };
            }
            else if(filter == 'odds') {
                vm.sortFilter = { _odds: true };
            }
            else {
                vm.sortFilter = '';
            }
        }
    }

})();

(function() {
  'use strict';

  LotteryHistoryResult.$inject = ["$log", "SingleLineProducts", "$filter", "$stateParams", "$rootScope", "$mdMedia", "$getLotteryHistory"];
  angular
    .module('sevenLottos')
    .controller('LotteryHistoryResult', LotteryHistoryResult);

  /** @ngInject */
  function LotteryHistoryResult($log, SingleLineProducts, $filter,$stateParams, $rootScope,$mdMedia,$getLotteryHistory) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;
    $getLotteryHistory.getHistoryForLottery($stateParams.name).then(function (data) {
      vm.lotteryHistory = data;
      vm.lottery = vm.lotteryHistory[0];
    });

    //console.log(vm.lotteryHistory, vm.lottery);
  }

})();

(function() {
  'use strict';

  LotteryPrizeBreakdown.$inject = ["$log", "SingleLineProducts", "$filter", "$rootScope", "$mdMedia", "$stateParams", "$getLotteryHistory"];
  angular
    .module('sevenLottos')
    .controller('LotteryPrizeBreakdown', LotteryPrizeBreakdown);

  /** @ngInject */
  function LotteryPrizeBreakdown($log, SingleLineProducts, $filter, $rootScope, $mdMedia,$stateParams, $getLotteryHistory) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;
    $getLotteryHistory.getBreakDown($stateParams.name, $stateParams.drawDate).then(function(data) {
      vm.lotteryName = $stateParams.name;
      vm.lotteryDrawHistory = data.history;
      vm.lotteryDrawIndex = data.index;
    });

  }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('HowPageController', HowPageController);

    /** @ngInject */
    function HowPageController() {
        //var vm = this;
    }

})();

(function() {
    'use strict';

    HomePageController.$inject = ["$scope", "$timeout", "$log", "SingleLineProducts"];
    angular
        .module('sevenLottos')
        .controller('HomePageController', HomePageController);

    /** @ngInject */
    function HomePageController($scope, $timeout, $log, SingleLineProducts) {
        var vm = this;

        // Promo area slider settings
        vm.promoSlider = {
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        // Lotteries slider settings
        vm.lotteriesSlider = {
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }
            ]
        };

        // Results slider settings
        vm.resultsSlider = {
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }
            ]
        };

        // Lottery results
        vm.lotteryResults = [];
        vm.lotteries = [];

        // Lottery results
        SingleLineProducts.results().then(function(results) {

            var lotteryResults = [];

            // Only add the first item from the results
            angular.forEach(results, function(result) {
                lotteryResults.push(result[0]);
            });

            vm.lotteryResults = lotteryResults;
        });

        //$log.log(vm.lotteries);
        SingleLineProducts.load().then(function() {
            // Set the lotteries
            vm.lotteries = SingleLineProducts.all();
        });
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('ContactPageController', ContactPageController);

    /** @ngInject */
    function ContactPageController() {
        //var vm = this;
    }

})();

(function() {
    'use strict';

    CheckoutPageController.$inject = ["$scope", "$log", "$iwCart", "$timeout", "$iwAccount", "$iwSession", "$iwPayments", "$window", "$stateParams", "ModalService"];
    angular
        .module('sevenLottos')
        .controller('CheckoutPageController', CheckoutPageController);

    /** @ngInject */
    function CheckoutPageController($scope, $log, $iwCart, $timeout, $iwAccount, $iwSession, $iwPayments, $window, $stateParams, ModalService) {
        var vm = this;

        vm.cart = $iwCart.getCart();
        vm.account = $iwAccount.getAccount();
        vm.session = $iwSession.getSession();
        vm.processOrder = processOrder;
        vm.deposit = deposit;
        vm.login = login;
        vm.register = register;

        vm.depositData = {
            cardNumber: '',
            expirationMonth: '',
            expirationYear: '',
            cvv: ''
        };

        // Processing feedback
        vm.processing = {
            processing: false,
            success: false,
            error: false,
            message: '' 
        };

        // Years
        vm.years = [];

        // Order complete
        vm.orderComplete = false;

        init();

        function init() {

            // Check if we are waiting for a deposit response
            if($stateParams.txnId) {
                checkDepositStatus($stateParams.txnId.replace('?txnId=', ''));
            }

            // Setup the years array
            vm.years = generateYears(10);
        }

        function generateYears(max) {
            var currentYear = new Date().getFullYear();
            var years = [];
            do {
                years.push(currentYear++);
                max--;
            }
            while(max > 0);
            return years;
        }

        function setProcessingStatus(processing, success, error, message) {
            if(processing) {
                vm.processing.processing = true;
                vm.processing.success = false;
                vm.processing.error = false;
                vm.processing.message = false;
            }
            else {
                vm.processing.processing = false;
                vm.processing.success = success;
                vm.processing.error = error;
                vm.processing.message = message;
            }
        }

        function checkDepositStatus(id) {

            // 
            $log.log('Checking deposit status');

            setProcessingStatus(true);

            // Check the status
            $iwPayments.checkStatus(id)
                .then(depositSuccess, depositFailed);

            function depositSuccess(response) {
                $log.log('Deposit success', response);
                setProcessingStatus(false, true, false, 'Success');
            }

            function depositFailed(response) {
                $log.log('Deposit failed', response);
                if(response.data.status == 'SUCCESS') {
                    setProcessingStatus(false, false, true, response.data.response.processorResponseMessage);
                }
                else {
                    setProcessingStatus(false, false, true, response.data.errorDescription);
                }
                //vm.account.balance = 10000;
            }
        }

        function processOrder() {
            $log.log('Processing order');

            setProcessingStatus(true);

            $iwCart.commit()
                .then(orderSuccess, orderFailed);

            function orderSuccess(/* response */) {
                $timeout(function() {
                    setProcessingStatus(false);
                    vm.orderComplete = true;
                }, 1500);
            }

            function orderFailed(response) {
                setProcessingStatus(false, false, true, response.data.errorDescription);
            }
        }

        function deposit() {

            // Deposit request
            var request = {
                amount: vm.cart.subTotal - vm.account.balance,
                cardNumber: vm.depositData.cardNumber,
                expirationMonth: vm.depositData.expirationMonth,
                expirationYear: vm.depositData.expirationYear,
                cvv: vm.depositData.cvv,
                firstName: vm.account.profile.firstName,
                lastName: vm.account.profile.lastName,
                redirectionURL: $window.location.href
            };

            if(vm.depositForm.$valid) {

                // Set feedback
                setProcessingStatus(true);
                $log.log('Depositing with', request);

                $iwPayments.deposit('wirecard', request)
                   .then(depositSuccess, depositFailed);
            }

            function depositSuccess(response) {
                $log.log('DP Success -> ', response);
                //setProcessingStatus(false, true, false, 'Deposit success!');
            }

            function depositFailed(response) {
                $log.log('DP Failed -> ', response);
                setProcessingStatus(false, false, true, response.data.errorDescription);
            }
        }

        function login() {
            // Open the login modal
            ModalService.login().then(loginSuccess);

            function loginSuccess() {
                //$state.go('checkout');
            }
        }

        function register() {
            ModalService.register().then(registerSuccess);

            function registerSuccess(resp) {
                //$log.log('Reg success', resp);
            }
        }
    }

})();

(function() {
    'use strict';

    AccountPageController.$inject = ["$log", "AccountModalService", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('AccountPageController', AccountPageController);

    /** @ngInject */
    function AccountPageController($log, AccountModalService, $iwAccount) {
        var vm = this;

        vm.deposit = deposit;
        vm.depositLimits = depositLimits;
        vm.lossLimits = lossLimits;
        vm.purchaseHistory = purchaseHistory;
        vm.selfExclusion = selfExclusion;
        vm.transactionHistory = transactionHistory;
        vm.updateAddress = updateAddress;
        vm.updateEmail = updateEmail;
        vm.updatePassword = updatePassword;
        vm.wagerLimits = wagerLimits;
        vm.withdraw = withdraw;
        vm.tickets = [];

        getSubscriptions();

        function getSubscriptions() {
            $log.log('Getting subscriptions');
            $iwAccount.getSubscriptions().then(
                function success(response) {
                    $log.log('Success subscriptions', response);
                    vm.tickets = response;
                },
                function error(response) {
                    $log.log('Error getting subscriptions', response);
                }
            );
        }

        /**
         * Opens the deposit modal
         * @param  {event} ev The event the user has initiated
         */
        function deposit(ev){
            $log.log('Modal Activated: deposit');
            AccountModalService.deposit(ev);
        }

        /**
         * Opens the depositLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function depositLimits(ev){
            $log.log('Modal Activated: depositLimits');
            AccountModalService.depositLimits(ev);
        }

        /**
         * Opens the lossLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function lossLimits(ev){
            $log.log('Modal Activated: lossLimits');
            AccountModalService.lossLimits(ev);
        }

        /**
         * Opens the purchaseHistory modal
         * @param  {event} ev The event the user has initiated
         */
        function purchaseHistory(ev){
            $log.log('Modal Activated: purchaseHistory');
            AccountModalService.purchaseHistory(ev);
        }

        /**
         * Opens the selfExclusion modal
         * @param  {event} ev The event the user has initiated
         */
        function selfExclusion(ev){
            $log.log('Modal Activated: selfExclusion');
            AccountModalService.selfExclusion(ev);
        }

        /**
         * Opens the transactionHistory modal
         * @param  {event} ev The event the user has initiated
         */
        function transactionHistory(ev){
            $log.log('Modal Activated: transactionHistory');
            AccountModalService.transactionHistory(ev);
        }

        /**
         * Opens the updateAddress modal
         * @param  {event} ev The event the user has initiated
         */
        function updateAddress(ev){
            $log.log('Modal Activated: updateAddress');
            AccountModalService.updateAddress(ev);
        }

        /**
         * Opens the updateEmail modal
         * @param  {event} ev The event the user has initiated
         */
        function updateEmail(ev){
            $log.log('Modal Activated: updateEmail');
            AccountModalService.updateEmail(ev);
        }

        /**
         * Opens the updatePassword modal
         * @param  {event} ev The event the user has initiated
         */
        function updatePassword(ev){
            $log.log('Modal Activated: updatePassword');
            AccountModalService.updatePassword(ev);
        }

        /**
         * Opens the wagerLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function wagerLimits(ev){
            $log.log('Modal Activated: wagerLimits');
            AccountModalService.wagerLimits(ev);
        }

        /**
         * Opens the withdraw modal
         * @param  {event} ev The event the user has initiated
         */
        function withdraw(ev){
            $log.log('Modal Activated: withdraw');
            AccountModalService.withdraw(ev);
        }

    }

})();

(function() {
    'use strict';

    SyndicateModalController.$inject = ["$scope", "$timeout", "$log", "IWConstants", "SyndicateTicket", "$iwCart", "$iwProducts"];
    angular
        .module('sevenLottos')
        .controller('SyndicateModalController', SyndicateModalController);

    /** @ngInject */
    function SyndicateModalController($scope, $timeout, $log, IWConstants, SyndicateTicket, $iwCart, $iwProducts) {
        var vm = this;

        // Product
        vm.product = $scope.ngDialogData.product;

        // New syndicate ticket
        vm.cartItem = new SyndicateTicket(vm.product);
        vm.cartItem.setShares(1);

        // Constants for HTLM access
        vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
        vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

        // Syndicate lotteries data
        vm.syndicateLotteries = {};

        // Number showing
        vm.showingNumbers = false;
        vm.showNumbers = showNumbers;
        vm.decreaseShares = decreaseShares;

        // Add to cart
        vm.addToCart = addToCart;

        matchTicketData();

        /**
         * Match the ticket list data to lottery names
         * @return {void}
         */
        function matchTicketData() {

            // Loop throguh each lottery without a name
            angular.forEach(vm.product.tickets.list, function(lottery) {

                // Loop through each ticket for that lottery
                angular.forEach(lottery, function(ticket) {

                    // Set name for each lottery
                    if(angular.isUndefined(vm.syndicateLotteries[ticket.drawTypeId])) {
                        vm.syndicateLotteries[ticket.drawTypeId] = {
                            name: getProductName(ticket.drawTypeId),
                            drawTypeId: ticket.drawTypeId,
                            tickets: []
                        }
                    }

                    // Add the ticket
                    var tmpTicket = {
                        drawDay: ticket.drawDay,
                        primaryNumbers: [],
                        secondaryNumbers: []
                    };

                    // Convert numbers
                    angular.forEach(ticket.primaryNumbers, function(number) {
                        if(number) {
                            tmpTicket.primaryNumbers.push(+number);
                        }
                    });
                    angular.forEach(ticket.secondaryNumbers, function(number) {
                        if(number) {
                            tmpTicket.secondaryNumbers.push(+number);
                        }
                    });

                    // Add to final array
                    vm.syndicateLotteries[ticket.drawTypeId].tickets.push(tmpTicket);
                });

            });
        }

        function getProductName(drawTypeId) {
            // Get product id
            var productId = $iwProducts.toProductId(drawTypeId);

            // Get product
            var product = $iwProducts.SingleLines.get(productId);

            // Return the name
            return angular.isDefined(product) ? product.name : '';
        }

        function addToCart() {
            if(vm.cartItem.subTotal > 0) {
                $iwCart.addItem(vm.cartItem);
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
            }
        }

        function showNumbers() {
            vm.showingNumbers = true;

            $log.log(vm.product.tickets.list);
        }

        function decreaseShares() {
            if(vm.cartItem.shares.claimed > 1) {
                vm.cartItem.decreaseShares();
            }
        }
    }

})();

(function() {
    'use strict';

    SingleLineModalController.$inject = ["$scope", "$timeout", "$log", "IWConstants", "SingleLineTicket", "$iwCart", "SingleLineProducts", "$iwMedia"];
    angular
        .module('sevenLottos')
        .controller('SingleLineModalController', SingleLineModalController);

    /** @ngInject */
    function SingleLineModalController($scope, $timeout, $log, IWConstants, SingleLineTicket, $iwCart, SingleLineProducts, $iwMedia) {
        var vm = this;

        // Product
        vm.product = $scope.ngDialogData.product;

        // Single Line Ticket
        vm.cartItem = new SingleLineTicket(vm.product);

        // Constants for HTLM access
        vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
        vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

        // Primary numbers
        vm.primaryNumbers = {
            chosen      : [],
            list        : vm.product.numbers.primary.list,
            min         : vm.product.numbers.primary.min,
            max         : vm.product.numbers.primary.max,
            total       : vm.product.numbers.primary.total,
            progress    : 0
        };

        // Secondary numbers
        vm.secondaryNumbers = {
            chosen      : [],
            list        : vm.product.numbers.secondary.list,
            min         : vm.product.numbers.secondary.min,
            max         : vm.product.numbers.secondary.max,
            total       : vm.product.numbers.secondary.total,
            progress    : 0
        };

        // Is mobile flag
        vm.isMobile = !$iwMedia('large');
        $scope.$watch(function() { return $iwMedia('large'); }, function(big) {
            vm.isMobile = !big;
        });

        // Choosing numbers mobile
        vm.choosingNumbers = true;

        // Accessible functions
        vm.choosePrimary = choosePrimary;
        vm.chooseSecondary = chooseSecondary;
        vm.addToCart = addToCart;
        vm.luckyDip = luckyDip;
        vm.chooseNumbers = chooseNumbers;

        function choosePrimary(number) {
            if(vm.primaryNumbers.chosen.indexOf(number) > -1) {
                vm.primaryNumbers.chosen.splice(vm.primaryNumbers.chosen.indexOf(number), 1);
            }
            else {
                if(!(vm.primaryNumbers.chosen.length >= vm.primaryNumbers.min && vm.primaryNumbers.chosen.length <= vm.primaryNumbers.max)) {
                    vm.primaryNumbers.chosen.push(number);
                }
            }

            // Try to add line
            addLine();
        }

        function chooseSecondary(number) {
            if(vm.secondaryNumbers.chosen.indexOf(number) > -1) {
                vm.secondaryNumbers.chosen.splice(vm.secondaryNumbers.chosen.indexOf(number), 1);
            }
            else {
                // if(vm.secondaryNumbers.chosen.length === vm.secondaryNumbers.total) {

                // }
                // else {
                //     vm.secondaryNumbers.chosen.push(number);
                // }
                vm.secondaryNumbers.chosen.push(number);
            }

            // Try to add line
            addLine();
        }

        function addLine() {
            if(vm.primaryNumbers.chosen.length === vm.primaryNumbers.min && vm.secondaryNumbers.chosen.length === vm.secondaryNumbers.min) {
                var line = {
                    primaryNumbers: vm.primaryNumbers.chosen,
                    secondaryNumbers: vm.secondaryNumbers.chosen
                };
                vm.cartItem.addLine(line);
                vm.primaryNumbers.chosen = [];
                vm.secondaryNumbers.chosen = [];
            }
        }

        function luckyDip () {
            SingleLineProducts.generateLine(vm.product.drawTypeId, vm.product.drawDates.next)
                .then(luckyDipSuccess, luckyDipError);

            function luckyDipSuccess(response) {
                vm.cartItem.addLine(response);
                if(vm.isMobile) {
                    vm.choosingNumbers = false;
                }
            }

            function luckyDipError() {}
        }

        function chooseNumbers() {
            vm.choosingNumbers = true;
        }

        function addToCart() {
            if(vm.cartItem.subTotal > 0) {
                $iwCart.addItem(vm.cartItem);
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
            }
        }
    }

})();

    (function() {
    'use strict';

    LoginModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('LoginModalController', LoginModalController);

    /** @ngInject */
    function LoginModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        // Global feedback object
        vm.feedback = {
            processing: false,
            success: false,
            error: false,
            message: ''
        };

        // Login information
        vm.login = {
            username: '',
            password: ''
        };

        // Reset password status
        vm.reset = {
            email: '',
            visible: false
        };

        // Accessible functions
        vm.login = login;
        vm.forgotPassword = forgotPassword;
        vm.resetPassword = resetPassword;

        function login() {
            if($scope.loginForm.$valid) {
                setFeedback(true);

                // Try to login with credentials
                $iwAccount.login(vm.login.username, vm.login.password)
                    .then(loginSuccess, loginFailed);
            }

            function loginSuccess() {
                $timeout(function() {
                    //$scope.closeThisDialog(true);
                    /*eslint-disable */
                    $scope.closeThisDialog(true);
                    /*eslint-enable */
                }, 1000);
            }

            function loginFailed(response) {
                $timeout(function() {
                    setFeedback(false, false, true, response.data.errorDesc);
                }, 1000);
            }
        }

        function forgotPassword(show) {
            if(show) {
                vm.reset.visible = true
            }
            else {
                vm.reset.visible = false;
            }
        }

        function resetPassword() {
            if($scope.forgotPassword.$valid) {
                setFeedback(true);

                $iwAccount.resetPassword(vm.reset.email)
                    .then(resetSuccess, resetFailed);
            }

            function resetSuccess(response) {
                $log.log('Reset worke', response);
                $timeout(function() {
                    setFeedback(false, true);
                    forgotPassword(false);
                }, 1000);
            }

            function resetFailed(response) {
                setFeedback(false, false, true, response.data.errorDesc || response.data.errorLiteral);
            }
        }


        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }
    }

})();

(function() {
    'use strict';

    RegisterModalController.$inject = ["$scope", "$timeout", "$log", "$iwAccount"];
    angular
        .module('sevenLottos')
        .controller('RegisterModalController', RegisterModalController);

    /** @ngInject */
    function RegisterModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        // Global feedback object
        vm.feedback = {
            processing: false,
            success: false,
            error: false,
            message: ''
        };

        // Registration data information
        vm.registerData = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            phone: '',
            terms: false
        };

        // Accessible functions
        vm.register = register;

        function register() {
            if($scope.registerForm.$valid && vm.registerData.terms == true && !vm.feedback.processing) {
                setFeedback(true);

                // Try to login with credentials
                $iwAccount.register(vm.registerData)
                    .then(registerSuccess, registerFailed);
            }
            else {
                setFeedback(false);
            }

            /** TODO: Change this so it handles failures */
            function registerSuccess() {
                $iwAccount.login(vm.registerData.email, vm.registerData.password).then(function() {
                    /*eslint-disable */
                    $scope.closeThisDialog(true);
                    /*eslint-enable */
                });
            }

            function registerFailed(response) {
                $log.log('Register failed', response);
                $timeout(function() {
                    setFeedback(false, false, true, response.errorDesc);
                }, 1000);
            }
        }

        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }
    }

})();

(function() {
    'use strict';

    AccountModalService.$inject = ["$q", "$log", "$mdDialog", "ngDialog"];
    angular
        .module('sevenLottos')
        .service('AccountModalService', AccountModalService);

    /** @ngInject */
    function AccountModalService($q, $log, $mdDialog, ngDialog) {
        // var vm = this;
        
        /**
         * Modal service
         * 
         * @type {Object}
         */
        var service = {
            deposit             : deposit,
            depositLimits       : depositLimits,
            lossLimits          : lossLimits,
            purchaseHistory     : purchaseHistory,
            selfExclusion       : selfExclusion,
            transactionHistory  : transactionHistory,
            updateAddress       : updateAddress,
            updateEmail         : updateEmail,
            updatePassword      : updatePassword,
            wagerLimits         : wagerLimits,
            withdraw            : withdraw
        };
        return service;

        /**
         * Show deposit
         */
        function deposit(/*ev*/) {
            var deferred = $q.defer();

            var depositDialog = ngDialog.open({
                name: 'depositModal',
                template: 'app/modals/account/deposit/deposit.html',
                appendClassName: 'deposit-modal',
                controller: 'DepositModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            depositDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show deposit limits
         */
        function depositLimits(/*ev*/) {
            var deferred = $q.defer();

            var depositLimitsDialog = ngDialog.open({
                name: 'depositLimitModal',
                template: 'app/modals/account/deposit-limits/deposit-limits.html',
                appendClassName: 'deposit-limits-modal',
                controller: 'DepositLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            depositLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show loss limits
         */
        function lossLimits(/*ev*/) {
            var deferred = $q.defer();

            var lossLimitsDialog = ngDialog.open({
                name: 'lossLimitModal',
                template: 'app/modals/account/loss-limits/loss-limits.html',
                appendClassName: 'loss-limits-modal',
                controller: 'LossLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            lossLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show purchase history
         */
        function purchaseHistory(/*ev*/) {
            var deferred = $q.defer();

            var purchaseHistoryDialog = ngDialog.open({
                name: 'lossLimitModal',
                template: 'app/modals/account/purchase-history/purchase-history.html',
                appendClassName: 'purchase-history-modal',
                controller: 'PurchaseHistoryModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            purchaseHistoryDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Self Exclusion Modal
         */
        function selfExclusion(/*ev*/) {
            var deferred = $q.defer();

            var selfExclusionDialog = ngDialog.open({
                name: 'selfExclusionModal',
                template: 'app/modals/account/self-exclusion/self-exclusion.html',
                appendClassName: 'self-exclusion-modal',
                controller: 'SelfExclusionModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            selfExclusionDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Transaction History Modal
         */
        function transactionHistory(/*ev*/) {
            var deferred = $q.defer();

            var transactionHistoryDialog = ngDialog.open({
                name: 'transactionHistoryModal',
                template: 'app/modals/account/transaction-history/transaction-history.html',
                appendClassName: 'transaction-history-modal',
                controller: 'TransactionHistoryModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            transactionHistoryDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Address Modal
         */
        function updateAddress(/*ev*/) {
            var deferred = $q.defer();

            var updateAddressDialog = ngDialog.open({
                name: 'updateAddressModal',
                template: 'app/modals/account/update-address/update-address.html',
                appendClassName: 'update-address-modal',
                controller: 'UpdateAddressModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updateAddressDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Email Modal
         */
        function updateEmail(/*ev*/) {
            var deferred = $q.defer();

            var updateEmailDialog = ngDialog.open({
                name: 'updateEmailModal',
                template: 'app/modals/account/update-email/update-email.html',
                appendClassName: 'update-email-modal',
                controller: 'UpdateEmailModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updateEmailDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Password Modal
         */
        function updatePassword(/*ev*/) {
            var deferred = $q.defer();

            var updatePasswordDialog = ngDialog.open({
                name: 'updatePasswordModal',
                template: 'app/modals/account/update-password/update-password.html',
                appendClassName: 'update-password-modal',
                controller: 'UpdatePasswordModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updatePasswordDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Wager Limits Modal
         */
        function wagerLimits(/*ev*/) {
            var deferred = $q.defer();

            var wagerLimitsDialog = ngDialog.open({
                name: 'wagerLimitsModal',
                template: 'app/modals/account/wager-limits/wager-limits.html',
                appendClassName: 'wager-limits-modal',
                controller: 'WagerLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            wagerLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Withdraw Modal
         */
        function withdraw(/*ev*/) {
            var deferred = $q.defer();

            var withdrawDialog = ngDialog.open({
                name: 'withdrawModal',
                template: 'app/modals/account/withdraw/withdraw.html',
                appendClassName: 'withdraw-modal',
                controller: 'WithdrawModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            withdrawDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

    }

})();
(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('syndicateItem', syndicateItem);

    /** @ngInject */
    function syndicateItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/syndicate-item/syndicate-item.html',
            scope: {
                data: '='
            },
            link: SyndicateItemLink,
            replace: true,
            controller: SyndicateItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function SyndicateItemLink(/*scope, iElement, iAttrs, controller, transclude*/) {
            
            // 
        }

        function SyndicateItemController($log, ModalService) {
            var vm = this;

            vm.play = play;

            function play() {
                $log.log('Playing syndicate', vm.data);
                ModalService.syndicate(vm.data.id);
            }
        }
    }

})();

(function() {
    'use strict';

    /**
     * This directive is pretty much a clone of the mdSidenav directive. This allows
     * us to use our own code which fixes the issues mdSidenav has with Internet
     * Explorer and mobile devices while still maintaining the functionality
     * mdSidenav provides. Some features have been stripped out.
     */
    slSidenav.$inject = ["$mdUtil", "$document", "$animate", "$q", "$mdConstant"];
    angular
        .module('sevenLottos')
        .directive('slSidenav', slSidenav);

    /** @ngInject */
    function slSidenav($mdUtil, $document, $animate, $q, $mdConstant) {
        SidenavController.$inject = ["$scope", "$log", "$mdComponentRegistry", "$attrs"];
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/sidenav/sidenav.html',
            template: '<div ng-transclude></div>',
            scope: {
                isOpen: '=?isOpen'
            },
            transclude: true,
            controller: SidenavController,
            controllerAs: 'vm',
            bindToController: true,
            compile: function(element) {
                element.addClass('sl-sidenav');
                return postLink;
            }
        };

        return directive;

        // Postlink function
        function postLink (scope, element, attr, sidenavCtrl) {

            // Default body overflow value
            var bodyOverflow = undefined;

            // Backdrop
            var backdrop;

            // Promise object
            var promise = $q.when(true);

            // Body element we insert our backdrop into
            var body = $document.find('body').eq(0);

            // Create the backdrop only if we want to
            if(angular.isUndefined(attr.disableBackdrop)) {
                backdrop = angular.element('<div class="sl-sidenav-backdrop"></div>');
            }

            // Watch for the isOpen changes and show/hide menu & backdrop
            scope.$watch('isOpen', changeIsOpen);

            // Publish special accessor for the Controller instance
            sidenavCtrl.$toggleOpen = toggleOpen;

            // Change opens status of menu
            function changeIsOpen(isOpen) {

                // Attache keypress event to close nav when 'escape' is pressed
                body[isOpen ? 'on' : 'off']('keydown', onKeyDown);

                // Attach close event to backdrop on click
                if(backdrop) {
                    backdrop[isOpen ? 'on' : 'off']('click', close);
                }

                // Disable body scroll
                disableScroll(isOpen);

                // Return the promise object
                return promise = $q.all([
                    // Show/hide backdrop
                    isOpen && backdrop ? $animate.enter(backdrop, body) : backdrop ? $animate.leave(backdrop) : $q.when(true),

                    // Add / Remove open class
                    $animate[!isOpen ? 'removeClass' : 'addClass'](element, 'sl-sidenav-open')
                ]);
            }

            // Disable scrolling of the body element
            function disableScroll(disabled) {
                if(disabled && !bodyOverflow) {
                    bodyOverflow = body.css('overflow');
                    body.css('overflow', 'hidden')
                    angular.element('html').addClass('sidenav-open');
                }
                else if(angular.isDefined(bodyOverflow)) {
                    body.css('overflow', bodyOverflow)
                    angular.element('html').removeClass('sidenav-open');
                    bodyOverflow = undefined;
                }
            }

            // Toggle the open/close status of the menu
            function toggleOpen(isOpen) {

                // Check if the menu is already at the state the user wants it at
                if(scope.isOpen == isOpen) {
                    return $q.when(true);
                }
                else {
                    return $q(function(resolve) {
                        // Toggle value to force an async 'changeIsOpen()' to run
                        scope.isOpen = isOpen;

                        // When the current navigation animation finishes resolve
                        $mdUtil.nextTick(function() {
                            promise.then(function(result) {
                                resolve(result);
                            });
                        });
                    });
                }
            }

            // Close nav with the scape key is pressed
            function onKeyDown(ev) {
              var isEscape = (ev.keyCode === $mdConstant.KEY_CODE.ESCAPE);
              return isEscape ? close(ev) : $q.when(true);
            }

            // Close
            function close(ev) {
                ev.preventDefault();
                sidenavCtrl.close();
            }
        }

        /** @ngInject */
        function SidenavController($scope, $log, $mdComponentRegistry, $attrs) {
            var vm = this;

            // Open the menu
            vm.open = function() {
                return vm.$toggleOpen(true);
            };

            // Close the menu
            vm.close = function() {
                return vm.$toggleOpen(false);
            };

            // Toggle the menu
            vm.toggle = function() {
                return vm.$toggleOpen(!$scope.isOpen);
            };

            // Toggle open
            vm.$toggleOpen = function(value) {
                return $q.when($scope.isOpen = value);
            };

            // Register component so we can use it with $mdSidenav service
            $mdComponentRegistry.register(vm, $attrs.componentId);
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('playslip', playslip);

    /** @ngInject */
    function playslip() {
        PlayslipController.$inject = ["$log", "$iwCart", "$iwSession", "IWConstants"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/playslip/playslip.html',
            scope: {
                showCheckout: '@'
            },
            link: PlayslipLink,
            controller: PlayslipController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function PlayslipLink(/*scope, iElement, iAttrs*/) {

        }

        /** @ngInject */
        function PlayslipController($log, $iwCart, $iwSession, IWConstants) {
            var vm = this;

            if(angular.isUndefined(vm.showCheckout)) {
                vm.showCheckout = true;
            }
            else {
                vm.showCheckout = vm.showCheckout == true ? true : false;
                vm.showCheckout = true;
            }

            // Get the cart object
            vm.cart = $iwCart.getCart();

            // Session
            vm.session = $iwSession.getSession();

            // Constants for HTLM access
            vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
            vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

            vm.removeAllProducts = removeAllProducts;
            vm.removeItem = removeItem;
            
            vm.logCart = function() {
                $log.log(vm.cart);
            }

            function removeAllProducts() {
                $iwCart.empty();
            }

            function removeItem(index) {
                $iwCart.removeItem(index);
            }
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('nextDraw', nextDraw);

    /** @ngInject */
    function nextDraw() {
        NextDrawController.$inject = ["$log", "$locale", "ModalService", "SingleLineProducts", "$timeout", "IWConstants", "$iwSession"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/next-draw/next-draw.html',
            scope: {
                productId: '=?',
                product: '=?'
            },
            controller: NextDrawController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        /** @ngInject */
        function NextDrawController($log, $locale, ModalService, SingleLineProducts, $timeout, IWConstants, $iwSession) {
            var vm = this;

            // Product type constants
            vm.productTypes = IWConstants.PRODUCT_TYPES;
            vm.session = $iwSession.getSession();

            // Public functions
            vm.play = play;

            init();

            /**
             * Initialise next draw (Grab product if got passed id)
             * 
             * @return {void}
             */
            function init() {
                if(angular.isDefined(vm.productId) && angular.isNumber(vm.productId)) {
                    $log.log('Loading the product', vm.productId);
                    vm.product = SingleLineProducts.one(vm.productId);
                }
            }

            /**
             * Open the play modal
             * 
             * @return {vopid}
             */
            function play() {
                if(vm.product.productType === IWConstants.PRODUCT_TYPES.SINGLE_LINE) {
                    ModalService.singleLine(vm.productId);
                }
                else if(vm.product.productType === IWConstants.PRODUCT_TYPES.SYNDICATE) {
                    ModalService.syndicate(vm.productId);
                }
            }
        }
    }

})();

(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .directive('lotteryDrawPrizeBreakdown', lotteryDrawPrizeBreakdown);

  /** @ngInject */
  function lotteryDrawPrizeBreakdown() {
    LotteryPrizeBreakdownController.$inject = ["$scope", "SingleLineProducts", "$getPrizeBreakdown", "$mdMedia"];
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/lotteryDrawPrizeBreakdown/draw-prize.html',
      controller: LotteryPrizeBreakdownController,
      controllerAs: 'prizeCtrl',
      bindToController: true,
      scope: {
        index: "=",
        history: "="
      }
    };

    return directive;

    /** @ngInject */
    function LotteryPrizeBreakdownController($scope, SingleLineProducts, $getPrizeBreakdown,$mdMedia) {
      $scope.$mdMedia = $mdMedia;
      var vm = this;
      //var lotteryNext;
      vm.query = {
        order: 'name',
        limit: 5,
        page: 1
      };
      //console.log('from the draw prize breakdown directive ', vm.history, vm.index);
      vm.currentDraw = vm.history[vm.index];
      var nextDrawIndex = vm.index - 1;
      var previousDrawIndex = vm.index + 1;
      vm.winningNumbers = {
        primaryNumbers: [],
        secondaryNumbers: []
      };
      vm.nextDraw = vm.history[nextDrawIndex];
      vm.previousDraw = vm.history[previousDrawIndex];
      $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
        //console.log('done', prizes);
        vm.drawPrizes = prizes;
        vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
          return parseInt(item, 10);
        });
        vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
          return parseInt(item, 10);
        });
      });

      //go thourgh the array and get next draw
      vm.getNextDraw = function() {
        if (vm.index > 0) {
          vm.index = vm.index - 1;
          //console.log(vm.index, vm.history.length);
          vm.currentDraw = vm.history[vm.index];
          nextDrawIndex = vm.index - 1;
          previousDrawIndex = vm.index + 1;
          vm.nextDraw = vm.history[nextDrawIndex];
          vm.previousDraw = vm.history[previousDrawIndex];
          $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
            //console.log('done', prizes);
            vm.drawPrizes = prizes;
            vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
            vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
          });
        }
      };

      //go thourgh the array and get prev draw
      vm.getPrevDraw = function() {
        if (vm.index < vm.history.length - 1) {
          vm.index = vm.index + 1;
          //console.log(vm.index, vm.history.length);
          vm.currentDraw = vm.history[vm.index];
          nextDrawIndex = vm.index - 1;
          previousDrawIndex = vm.index + 1;
          vm.nextDraw = vm.history[nextDrawIndex];
          vm.previousDraw = vm.history[previousDrawIndex];
          $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
            //console.log('done',prizes);
            vm.drawPrizes = prizes;
            vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
            vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
          });
        }
      };

      // go to select ticket page
      vm.play = function() {
          // do something here @Ben
      };
    }

  }

})();

(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .directive('lotteryDrawHistory', lotteryDrawHistory);

  /** @ngInject */
  function lotteryDrawHistory() {
    LotteryDrawHistory.$inject = ["$scope", "SingleLineProducts", "$filter", "$getPrizeBreakdown", "$state", "$interval"];
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/lotteryDrawHistory/draw-history.html',
      controller: LotteryDrawHistory,
      controllerAs: 'drawCtrl',
      bindToController: true,
      scope: {
        history: "="
      }
    };

    return directive;

    /** @ngInject */
    function LotteryDrawHistory($scope,SingleLineProducts, $filter,$getPrizeBreakdown,$state, $interval) {
      var vm = this;
      var lotteryNext;
      //console.log('from the draw history directive ', vm.history);
      angular.forEach(vm.history, function(draw) {
        $getPrizeBreakdown.getPrizes(draw.drawDateTime, draw.drawId).then(function(prizes) {
            draw.prize = prizes;
        }, function() {
            //console.log('error geting prizes for draw', error);
        });
      });
      vm.lotteryName = vm.history[0].lotteryName;
      vm.nextDraw = vm.history[0].nextDrawDateTimeList;
      vm.drawTypeId = vm.history[0].drawTypeId;
      // pagination
      vm.curPage = 0;
      vm.pageSize = 10;

      vm.numberOfPages = function() {
        return Math.ceil(vm.history.length / vm.pageSize);
      };

      //prev function
      vm.prev = function() {
        vm.curPage = vm.curPage - 1;
      };

      //prev function
      vm.next = function() {
        vm.curPage = vm.curPage + 1;
      };

      // go to prize breakdown page
      vm.prizeBreakdown = function(drawTime, drawId) {
        //console.log(drawTime, drawId);
        var index = vm.history.map(function(obj, index) {
          if (obj.drawId == drawId) {
            return index;
          }
        }).filter(isFinite)[0];
        //console.log(index);
        var data = {
          name: vm.lotteryName,
          drawDate: drawTime,
          index: index,
          history: vm.history
        };
        //console.log(data);
        //localStorage.setItem('prizeItem', JSON.stringify(data));
        $state.go('prizebreakdown', data);
      };

      //compare dates
      var today = $filter('formatDate')(new Date());
      vm.compareDates = function(dateArr) {
        var keepGoing = true;
        var endDate;
        angular.forEach(dateArr, function(date) {
          var lotteryDate = $filter('formatDate')(date);
          if (keepGoing) {
            if (lotteryDate > today) {
              endDate = date;
              keepGoing = false;
            }
          }
        });
        return endDate;
      };
      //time left to next draw
      vm.calculateTime = function(date) {
        lotteryNext = date;
        var today = new Date(); // today's date
        var nextDraw = new Date(date); // next draw date for the lottery
        var difference = nextDraw.getTime() - today.getTime();

        //calculate days
        var daysLeft = Math.floor(difference / 1000 / 60 / 60 / 24);
        difference -= daysLeft * 1000 * 60 * 60 * 24;
        daysLeft = (daysLeft.toString().length < 2) ? "0" + daysLeft : daysLeft;

        //calcualte hours
        var hoursLeft = Math.floor(difference / 1000 / 60 / 60);
        difference -= hoursLeft * 1000 * 60 * 60;
        hoursLeft = (hoursLeft.toString().length < 2) ? "0" + hoursLeft : hoursLeft;

        //calculate minutes
        var minutesLeft = Math.floor(difference / 1000 / 60);
        difference -= minutesLeft * 1000 * 60;
        minutesLeft = (minutesLeft.toString().length < 2) ? "0" + minutesLeft : minutesLeft;


        var secondsLeft = Math.floor(difference / 1000);
        secondsLeft = (secondsLeft.toString().length < 2) ? "0" + secondsLeft : secondsLeft;

        //console.log(daysLeft, hoursLeft, minutesLeft, secondsLeft);

        return {
          daysLeft: daysLeft,
          hoursLeft: hoursLeft,
          minutesLeft: minutesLeft,
          secondsLeft: secondsLeft
        };
      };

      $interval(function(){
      //setInterval(function() {
        //if (lotteryNext !== undefined) {
        if(angular.isDefined(lotteryNext)) {
          vm.calculateTime(lotteryNext);
          //$scope.$apply();
        }
      }, 1000);
    }

  }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('lotteryResultItem', lotteryResultItem);

    /** @ngInject */
    function lotteryResultItem() {
        LotteryResultItemController.$inject = ["$log", "ModalService", "SingleLineProducts"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/lottery-result-item/lottery-result-item.html',
            scope: {
                data: '='
            },
            link: LotteryResultItemLink,
            replace: true,
            controller: LotteryResultItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function LotteryResultItemLink(/* scope, iElement, iAttrs, controller, transclude */) {

            // 
        }

        /** @ngInject */
        function LotteryResultItemController($log, ModalService, SingleLineProducts) {
            var vm = this;

            vm.play = play;

            function play() {
                var productId = SingleLineProducts.convertId(vm.data.drawTypeId);
                if(productId) {
                    ModalService.singleLine(productId);
                }
            }
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('lotteryItem', lotteryItem);

    /** @ngInject */
    function lotteryItem() {
        LotteryItemController.$inject = ["$log", "ModalService"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/lottery-item/lottery-item.html',
            scope: {
                data: '='
            },
            link: LotteryItemLink,
            replace: true,
            controller: LotteryItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function LotteryItemLink(/*scope, iElement, iAttrs, controller, transclude*/) {

            // 
        }

        /** @ngInject */
        function LotteryItemController($log, ModalService) {
            var vm = this;

            vm.play = play;

            function play() {
                ModalService.singleLine(vm.data.id);
            }
            //$log.log('Lottery controller', vm.data);
        }
    }

})();

/*eslint-disable */
/**
 * Isotope v1.5.25
 * An exquisite jQuery plugin for magical layouts
 * http://isotope.metafizzy.co
 *
 * Commercial use requires one-time purchase of a commercial license
 * http://isotope.metafizzy.co/docs/license.html
 *
 * Non-commercial use is licensed under the MIT License
 *
 * Copyright 2013 Metafizzy
 */
(function(a,b,c){"use strict";var d=a.document,e=a.Modernizr,f=function(a){return a.charAt(0).toUpperCase()+a.slice(1)},g="Moz Webkit O Ms".split(" "),h=function(a){var b=d.documentElement.style,c;if(typeof b[a]=="string")return a;a=f(a);for(var e=0,h=g.length;e<h;e++){c=g[e]+a;if(typeof b[c]=="string")return c}},i=h("transform"),j=h("transitionProperty"),k={csstransforms:function(){return!!i},csstransforms3d:function(){var a=!!h("perspective");if(a){var c=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),d="@media ("+c.join("transform-3d),(")+"modernizr)",e=b("<style>"+d+"{#modernizr{height:3px}}"+"</style>").appendTo("head"),f=b('<div id="modernizr" />').appendTo("html");a=f.height()===3,f.remove(),e.remove()}return a},csstransitions:function(){return!!j}},l;if(e)for(l in k)e.hasOwnProperty(l)||e.addTest(l,k[l]);else{e=a.Modernizr={_version:"1.6ish: miniModernizr for Isotope"};var m=" ",n;for(l in k)n=k[l](),e[l]=n,m+=" "+(n?"":"no-")+l;b("html").addClass(m)}if(e.csstransforms){var o=e.csstransforms3d?{translate:function(a){return"translate3d("+a[0]+"px, "+a[1]+"px, 0) "},scale:function(a){return"scale3d("+a+", "+a+", 1) "}}:{translate:function(a){return"translate("+a[0]+"px, "+a[1]+"px) "},scale:function(a){return"scale("+a+") "}},p=function(a,c,d){var e=b.data(a,"isoTransform")||{},f={},g,h={},j;f[c]=d,b.extend(e,f);for(g in e)j=e[g],h[g]=o[g](j);var k=h.translate||"",l=h.scale||"",m=k+l;b.data(a,"isoTransform",e),a.style[i]=m};b.cssNumber.scale=!0,b.cssHooks.scale={set:function(a,b){p(a,"scale",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.scale?d.scale:1}},b.fx.step.scale=function(a){b.cssHooks.scale.set(a.elem,a.now+a.unit)},b.cssNumber.translate=!0,b.cssHooks.translate={set:function(a,b){p(a,"translate",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.translate?d.translate:[0,0]}}}var q,r;e.csstransitions&&(q={WebkitTransitionProperty:"webkitTransitionEnd",MozTransitionProperty:"transitionend",OTransitionProperty:"oTransitionEnd otransitionend",transitionProperty:"transitionend"}[j],r=h("transitionDuration"));var s=b.event,t=b.event.handle?"handle":"dispatch",u;s.special.smartresize={setup:function(){b(this).bind("resize",s.special.smartresize.handler)},teardown:function(){b(this).unbind("resize",s.special.smartresize.handler)},handler:function(a,b){var c=this,d=arguments;a.type="smartresize",u&&clearTimeout(u),u=setTimeout(function(){s[t].apply(c,d)},b==="execAsap"?0:100)}},b.fn.smartresize=function(a){return a?this.bind("smartresize",a):this.trigger("smartresize",["execAsap"])},b.Isotope=function(a,c,d){this.element=b(c),this._create(a),this._init(d)};var v=["width","height"],w=b(a);b.Isotope.settings={resizable:!0,layoutMode:"masonry",containerClass:"isotope",itemClass:"isotope-item",hiddenClass:"isotope-hidden",hiddenStyle:{opacity:0,scale:.001},visibleStyle:{opacity:1,scale:1},containerStyle:{position:"relative",overflow:"hidden"},animationEngine:"best-available",animationOptions:{queue:!1,duration:800},sortBy:"original-order",sortAscending:!0,resizesContainer:!0,transformsEnabled:!0,itemPositionDataEnabled:!1},b.Isotope.prototype={_create:function(a){this.options=b.extend({},b.Isotope.settings,a),this.styleQueue=[],this.elemCount=0;var c=this.element[0].style;this.originalStyle={};var d=v.slice(0);for(var e in this.options.containerStyle)d.push(e);for(var f=0,g=d.length;f<g;f++)e=d[f],this.originalStyle[e]=c[e]||"";this.element.css(this.options.containerStyle),this._updateAnimationEngine(),this._updateUsingTransforms();var h={"original-order":function(a,b){return b.elemCount++,b.elemCount},random:function(){return Math.random()}};this.options.getSortData=b.extend(this.options.getSortData,h),this.reloadItems(),this.offset={left:parseInt(this.element.css("padding-left")||0,10),top:parseInt(this.element.css("padding-top")||0,10)};var i=this;setTimeout(function(){i.element.addClass(i.options.containerClass)},0),this.options.resizable&&w.bind("smartresize.isotope",function(){i.resize()}),this.element.delegate("."+this.options.hiddenClass,"click",function(){return!1})},_getAtoms:function(a){var b=this.options.itemSelector,c=b?a.filter(b).add(a.find(b)):a,d={position:"absolute"};return c=c.filter(function(a,b){return b.nodeType===1}),this.usingTransforms&&(d.left=0,d.top=0),c.css(d).addClass(this.options.itemClass),this.updateSortData(c,!0),c},_init:function(a){this.$filteredAtoms=this._filter(this.$allAtoms),this._sort(),this.reLayout(a)},option:function(a){if(b.isPlainObject(a)){this.options=b.extend(!0,this.options,a);var c;for(var d in a)c="_update"+f(d),this[c]&&this[c]()}},_updateAnimationEngine:function(){var a=this.options.animationEngine.toLowerCase().replace(/[ _\-]/g,""),b;switch(a){case"css":case"none":b=!1;break;case"jquery":b=!0;break;default:b=!e.csstransitions}this.isUsingJQueryAnimation=b,this._updateUsingTransforms()},_updateTransformsEnabled:function(){this._updateUsingTransforms()},_updateUsingTransforms:function(){var a=this.usingTransforms=this.options.transformsEnabled&&e.csstransforms&&e.csstransitions&&!this.isUsingJQueryAnimation;a||(delete this.options.hiddenStyle.scale,delete this.options.visibleStyle.scale),this.getPositionStyles=a?this._translate:this._positionAbs},_filter:function(a){var b=this.options.filter===""?"*":this.options.filter;if(!b)return a;var c=this.options.hiddenClass,d="."+c,e=a.filter(d),f=e;if(b!=="*"){f=e.filter(b);var g=a.not(d).not(b).addClass(c);this.styleQueue.push({$el:g,style:this.options.hiddenStyle})}return this.styleQueue.push({$el:f,style:this.options.visibleStyle}),f.removeClass(c),a.filter(b)},updateSortData:function(a,c){var d=this,e=this.options.getSortData,f,g;a.each(function(){f=b(this),g={};for(var a in e)!c&&a==="original-order"?g[a]=b.data(this,"isotope-sort-data")[a]:g[a]=e[a](f,d);b.data(this,"isotope-sort-data",g)})},_sort:function(){var a=this.options.sortBy,b=this._getSorter,c=this.options.sortAscending?1:-1,d=function(d,e){var f=b(d,a),g=b(e,a);return f===g&&a!=="original-order"&&(f=b(d,"original-order"),g=b(e,"original-order")),(f>g?1:f<g?-1:0)*c};this.$filteredAtoms.sort(d)},_getSorter:function(a,c){return b.data(a,"isotope-sort-data")[c]},_translate:function(a,b){return{translate:[a,b]}},_positionAbs:function(a,b){return{left:a,top:b}},_pushPosition:function(a,b,c){b=Math.round(b+this.offset.left),c=Math.round(c+this.offset.top);var d=this.getPositionStyles(b,c);this.styleQueue.push({$el:a,style:d}),this.options.itemPositionDataEnabled&&a.data("isotope-item-position",{x:b,y:c})},layout:function(a,b){var c=this.options.layoutMode;this["_"+c+"Layout"](a);if(this.options.resizesContainer){var d=this["_"+c+"GetContainerSize"]();this.styleQueue.push({$el:this.element,style:d})}this._processStyleQueue(a,b),this.isLaidOut=!0},_processStyleQueue:function(a,c){var d=this.isLaidOut?this.isUsingJQueryAnimation?"animate":"css":"css",f=this.options.animationOptions,g=this.options.onLayout,h,i,j,k;i=function(a,b){b.$el[d](b.style,f)};if(this._isInserting&&this.isUsingJQueryAnimation)i=function(a,b){h=b.$el.hasClass("no-transition")?"css":d,b.$el[h](b.style,f)};else if(c||g||f.complete){var l=!1,m=[c,g,f.complete],n=this;j=!0,k=function(){if(l)return;var b;for(var c=0,d=m.length;c<d;c++)b=m[c],typeof b=="function"&&b.call(n.element,a,n);l=!0};if(this.isUsingJQueryAnimation&&d==="animate")f.complete=k,j=!1;else if(e.csstransitions){var o=0,p=this.styleQueue[0],s=p&&p.$el,t;while(!s||!s.length){t=this.styleQueue[o++];if(!t)return;s=t.$el}var u=parseFloat(getComputedStyle(s[0])[r]);u>0&&(i=function(a,b){b.$el[d](b.style,f).one(q,k)},j=!1)}}b.each(this.styleQueue,i),j&&k(),this.styleQueue=[]},resize:function(){this["_"+this.options.layoutMode+"ResizeChanged"]()&&this.reLayout()},reLayout:function(a){this["_"+this.options.layoutMode+"Reset"](),this.layout(this.$filteredAtoms,a)},addItems:function(a,b){var c=this._getAtoms(a);this.$allAtoms=this.$allAtoms.add(c),b&&b(c)},insert:function(a,b){this.element.append(a);var c=this;this.addItems(a,function(a){var d=c._filter(a);c._addHideAppended(d),c._sort(),c.reLayout(),c._revealAppended(d,b)})},appended:function(a,b){var c=this;this.addItems(a,function(a){c._addHideAppended(a),c.layout(a),c._revealAppended(a,b)})},_addHideAppended:function(a){this.$filteredAtoms=this.$filteredAtoms.add(a),a.addClass("no-transition"),this._isInserting=!0,this.styleQueue.push({$el:a,style:this.options.hiddenStyle})},_revealAppended:function(a,b){var c=this;setTimeout(function(){a.removeClass("no-transition"),c.styleQueue.push({$el:a,style:c.options.visibleStyle}),c._isInserting=!1,c._processStyleQueue(a,b)},10)},reloadItems:function(){this.$allAtoms=this._getAtoms(this.element.children())},remove:function(a,b){this.$allAtoms=this.$allAtoms.not(a),this.$filteredAtoms=this.$filteredAtoms.not(a);var c=this,d=function(){a.remove(),b&&b.call(c.element)};a.filter(":not(."+this.options.hiddenClass+")").length?(this.styleQueue.push({$el:a,style:this.options.hiddenStyle}),this._sort(),this.reLayout(d)):d()},shuffle:function(a){this.updateSortData(this.$allAtoms),this.options.sortBy="random",this._sort(),this.reLayout(a)},destroy:function(){var a=this.usingTransforms,b=this.options;this.$allAtoms.removeClass(b.hiddenClass+" "+b.itemClass).each(function(){var b=this.style;b.position="",b.top="",b.left="",b.opacity="",a&&(b[i]="")});var c=this.element[0].style;for(var d in this.originalStyle)c[d]=this.originalStyle[d];this.element.unbind(".isotope").undelegate("."+b.hiddenClass,"click").removeClass(b.containerClass).removeData("isotope"),w.unbind(".isotope")},_getSegments:function(a){var b=this.options.layoutMode,c=a?"rowHeight":"columnWidth",d=a?"height":"width",e=a?"rows":"cols",g=this.element[d](),h,i=this.options[b]&&this.options[b][c]||this.$filteredAtoms["outer"+f(d)](!0)||g;h=Math.floor(g/i),h=Math.max(h,1),this[b][e]=h,this[b][c]=i},_checkIfSegmentsChanged:function(a){var b=this.options.layoutMode,c=a?"rows":"cols",d=this[b][c];return this._getSegments(a),this[b][c]!==d},_masonryReset:function(){this.masonry={},this._getSegments();var a=this.masonry.cols;this.masonry.colYs=[];while(a--)this.masonry.colYs.push(0)},_masonryLayout:function(a){var c=this,d=c.masonry;a.each(function(){var a=b(this),e=Math.ceil(a.outerWidth(!0)/d.columnWidth);e=Math.min(e,d.cols);if(e===1)c._masonryPlaceBrick(a,d.colYs);else{var f=d.cols+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.colYs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryPlaceBrick(a,g)}})},_masonryPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=this.masonry.columnWidth*d,h=c;this._pushPosition(a,g,h);var i=c+a.outerHeight(!0),j=this.masonry.cols+1-f;for(e=0;e<j;e++)this.masonry.colYs[d+e]=i},_masonryGetContainerSize:function(){var a=Math.max.apply(Math,this.masonry.colYs);return{height:a}},_masonryResizeChanged:function(){return this._checkIfSegmentsChanged()},_fitRowsReset:function(){this.fitRows={x:0,y:0,height:0}},_fitRowsLayout:function(a){var c=this,d=this.element.width(),e=this.fitRows;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.x!==0&&f+e.x>d&&(e.x=0,e.y=e.height),c._pushPosition(a,e.x,e.y),e.height=Math.max(e.y+g,e.height),e.x+=f})},_fitRowsGetContainerSize:function(){return{height:this.fitRows.height}},_fitRowsResizeChanged:function(){return!0},_cellsByRowReset:function(){this.cellsByRow={index:0},this._getSegments(),this._getSegments(!0)},_cellsByRowLayout:function(a){var c=this,d=this.cellsByRow;a.each(function(){var a=b(this),e=d.index%d.cols,f=Math.floor(d.index/d.cols),g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByRowGetContainerSize:function(){return{height:Math.ceil(this.$filteredAtoms.length/this.cellsByRow.cols)*this.cellsByRow.rowHeight+this.offset.top}},_cellsByRowResizeChanged:function(){return this._checkIfSegmentsChanged()},_straightDownReset:function(){this.straightDown={y:0}},_straightDownLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,0,c.straightDown.y),c.straightDown.y+=d.outerHeight(!0)})},_straightDownGetContainerSize:function(){return{height:this.straightDown.y}},_straightDownResizeChanged:function(){return!0},_masonryHorizontalReset:function(){this.masonryHorizontal={},this._getSegments(!0);var a=this.masonryHorizontal.rows;this.masonryHorizontal.rowXs=[];while(a--)this.masonryHorizontal.rowXs.push(0)},_masonryHorizontalLayout:function(a){var c=this,d=c.masonryHorizontal;a.each(function(){var a=b(this),e=Math.ceil(a.outerHeight(!0)/d.rowHeight);e=Math.min(e,d.rows);if(e===1)c._masonryHorizontalPlaceBrick(a,d.rowXs);else{var f=d.rows+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.rowXs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryHorizontalPlaceBrick(a,g)}})},_masonryHorizontalPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=c,h=this.masonryHorizontal.rowHeight*d;this._pushPosition(a,g,h);var i=c+a.outerWidth(!0),j=this.masonryHorizontal.rows+1-f;for(e=0;e<j;e++)this.masonryHorizontal.rowXs[d+e]=i},_masonryHorizontalGetContainerSize:function(){var a=Math.max.apply(Math,this.masonryHorizontal.rowXs);return{width:a}},_masonryHorizontalResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_fitColumnsReset:function(){this.fitColumns={x:0,y:0,width:0}},_fitColumnsLayout:function(a){var c=this,d=this.element.height(),e=this.fitColumns;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.y!==0&&g+e.y>d&&(e.x=e.width,e.y=0),c._pushPosition(a,e.x,e.y),e.width=Math.max(e.x+f,e.width),e.y+=g})},_fitColumnsGetContainerSize:function(){return{width:this.fitColumns.width}},_fitColumnsResizeChanged:function(){return!0},_cellsByColumnReset:function(){this.cellsByColumn={index:0},this._getSegments(),this._getSegments(!0)},_cellsByColumnLayout:function(a){var c=this,d=this.cellsByColumn;a.each(function(){var a=b(this),e=Math.floor(d.index/d.rows),f=d.index%d.rows,g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByColumnGetContainerSize:function(){return{width:Math.ceil(this.$filteredAtoms.length/this.cellsByColumn.rows)*this.cellsByColumn.columnWidth}},_cellsByColumnResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_straightAcrossReset:function(){this.straightAcross={x:0}},_straightAcrossLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,c.straightAcross.x,0),c.straightAcross.x+=d.outerWidth(!0)})},_straightAcrossGetContainerSize:function(){return{width:this.straightAcross.x}},_straightAcrossResizeChanged:function(){return!0}},b.fn.imagesLoaded=function(a){function h(){a.call(c,d)}function i(a){var c=a.target;c.src!==f&&b.inArray(c,g)===-1&&(g.push(c),--e<=0&&(setTimeout(h),d.unbind(".imagesLoaded",i)))}var c=this,d=c.find("img").add(c.filter("img")),e=d.length,f="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",g=[];return e||h(),d.bind("load.imagesLoaded error.imagesLoaded",i).each(function(){var a=this.src;this.src=f,this.src=a}),c};var x=function(b){a.console&&a.console.error(b)};b.fn.isotope=function(a,c){if(typeof a=="string"){var d=Array.prototype.slice.call(arguments,1);this.each(function(){var c=b.data(this,"isotope");if(!c){x("cannot call methods on isotope prior to initialization; attempted to call method '"+a+"'");return}if(!b.isFunction(c[a])||a.charAt(0)==="_"){x("no such method '"+a+"' for isotope instance");return}c[a].apply(c,d)})}else this.each(function(){var d=b.data(this,"isotope");d?(d.option(a),d._init(c)):b.data(this,"isotope",new b.Isotope(a,this,c))});return this}})(window,jQuery);
/*eslint-enable */
/*eslint-disable */
(function(window, document) {

// Create all modules and define dependencies to make sure they exist
// and are loaded in the correct order to satisfy dependency injection
// before all nested files are concatenated by Grunt

// Config
angular.module('iso.config', [])
    .value('iso.config', {
        debug: true
    });

// Modules
angular.module('iso.directives', ['iso.services']);
angular.module('iso.services', []);
angular.module('iso',
    [
        'iso.config',
        'iso.directives',
        'iso.services'
    ]);


angular.module("iso.controllers", ["iso.config", "iso.services"])
.controller("angularIsotopeController", [
  "iso.config", "iso.topics", "$scope", "$timeout", "optionsStore", function(config, topics, $scope, $timeout, optionsStore) {
    "use strict";
    var buffer, initEventHandler, isoMode, isotopeContainer, methodHandler, onLayoutEvent, optionsHandler, postInitialized, scope;
    onLayoutEvent = "isotope.onLayout";
    postInitialized = false;
    isotopeContainer = null;
    buffer = [];
    scope = "";
    isoMode = "";
    $scope.$on(onLayoutEvent, function(event) {});
    $scope.layoutEventEmit = function($elems, instance) {
      return $timeout(function() {
        return $scope.$apply(function() {
          return $scope.$emit(onLayoutEvent);
        });
      });
    };
    optionsStore.store({
      onLayout: $scope.layoutEventEmit
    });
    initEventHandler = function(fun, evt, hnd) {
      if (evt) {
        return fun.call($scope, evt, hnd);
      }
    };
    $scope.delayInit = function(isoInit) {
      optionsStore.storeInit(isoInit);
    };
    $scope.delayedInit = function() {
      var isoInit = optionsStore.retrieveInit();
      $scope.init(isoInit);
    };

    $scope.$on('iso-init', function() {
      $scope.delayedInit();
    });
    $scope.init = function(isoInit) {
      optionsStore.storeInit(isoInit);
      isotopeContainer = isoInit.element;
      initEventHandler($scope.$on, isoInit.isoOptionsEvent || topics.MSG_OPTIONS, optionsHandler);
      initEventHandler($scope.$on, isoInit.isoMethodEvent || topics.MSG_METHOD, methodHandler);
      $scope.isoMode = isoInit.isoMode || "addItems";
      return $timeout(function() {
        var opts = optionsStore.retrieve();

        if (!(window.jQuery && isotopeContainer.isotope(opts)))
        {
            // create jqLite wrapper
            var instance = new Isotope(isotopeContainer[0], opts);

            isotopeContainer.isotope = function(options, callback) {
                var args = Array.prototype.slice.call( arguments, 1 );
                if ( typeof options === 'string' ) {
                    return(instance[options].apply(instance, args));
                } else {
                    instance.option( options );
                    instance._init( callback );
                }
           }
        }

        postInitialized = true;
      });
    };
    $scope.setIsoElement = function($element) {
      if (postInitialized) {
        return $timeout(function() {
          return isotopeContainer.isotope($scope.isoMode, $element);
        });
      }
    };
    $scope.refreshIso = function() {
      if (postInitialized) {
        return isotopeContainer.isotope();
      }
    };
    $scope.updateOptions = function(option) {
      if (isotopeContainer) {
        isotopeContainer.isotope(option);
      } else {
        optionsStore.store(option);
      }
    };
    $scope.updateMethod = function(name, params, cb) {
      return isotopeContainer.isotope(name, params, cb);
    };
    optionsHandler = function(event, option) {
      return $scope.updateOptions(option);
    };
    methodHandler = function(event, option) {
      var name, params;
      name = option.name;
      params = option.params;
      return $scope.updateMethod(name, params, null);
    };

    $scope.removeAll = function(cb) {
      return isotopeContainer.isotope("remove", isotopeContainer.data("isotope").$allAtoms, cb);
    };
    $scope.refresh = function() {
      return isotopeContainer.isotope();
    };
    $scope.$on(config.refreshEvent, function() {
      return $scope.refreshIso();
    });
    $scope.$on(topics.MSG_REMOVE, function(message, element) {
      return $scope.removeElement(element);
    });
    $scope.$on(topics.MSG_OPTIONS, function(message, options) {
      return optionsHandler(message, options);
    });
    $scope.$on(topics.MSG_METHOD, function(message, opt) {
      return methodHandler(message, opt);
    });
    $scope.removeElement = function(element) {
      return isotopeContainer && isotopeContainer.isotope("remove", element);
    };
  }
])
.controller("isoSortByDataController", [
  "iso.config", "$scope", "optionsStore", function(config, $scope, optionsStore) {
    var getValue, reduce;
    $scope.getHash = function(s) {
      return "opt" + s;
    };
    $scope.storeMethods = function(methods) {
      return optionsStore.store({
        getSortData: methods
      });
    };
    $scope.optSortData = function(index, item) {
      var $item, elementSortData, fun, genSortDataClosure, selector, sortKey, type;
      elementSortData = {};
      $item = angular.element(item);
      selector = $item.attr("ok-sel");
      type = $item.attr("ok-type");
      sortKey = $scope.getHash(selector);
      fun = ($item.attr("opt-convert") ? eval_("[" + $item.attr("opt-convert") + "]")[0] : null);
      genSortDataClosure = function(selector, type, convert) {
        return function($elem) {
          return getValue(selector, $elem, type, convert);
        };
      };
      elementSortData[sortKey] = genSortDataClosure(selector, type, fun);
      return elementSortData;
    };
    $scope.createSortByDataMethods = function(elem) {
      var options, sortDataArray;
      options = $(elem);
      sortDataArray = reduce(options.map($scope.optSortData));
      return sortDataArray;
    };
    reduce = function(list) {
      var reduction;
      reduction = {};
      angular.forEach(list, function(item, index) {
        return angular.extend(reduction, item);
      });
      return reduction;
    };
    getValue = function(selector, $elem, type, evaluate) {
      var getText, item, text, toType, val;
      getText = function($elem, item, selector) {
        var text;
        if (!item.length) {
          return $elem.text();
        }
        text = "";
        switch (selector.charAt(0)) {
          case "#":
            text = item.text();
            break;
          case ".":
            text = item.text();
            break;
          case "[":
            text = item.attr(selector.replace("[", "").replace("]", "").split()[0]);
        }
        return text;
      };
      toType = function(text, type) {
        var numCheck, utility;
        numCheck = function(val) {
          if (isNaN(val)) {
            return Number.POSITIVE_INFINITY;
          } else {
            return val;
          }
        };
        utility = {
          text: function(s) {
            return s.toString();
          },
          integer: function(s) {
            return numCheck(parseInt(s, 10));
          },
          float: function(s) {
            return numCheck(parseFloat(s));
          },
          boolean: function(s) {
            return "true" === s;
          }
        };
        if (utility[type]) {
          return utility[type](text);
        } else {
          return text;
        }
      };
      item = $elem.find(selector);
      text = getText($elem, item, selector);
      val = toType(text, type);
      if (evaluate) {
        return evaluate(val);
      } else {
        return val;
      }
    };
  }
]);
angular.module("iso.directives", ["iso.config", "iso.services", "iso.controllers"]);

angular.module("iso.directives")
.directive("isotopeContainer", ["$injector", "$parse", function($injector, $parse) {
    "use strict";
    var options;
    options = {};
    return {
      controller: "angularIsotopeController",
      link: function(scope, element, attrs) {
        var isoInit, isoOptions, linkOptions;
        linkOptions = [];
        isoOptions = attrs.isoOptions;
        isoInit = {};
        if (isoOptions) {
          linkOptions = $parse(isoOptions)(scope);
          if (angular.isObject(linkOptions)) {
            scope.updateOptions(linkOptions);
          }
        }
        isoInit.element = element;
        isoInit.isoOptionsEvent = attrs.isoOptionsSubscribe;
        isoInit.isoMethodEvent = attrs.isoMethodSubscribe;
        isoInit.isoMode = attrs.isoMode;
        if (attrs.isoUseInitEvent === "true") {
          scope.delayInit(isoInit);
        } else {
          scope.init(isoInit);
        }
        return element;
      }
    };
  }
])
.directive("isotopeItem", [
  "$rootScope", "iso.config", "iso.topics", "$timeout", function($rootScope, config, topics, $timeout) {
    return {
      restrict: "A",
      require: "^isotopeContainer",
      link: function(scope, element, attrs) {

        scope.setIsoElement(element);
        scope.$on('$destroy', function(message) {
          $rootScope.$broadcast(topics.MSG_REMOVE, element);
        });
        if (attrs.ngRepeat && true === scope.$last && "addItems" === scope.isoMode) {
          element.ready(function() {
            return $timeout((function() {
              return scope.refreshIso();
            }), config.refreshDelay || 0);
          });
        }
        if (!attrs.ngRepeat) {
          element.ready(function() {
            return $timeout((function() {
              return scope.refreshIso();
            }), config.refreshDelay || 0);
          });          
        }
        return element;
      }
    };
  }
])
.directive("isoSortbyData", function() {
    return {
      restrict: "A",
      controller: "isoSortByDataController",
      link: function(scope, element, attrs) {
        var methSet, methods, optEvent, optKey, optionSet, options;
        optionSet = angular.element(element);
        optKey = optionSet.attr("ok-key");
        optEvent = "iso-opts";
        options = {};
        methSet = optionSet.find("[ok-sel]");
        methSet.each(function(index) {
          var $this;
          $this = angular.element(this);
          return $this.attr("ok-sortby-key", scope.getHash($this.attr("ok-sel")));
        });
        methods = scope.createSortByDataMethods(methSet);
        return scope.storeMethods(methods);
      }
    };
  }
)
.directive("optKind", ['optionsStore', 'iso.topics', function(optionsStore, topics) {
  return {
    restrict: "A",
    controller: "isoSortByDataController",
    link: function(scope, element, attrs) {
      var createSortByDataMethods, createOptions, doOption, emitOption, optKey, optPublish, methPublish, optionSet, determineAciveClass, activeClass, activeSelector, active;
      optionSet = $(element);
      optPublish = attrs.okPublish || attrs.okOptionsPublish || topics.MSG_OPTIONS;
      methPublish = attrs.okMethodPublish || topics.MSG_METHOD;
      optKey = optionSet.attr("ok-key");

      determineActiveClass = function() {
        activeClass = attrs.okActiveClass;
        if (!activeClass) {
          activeClass = optionSet.find(".selected").length ? "selected" : "active";
        }
        activeSelector = "." + activeClass;
        active = optionSet.find(activeSelector);
      };

      createSortByDataMethods = function(optionSet) {
        var methSet, methods, optKey, options;
        optKey = optionSet.attr("ok-key");
        if (optKey !== "sortBy") {
          return;
        }
        options = {};
        methSet = optionSet.find("[ok-sel]");
        methSet.each(function(index) {
          var $this;
          $this = angular.element(this);
          return $this.attr("ok-sortby-key", scope.getHash($this.attr("ok-sel")));
        });
        methods = scope.createSortByDataMethods(methSet);
        return scope.storeMethods(methods);
      };

      createOptions = function(item) {
        var ascAttr, key, option, virtualSortByKey;
        if (item) {
          option = {};
          virtualSortByKey = item.attr("ok-sortby-key");
          ascAttr = item.attr("opt-ascending");
          key = virtualSortByKey || item.attr("ok-sel");
          if (virtualSortByKey) {
            option.sortAscending = (ascAttr ? ascAttr === "true" : true);
          }
          option[optKey] = key;
          return option;
        }
      };

      emitOption = function(option) {
        optionsStore.store(option);
        return scope.$emit(optPublish, option);
      };

      doOption = function(event) {
        var selItem;
        event.preventDefault();
        selItem = angular.element(event.target);
        if (selItem.hasClass(activeClass)) {
          return false;
        }
        optionSet.find(activeSelector).removeClass(activeClass);
        selItem.addClass(activeClass);
        emitOption(createOptions(selItem));
        return false;
      };

      determineActiveClass();
      
      createSortByDataMethods(optionSet);

      if (active.length) {
        var opts = createOptions(active);
        optionsStore.store(opts);
      }

      return optionSet.on("click", function(event) {
        return doOption(event);
      });
    }
  };
}]);
angular.module("iso.services", ["iso.config"], [
  '$provide', function($provide) {
    return $provide.factory("optionsStore", [
      "iso.config", function(config) {
        "use strict";
        var storedOptions, delayedInit;
        storedOptions = config.defaultOptions || {};
        return {
          store: function(option) {
            angular.extend(storedOptions, option);
            return storedOptions;
          },
          retrieve: function() {
            return storedOptions;
          },
          storeInit: function(init) {
            delayedInit = init;
          },
          retrieveInit: function() {
            return delayedInit;
          }
        };
      }
    ]);
  }
])
.value('iso.topics', {
  MSG_OPTIONS:'iso-option',
  MSG_METHOD:'iso-method',
  MSG_REMOVE:'iso-remove'
});
})(window, document);
/*eslint-enable */
(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('pageHeader', pageHeader);

    /** @ngInject */
    function pageHeader() {
        PageHeaderController.$inject = ["$log", "$mdSidenav", "ModalService", "$iwSession", "$iwAccount", "$state", "$iwCart"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/header/header.html',
            scope: {
            },
            link: PageHeaderLink,
            controller: PageHeaderController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function PageHeaderLink(scope, iElement) {

            // Navigation bar
            var navbar = iElement.find('nav');

            // Navigation bar buttons width optional widget areas
            var buttons = iElement.find('nav .md-button-t');

            // Loop through each button and add handlers for widgets
            angular.forEach(buttons, function(value/*, index*/) {

                // Button
                var button = angular.element(value);

                // Widget area
                var widget = angular.element(button.next('.widget'));

                // If we have a widget area add click handler
                if(widget.length) {

                    // Add the dropdown arrow
                    button.append('<span class="toggle-icon"><i class="material-icons">&#xE313;</i></span>');

                    // Show / hide widget area
                    button.on('click', function() {
    
                        // Close open widgets
                        navbar.find('.open').not(button.parent()).removeClass('open').find('.widget').stop().slideUp();

                        // Show / hide this widget
                        widget.stop().slideToggle();

                        // Add / remove toggle class
                        widget.parent().toggleClass('open');
                    });
                }
            });


            // Outside header click then close widgets
            angular.element('body').on('click', function(e) {
                if(!angular.element(e.target).closest(iElement).length) {
                    navbar.find('.open').removeClass('open').find('.widget').slideUp();
                }
            });
        }

        /** @ngInject */
        function PageHeaderController($log, $mdSidenav, ModalService, $iwSession, $iwAccount, $state, $iwCart) {
            var vm = this;

            vm.cart = $iwCart.getCart();
            vm.menuOpen = false;
            vm.togglePlayslip = togglePlayslip;
            vm.toggleMenu = toggleMenu;

            // Get session object
            vm.session = $iwSession.getSession();
            vm.account = $iwAccount.getAccount();

            // Exposed methods
            vm.login = login;
            vm.logout = logout;
            vm.register = register;

            function toggleMenu() {
                vm.menuOpen = !vm.menuOpen;
            }

            function togglePlayslip() {
                $mdSidenav('mobile-payslip').toggle();
            }

            function register(/*ev*/) {
                ModalService.register().then(registerSuccess, registerFailed);

                function registerSuccess(resp) {
                    $log.log('Reg success', resp);
                }

                function registerFailed() {
                    //$log.log('Reg failed', resp);
                }
            }

            function login(/*ev*/) {
                // Open the login modal
                ModalService.login().then(loginSuccess, loginFailed);

                function loginSuccess() {
                    $state.go('home');
                }

                function loginFailed() {}
            }

            function logout(/*ev*/) {
                $iwAccount.logout();
                $state.go('play');
            }
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('pageFooter', slFooter);

    /** @ngInject */
    function slFooter() {
        FooterController.$inject = ["$log"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/footer/footer.html',
            scope: {
            },
            controller: FooterController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        /** @ngInject */
        function FooterController($log) {
            var vm = this;

            vm.chat = chat;

            function chat() {
                $log.log('Chat button clicked');
            }
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('feedbackMessage', feedbackMessage);

    /** @ngInject */
    function feedbackMessage() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/feedback-message/feedback-message.html',
            scope: {
                type: '='
            },
            link: FeedbackMessageLink,
            transclude: true,
            replace: true,
            controller: FeedbackMessageController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function FeedbackMessageLink(/*scope, iElement, iAttrs, controller, transclude*/) {

        }

        /** @ngInject */
        function FeedbackMessageController() {
            //var vm = this;
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('countDownCircle', countDownCircle);

    /** @ngInject */
    function countDownCircle() {
        CountDownController.$inject = ["$interval", "$log", "moment"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/count-down-circle/count-down-circle.html',
            scope: {
                endDate: '@'
            },
            controller: CountDownController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function CountDownController($interval, $log, moment) {
            var vm = this;

            vm.maxProgress = 59;
            vm.currentProgress = 0;
            vm.currentText = '-';

            // End date in a moment object
            var endDate = moment(new Date(vm.endDate));

            // Update time and progress every second
            $interval(function() {
                // Get the difference from now and the end date
                var difference = endDate.diff(moment());

                // Get the duration
                var duration = moment.duration(difference);

                // We bit hacky, please chance this
                // vm.currentText = duration.humanize().replace(/[0-9]/g, '');

                // Show days
                if(duration.days() > 0) {
                    vm.maxProgress = 7;
                    vm.currentProgress = duration.days();
                    vm.currentText = duration.days() > 1 ? 'days' : 'day';
                }

                // Show hours
                else if(duration.hours() > 0) {
                    vm.maxProgress = 24;
                    vm.currentProgress = duration.hours();
                    vm.currentText = duration.hours() > 1 ? 'hours' : 'hour';
                }

                // Show minutes
                else if(duration.minutes() > 0) {
                    vm.maxProgress = 59;
                    vm.currentProgress = duration.minutes();
                    vm.currentText = duration.minutes() > 1 ? 'minutes' : 'minute';
                }

                // Show seconds
                else if(duration.seconds() > 0) {
                    vm.maxProgress = 59;
                    vm.currentProgress = duration.seconds();
                    vm.currentText = duration.seconds() > 1 ? 'seconds' : 'second';
                }
                else {
                    vm.maxProgress = 1;
                    vm.currentProgress = 1;
                    vm.currentText = '-';
                }
            }, 1000);
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('contentCarousel', contentCarousel);

    /** @ngInject */
    function contentCarousel() {
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/content-carousel/content-carousel.html',
            template: '<slick settings="vm.carouselConfig"></slick>',
            scope: {
                settings: '='
            },
            link: ContentCarousellLink,
            transclude: true,
            controller: ContentCarouselController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function ContentCarousellLink(scope, iElement, iAttrs, controller, transclude) {

            // Add class
            iElement.addClass('content-carousel');

            // Add the transcluded content
            iElement.find('slick').append(transclude());
        }

        /** @ngInject */
        function ContentCarouselController() {
            var vm = this;

            // Carousel settings
            vm.carouselConfig = {
                prevArrow: '<button class="slick-prev"><i class="material-icons">&#xE314;</i></button>',
                nextArrow: '<button class="slick-next"><i class="material-icons">&#xE315;</i></button>',
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                dots: false
            };

            // Combine user settings with our default settings
            angular.extend(vm.carouselConfig, vm.settings);
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('cartButton', CartButton);

    /** @ngInject */
    function CartButton() {
        CartButtonLink.$inject = ["scope", "iElement"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/cart-button/cart-button.html',
            scope: {
                direction: '@',
                scrollTo: '@'
            },
            link: CartButtonLink,
            controller: CartButtonController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function CartButtonLink(scope, iElement/*, iAttrs, controller, transclude*/) {

            var dropdown = iElement.find('.dropdown-content');

            iElement.on('click', function() {
                dropdown.toggle();
            });


            // Outside header click then close widgets
            angular.element('body').on('click', function(e) {
                if(!angular.element(e.target).closest(iElement).length) {
                    dropdown.hide();
                }
            });
        }

        function CartButtonController($iwCart) {
            var vm = this;
            vm.cart = $iwCart.getCart();
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('cashier', cashier);

    /** @ngInject */
    function cashier() {
        CashierController.$inject = ["$log", "$scope"];
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/cashier/cashier.html',
            scope: {
            },
            link: CashierLink,
            controller: CashierController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function CashierLink(/*scope, iElement, iAttrs*/) {

        }

        /** @ngInject */
        function CashierController($log,$scope) {
            //var vm = this;
            $log.log('Cashier loaded');
            $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
            $scope.card = {
              number: "0000 0000 0000 xxxx",
              expiry : {
                month : $scope.months[3],
                date : new Date()
              },
              cvv : 215,
              cardholder : "Mr john smith"
            };
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('arrowBar', ArrowBar);

    /** @ngInject */
    function ArrowBar() {
        ArrowBarLink.$inject = ["scope", "iElement", "iAttrs", "controller", "transclude"];
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/arrow-bar/arrow-bar.html',
            template: '<span class="arrow-bar-arrow"></span>',
            scope: {
                direction: '@',
                scrollTo: '@'
            },
            link: ArrowBarLink,
            transclude: true
        };

        return directive;

        /** @ngInject */
        function ArrowBarLink(scope, iElement, iAttrs, controller, transclude) {

            // Add the transcluded content
            iElement.prepend(transclude());

            // Arrow
            var arrow = iElement.find('.arrow-bar-arrow');

            // Target scroll element
            var target = iAttrs.scrollTo ? angular.element(iAttrs.scrollTo) : false;

            // If the string 'top' was passed then scroll to top of page
            if(iAttrs.scrollTo && iAttrs.scrollTo === 'top') {
                target = angular.element('body');
            }

            // If scroll target exists attach a click handler
            if(target && target.length) {
                arrow.addClass('action');
                arrow.bind('click', function() {
                    angular.element('html,body').animate({ scrollTop: target.offset().top }, 'slow');
                });
            }
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('accountInfo', accountInfo);

    /** @ngInject */
    function accountInfo() {
        AccountInfoController.$inject = ["$scope"];
        var directive = {
            restrict: 'EA',
            controller: AccountInfoController
        };

        return directive;

        /** @ngInject */
        function AccountInfoController($scope) {

            $scope.firstName = 'Ben';
            $scope.lastName = 'Thomson';
            $scope.fullName = $scope.firstName + ' ' + $scope.lastName;
            $scope.loggedIn = true;
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('accountButton', accountButton);

    /** @ngInject */
    function accountButton() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/account-button/account-button.html',
            scope: {
                icon: '@'
            },
            transclude: true
            //controller: AccountButtonController,
            //controllerAs: 'vm',
            //bindToController: true
        };

        return directive;

        // function AccountButtonController($log) {
        //     var vm = this;
        // }
    }

})();

(function() {

    getPrizeBreakdown.$inject = ["$http", "$q", "SingleLineProducts", "$log"];
    angular
        .module('sevenLottos')
        .service('$getPrizeBreakdown', getPrizeBreakdown);

    //get prizes bt draw ID
    function getPrizeBreakdown($http, $q, SingleLineProducts, $log) {
        var getPrizes = function(drawDate, drawId) {
            var deferred = $q.defer();
            var data = {
                "startDate": drawDate,
                "endDate": drawDate,
                "addAuth" : false
            };

            SingleLineProducts.drawDetails(drawDate, drawDate)
            .then(function (prizes) {
                //find the prizes where draw id matches the one we are looking for
                var prizeForDraw = prizes.filter(function(obj) {
                    return obj.drawingId == drawId;
                })[0];
                deferred.resolve(prizeForDraw); // resolve
            },function (error) {
                //console.log('error getting prizes ', error);
                deferred.reject(error); //reject
            });

            //promise
            return deferred.promise;
        };

        return {
            getPrizes: getPrizes
        };
    }

})();

(function() {

  getLotteryHistory.$inject = ["$http", "$q", "SingleLineProducts"];
  angular
    .module('sevenLottos')
    .service('$getLotteryHistory', getLotteryHistory);

  //get prizes bt draw ID
  function getLotteryHistory($http, $q, SingleLineProducts) {
    var getHistoryForLottery = function(name) {
      var deferred = $q.defer();
      SingleLineProducts.results().then(function(history) {
        //console.log('service ----- results', history);
        angular.forEach(history, function(data) {
          //console.log('arrays', data);
          var lotteryHistoryArray = data.filter(function(obj) {
            return obj.lotteryName == name;
          });
          //console.log(lotteryHistoryArray);
          if (lotteryHistoryArray.length > 0) {
            deferred.resolve(lotteryHistoryArray);
          }
        });
      });
      return deferred.promise;
    };

    var getBreakDown = function(name, drawDate) {
      var deferred = $q.defer();
      var endData = {
        history: [],
        index: ''
      };
      getHistoryForLottery(name).then(function(history) {
        //console.log('asdasdsadsdasdsadsasa-asd-s-s-das-dsa-d-asd-s', history, drawDate);
        endData.history = history;
        history.filter(function(obj,index) {
          if(obj.drawDateTime === drawDate){
            endData.index = index;
          }
        });
        deferred.resolve(endData);
      });
      return deferred.promise;
    };

    return {
      getHistoryForLottery: getHistoryForLottery,
      getBreakDown: getBreakDown
    };
  }
})();

(function() {
    'use strict';

    ModalService.$inject = ["$q", "$log", "$mdDialog", "ngDialog", "SingleLineProducts", "SyndicateProducts"];
    angular
        .module('sevenLottos')
        .service('ModalService', ModalService);

    /** @ngInject */
    function ModalService($q, $log, $mdDialog, ngDialog, SingleLineProducts, SyndicateProducts) {
        // var vm = this;
        
        /**
         * Modal service
         * 
         * @type {Object}
         */
        var service = {
            login: login,
            register: register,
            singleLine: singleLine,
            syndicate: syndicate,

            showLogin: login,
            showRegister: register
        };
        return service;

        /**
         * Show login
         */
        function login(/*ev*/) {
            var deferred = $q.defer();

            var loginDialog = ngDialog.open({
                name: 'loginModal',
                template: 'app/modals/login/login.html',
                appendClassName: 'login-modal',
                controller: 'LoginModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            loginDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show register
         */
        function register(/*ev*/) {
            var deferred = $q.defer();

            var registerDialog = ngDialog.open({
                name: 'loginModal',
                template: 'app/modals/register/register.html',
                appendClassName: 'register-modal',
                controller: 'RegisterModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            registerDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Single Line
         */
        function singleLine(id) {
            $log.log('Showing single line', id);
            var deferred = $q.defer();

            // Fetch the product
            var product = SingleLineProducts.get(id);

            // Check if we have the product
            if(product) {
                var singleLineDialog = ngDialog.open({
                    name: 'singleLineModal',
                    template: 'app/modals/singleline/singleline.html',
                    appendClassName: 'game single-line-modal',
                    controller: 'SingleLineModalController',
                    controllerAs: 'vm',
                    closeByDocument: false,
                    data: {
                        productId: id,
                        product: product
                    }
                });

                singleLineDialog.closePromise.then(function (data) {
                    if(data.value === true) {
                        deferred.resolve(data);
                    }
                    else {
                        deferred.reject(data);
                    }
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }

        /**
         * Syndicate
         */
        function syndicate(id/*, ev*/) {
            $log.log('Showing syndicate', id);
            var deferred = $q.defer();

            // Fetch the syndicate
            var syndicate = SyndicateProducts.get(id);

            $log.log('Found', syndicate);

            // Check if we have the syndicate
            if(syndicate) {
                var syndicateDialog = ngDialog.open({
                    name: 'loginModal',
                    template: 'app/modals/syndicate/syndicate.html',
                    appendClassName: 'game syndicate-modal',
                    controller: 'SyndicateModalController',
                    controllerAs: 'vm',
                    closeByDocument: false,
                    data: {
                        productId: id,
                        product: syndicate
                    }
                });

                syndicateDialog.closePromise.then(function (data) {
                    if(data.value === true) {
                        deferred.resolve(data);
                    }
                    else {
                        deferred.reject(data);
                    }
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }
    }

})();
(function() {

  angular
    .module('sevenLottos')
    .filter('pagination', Pagination);

  function Pagination() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    };
  }
})();

(function() {
  'use strict';

  angular
    .module('insightWebAngular')
    .filter('formatTime', formatTimeFilter);

  function formatTimeFilter() {
    return function(lottery) {
      var date = new Date(lottery.nextDrawDateTimeList[lottery.nextDrawDateTimeList.length - 1]);
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
      var dd = date.getDate().toString();
      return yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
    };
  }
})();

(function() {

  angular
    .module('sevenLottos')
    .filter('formatDate', formatDateFromDate);

  function formatDateFromDate() {
    return function(input) {
      if (input) {
        var date = new Date(input);
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = date.getDate().toString();
        var hours = date.getHours().toString();
        if (hours < 10) {
          hours = 0 + hours;
        }
        var min = date.getMinutes().toString();
        if (min < 10) {
          min = 0 + min;
        }
        var sec = date.getSeconds().toString();
        if (sec < 10) {
          sec = 0 + sec;
        }
        return (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy + ' , ' + hours + ':' + min + ':' + sec; // padding
      } else {
        return ' - ';
      }
    };
  }
})();

(function() {
    'use strict';

    runBlock.$inject = ["$log", "$rootScope", "$mdSidenav", "tmhDynamicLocale", "$iwSession"];
    angular
        .module('sevenLottos')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $rootScope, $mdSidenav, tmhDynamicLocale, $iwSession) {
        $log.debug('runBlock end');

        // tmhDynamicLocale.set('da-dk');
        //tmhDynamicLocale.set($iwSession.getLanguage());
        //$log.log( $iwSession.getSession() );

        $rootScope.$on('$stateChangeSuccess', function() {
            $mdSidenav('mobile-payslip').close();
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            //$document.find('body').eq(0).scrollTop = $document.documentElement.scrollTop = 0;
        });
    }

})();

(function() {
    'use strict';

    routerConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    angular
        .module('sevenLottos')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/pages/home/home.html',
                controller: 'HomePageController',
                controllerAs: 'homePage'
            })
            .state('play', {
                url: '/play-lottery/:productId',
                templateUrl: 'app/pages/play/play.html',
                controller: 'PlayPageController',
                controllerAs: 'playPage'
            })
            .state('results', {
                url: '/lottery-results',
                templateUrl: 'app/pages/results/results.html',
                controller: 'ResultsPageController',
                controllerAs: 'ResultsPage'
            })
            .state('lotteryresults', {
                url: '/lottery-results/:name',
                templateUrl: 'app/pages/lotteryHsitoryResult/lottery-results.html',
                controller: 'LotteryHistoryResult',
                params: {name: null, history : null },
                controllerAs: 'lotteryResult'
            })
            .state('prizebreakdown', {
                url: '/lottery-results/:name/:drawDate',
                templateUrl: 'app/pages/lotteryDrawPrizebreakdown/prize-breakdown.html',
                params: {name: null, drawDate: null,index:null,history:null},
                controller: 'LotteryPrizeBreakdown',
                controllerAs: 'prizeCtrl'
            })
            .state('how', {
                url: '/how-to-play',
                templateUrl: 'app/pages/how/how.html',
                controller: 'HowPageController',
                controllerAs: 'howPage'
            })
            .state('contact', {
                url: '/contact-us',
                templateUrl: 'app/pages/contact/contact.html',
                controller: 'ContactPageController',
                controllerAs: 'contactPage'
            })
            .state('account', {
                url: '/account',
                templateUrl: 'app/pages/account/account.html',
                controller: 'AccountPageController',
                controllerAs: 'AccountPage'
            })
            .state('checkout', {
                url: '/checkout?txnId',
                templateUrl: 'app/pages/checkout/checkout.html',
                controller: 'CheckoutPageController',
                controllerAs: 'vm'
            });

        $urlRouterProvider.otherwise('/');
    }

})();

/* global moment:false */
(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .constant('moment', moment)
        .constant('APP_CONFIG', {
            domain: '7lottos.com',
            brand: '7lottos',
            frontEnd: '7lottos',
            frontEndId: 1,
            defaultLanguage: 'en',
            defaultCurrency: 'EUR'
        });

})();

(function() {
    'use strict';

    config.$inject = ["$logProvider", "ngDialogProvider", "$mdThemingProvider", "tmhDynamicLocaleProvider"];
    angular
        .module('sevenLottos')
        .config(config);

    /** @ngInject */
    function config($logProvider, ngDialogProvider, $mdThemingProvider, tmhDynamicLocaleProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // ngDialog settings
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-7lotto',
            setOpenOnePerName: true
        });
        ngDialogProvider.setOpenOnePerName(true);

        //tmhDynamicLocaleProvider.localeLocationPattern('/base/node_modules/angular-i18n/angular-locale_{{locale}}.js');
        tmhDynamicLocaleProvider.localeLocationPattern('http://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.8/angular-locale_{{locale}}.js');
        // https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.8/angular-locale_cy.min.js

        // Configure the theme
        $mdThemingProvider.theme('default')
            .primaryPalette('blue', {
                'default': '500'
            })
            .accentPalette('deep-orange', {
                'default': '500'
            })
            .warnPalette('red');
    }

})();

//# sourceMappingURL=../maps/scripts/app-2ab64d6d75.js.map
