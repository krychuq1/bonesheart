(function() {
    'use strict';

    angular
        .module('bt.owl-carousel', []);

})();

(function() {
    'use strict';

    angular
        .module('bt.owl-carousel')
        .directive('owlSlide', owlSlide);

    /* @ngInject */
    function owlSlide() {
        var directive = {
            require: ['^owlCarousel'],
            restrict: 'AE',
            link: link
        };
        return directive;

        function link(scope, element, attrs, controller) {

            // Carousel controller
            var carouselCtrl = controller[0];

            // Register the slide with parent
            carouselCtrl.addSlide(scope, element);

            // On slide destory de-register slide
            scope.$on('$destroy', function() {
                carouselCtrl.removeSlide(scope, element);
            });
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('bt.owl-carousel')
        .factory('OwlCarouselService', OwlCarouselService);

    /** @ngInject */
    function OwlCarouselService() {
        //var vm = this;

        /**
         * Carousel instances
         * 
         * @type {Array}
         */
        var instances = [];

        var service = {
            register: register,
            get: get,
            find: get,
            getInstances: getInstances
        };
        return service;

        /**
         * Register the carousel instance
         * 
         * @param  {Object} instance    Carousel instance
         * @param  {String} id          Handle id
         * @return {Function}           De-register function
         */
        function register(instance, id) {

            // Add a handle id
            instance.$handle = id;

            // Add to instance list
            instances.push(instance);

            // Return de-register function
            return deregister;

            /**
             * Remove registration of instance
             */
            function deregister() {
                var index = instances.indexOf(instance);
                if (index > -1) {
                    instances.splice(index, 1);
                }
            }
        }

        /**
         * Get an instance of the carousel
         * 
         * @param  {String}     handle  Handle id to look for
         * @return {Objet|null}         Object if found or null if not found
         */
        function get(handle) {

            // Instsance (initially null)
            var instance = null;

            // Loop through each instance and look for the handle
            angular.forEach(instances, function(_instance) {
                if(_instance.$handle === handle) {
                    instance = _instance;
                }
            });

            // Return instance
            return instance;
        }

        /**
         * Return all registered instances
         * 
         * @return {Array}      Array of Owl Carousel instances
         */
        function getInstances() {
            return instances;
        }
    }

})();

(function() {
    'use strict';

    OwlCarouselController.$inject = ["$attrs", "OwlCarouselService"];
    angular
        .module('bt.owl-carousel')
        .directive('owlCarousel', owlCarousel);

    /* @ngInject */
    function owlCarousel() {
        var directive = {
            bindToController: true,
            controller: OwlCarouselController,
            controllerAs: 'vm',
            link: link,
            restrict: 'AE',
            scope: {
                settings: '=?',
                owlId: '@'
            },
            transclude: true
        };
        return directive;

        function link(scope, element, attrs, controller, transclude) {

            // Owl carousel object
            var owl;

            // Options
            var options = scope.vm.settings || {};

            // Current page (used for re-initialisation)
            var currentPage = 0;

            // Initialised flag
            var initialised = false;

            // Append the transcluded content
            angular.element(element).append(transclude());

            // Watch for settings changed and re-initialise the carousel
            scope.$watch(function() { return scope.vm.settings }, function(newSettings, oldSettings) {
                if(newSettings !== oldSettings) {
                    reInit();
                }
            }, true);

            // Watch for changed in slides and re-initialise the carousel
            scope.$watchCollection(function() { return scope.vm.slides }, function(newSlides, oldSlides) {
                if(newSlides.length > 0 && newSlides !== oldSlides) {
                    reInit();
                }
            });

            // Publish special accessor for the controller instance
            controller.init = init;
            controller.destory = destory;
            controller.reInit = reInit;
            controller.trigger = trigger;

            // Destroy handling
            scope.$on('$destroy', function() {
                controller.destory;
                destory();
            });

            /**
             * Initialise the carousel
             * 
             * @return {void}
             */
            function init() {
                // If the slider has been initialised before already then add the start position to previous one
                if(initialised) {
                    options.startPosition = currentPage;
                }

                // Initialise the carousel
                owl = element.owlCarousel(options);

                // Add owl carousel class
                element.addClass('owl-carousel');

                // On the change event of the carousel then set the current page
                owl.on('changed.owl.carousel', function(event) {
                    currentPage = event.item.index;
                });

                // Set initial initialised flag to true
                initialised = true;
            }

            /**
             * Destroy the carousel
             * 
             * @return {void}
             */
            function destory() {
                // Destroy the carousel
                if(owl) {
                    owl.trigger('destroy.owl.carousel');
                }

                // Remove owl carousel class
                element.removeClass('owl-carousel');

                // Remove remaining children
                // @TODO: Better method for this. We need to destroy the
                //        carousel before the ng-repeat renders again
                element.find('.owl-item').remove();
            }

            /**
             * Re-initialise the carousel
             * 
             * @return {void}
             */
            function reInit() {
                destory();
                init();
            }

            /**
             * Trigger an Owl Carousel event
             * 
             * @param  {String} event  Event
             * @param  {Array} params Optional parameters
             * @return {void}
             */
            function trigger(event, params) {
                if(angular.isDefined(params)) {
                    owl.trigger(event, params);
                }
                else {
                    owl.trigger(event);
                }
            }
        }
    }

    /* @ngInject */
    function OwlCarouselController($attrs, OwlCarouselService) {
        var vm = this;

        // Array of slides
        vm.slides = [];

        // Public methods
        vm.addSlide = addSlide;
        vm.removeSlide = removeSlide;

        // Re-register component id ID changes
        $attrs.$observe('owlId', function(id) {
            if(id && id !== vm.$handle) {
                vm.destroy();
                vm.destroy = OwlCarouselService.register(vm, id);
            }
        });

        // Register the carousel
        vm.destroy = OwlCarouselService.register(vm, vm.owlId);

        /**
         * Add a slide to the carousel list
         * 
         * @param {Object} slideScope   Slide scope
         * @param {Object} element      Slide element
         */
        function addSlide(slideScope, element) {
            vm.slides.push({
                scope: slideScope,
                element: element
            });
        }

        /**
         * Remove a slide
         * 
         * @param  {Object} slideScope Slide scope
         * @return {void}
         */
        function removeSlide(slideScope) {
            var index = findSlideIndex(slideScope);
            if(index > -1) {
                vm.slides.splice(index, 1);
            }
        }

        /**
         * Find an index of a slide
         * 
         * @param  {Object} slide Slide scope
         * @return {Number}       Slide index
         */
        function findSlideIndex(slide) {
            for (var i = 0; i < vm.slides.length; i++) {
                if (vm.slides[i].scope === slide) {
                    return i;
                }
            }
        }
    }

})();
//(function() {
//    'use strict';
//
//    angular
//        .module('bt.owl-carousel')
//        .run(runBlock);
//
//    /** @ngInject */
//    function runBlock($log) {
//        $log.debug('runBlock end');
//    }
//
//})();

// (function() {
//     'use strict';

//     angular
//         .module('bt.owl-carousel');

// })();

//(function() {
//    'use strict';
//
//    angular
//        .module('bt.owl-carousel')
//        .config(config);
//
//        /** @ngInject */
//        function config($logProvider) {
//            // Enable log
//            $logProvider.debugEnabled(true);
//        }
//
//})();

//# sourceMappingURL=bt-owl-carousel.js.map
