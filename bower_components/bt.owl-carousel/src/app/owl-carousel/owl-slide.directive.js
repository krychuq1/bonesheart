(function() {
    'use strict';

    angular
        .module('bt.owl-carousel')
        .directive('owlSlide', owlSlide);

    /* @ngInject */
    function owlSlide() {
        var directive = {
            require: ['^owlCarousel'],
            restrict: 'AE',
            link: link
        };
        return directive;

        function link(scope, element, attrs, controller) {

            // Carousel controller
            var carouselCtrl = controller[0];

            // Register the slide with parent
            carouselCtrl.addSlide(scope, element);

            // On slide destory de-register slide
            scope.$on('$destroy', function() {
                carouselCtrl.removeSlide(scope, element);
            });
        }
    }

})();