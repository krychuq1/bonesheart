(function() {
    'use strict';

    angular
        .module('bt.owl-carousel')
        .factory('OwlCarouselService', OwlCarouselService);

    /** @ngInject */
    function OwlCarouselService() {
        //var vm = this;

        /**
         * Carousel instances
         * 
         * @type {Array}
         */
        var instances = [];

        var service = {
            register: register,
            get: get,
            find: get,
            getInstances: getInstances
        };
        return service;

        /**
         * Register the carousel instance
         * 
         * @param  {Object} instance    Carousel instance
         * @param  {String} id          Handle id
         * @return {Function}           De-register function
         */
        function register(instance, id) {

            // Add a handle id
            instance.$handle = id;

            // Add to instance list
            instances.push(instance);

            // Return de-register function
            return deregister;

            /**
             * Remove registration of instance
             */
            function deregister() {
                var index = instances.indexOf(instance);
                if (index > -1) {
                    instances.splice(index, 1);
                }
            }
        }

        /**
         * Get an instance of the carousel
         * 
         * @param  {String}     handle  Handle id to look for
         * @return {Objet|null}         Object if found or null if not found
         */
        function get(handle) {

            // Instsance (initially null)
            var instance = null;

            // Loop through each instance and look for the handle
            angular.forEach(instances, function(_instance) {
                if(_instance.$handle === handle) {
                    instance = _instance;
                }
            });

            // Return instance
            return instance;
        }

        /**
         * Return all registered instances
         * 
         * @return {Array}      Array of Owl Carousel instances
         */
        function getInstances() {
            return instances;
        }
    }

})();
