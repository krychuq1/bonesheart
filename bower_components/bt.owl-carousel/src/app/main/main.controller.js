(function() {
    'use strict';

    angular
        .module('bt.owl-carousel')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($log) {
        var vm = this;

        $log.log('Main controller loaded');

        vm.slides = [
            {
                name: 'Ben'
            },
            {
                name: 'Thomson'
            },
            {
                name: 'Was'
            },
            {
                name: 'Ere'
            },
            {
                name: 'Today'
            },
            {
                name: ':O'
            }
        ];
    }

})();
