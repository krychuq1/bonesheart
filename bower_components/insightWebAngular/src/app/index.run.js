(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $iwSession, $iwProducts, $iwCart) {
        $log.debug('runBlock end');

        // Initialise the session
        $iwSession.init();

        // 
        $iwProducts.init();

        //
        $iwCart.init();
    }

})();
