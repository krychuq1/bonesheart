/* global moment:false, WirecardHPP:false, _iwBaseUrls:false */
(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .constant('moment', moment)
        .constant('WirecardHPP', WirecardHPP)
        .constant('AUTH_EVENTS', {
            loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated'
        })
        .constant('IWConfig', angular.isDefined(window.IWConfig) ? window.IWConfig : {});

})();