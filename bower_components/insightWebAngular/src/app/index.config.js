(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .config(config);

    /** @ngInject */
    function config($logProvider, $httpProvider) {
        // Enable log
        $logProvider.debugEnabled(true);


        // Add API interceptor
        $httpProvider.interceptors.push(['$injector', function ($injector) {
            return $injector.get('insightApiInterceptor');
        }]);
    }

})();
