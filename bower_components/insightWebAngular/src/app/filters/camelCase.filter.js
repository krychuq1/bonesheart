(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .filter('camelCase', camelCaseFilter);

    function camelCaseFilter() {
        return function(input) {
            return input.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
                return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
            }).replace(/\s+/g, '');
        }
    }

})();
