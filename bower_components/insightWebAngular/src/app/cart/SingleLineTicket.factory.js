(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('SingleLineTicket', SingleLineTicket);

    /** @ngInject */
    function SingleLineTicket($log, IWConstants) {
        var vm = this;

        /**
         * Event hooks
         *
         * @type {Object}
         */
        var eventHooks = {
            onTotalPriceChange: []
        };

        /**
         * Internal single line data
         *
         * @type {Object}
         */
        vm.lotteryData = {};

        /**
         * New instance of single line ticket for cart
         *
         * @param {Object} lottery              Lottery data
         * @param {Array}  lines                Array of lines
         * @param {Array}  drawDays             Array of days to play (1 = Monday, 2 = Tuesday etc...)
         * @param {String} playingPeriod        Playing period CONSTANT
         * @param {Number} playingPeriodLength  Number of weeks to play for
         * @param {String} billingPeriod        Billing period CONSTANT
         */
        function SingleLineTicket(lottery, lines, drawDays, playingPeriod, playingPeriodLength, billingPeriod) {
            this.name                   = false;
            this.productId              = 0;
            this.drawTypeId             = 0;
            this.lines                  = [];
            this.drawDays               = drawDays || [];
            this.linePrice              = 0;
            this.playingPeriod          = IWConstants.PLAYING_PERIODS.SUBSCRIPTION;
            this.playingPeriodLength    = 1;
            this.billingPeriod          = IWConstants.BILLING_PERIODS.MONTHLY;
            this.productType            = IWConstants.PRODUCT_TYPES.SINGLE_LINE;
            this.subTotal               = 0;
            this.currency               = IWConstants.APP.defaultCurrency;

            // Set interanl single line data
            if(angular.isDefined(lottery)) {
                this.setData(lottery);
            }

            // Set lines
            if(angular.isDefined(lines)) {
                this.setLines(lines);
            }

            // Set draw days
            if(angular.isDefined(drawDays)) {
                this.setDrawDays(drawDays);
            }

            // Set playing period
            if(angular.isDefined(playingPeriod)) {
                this.setPlayingPeriod(playingPeriod, playingPeriodLength);
            }

            // Set billing period
            if(angular.isDefined(billingPeriod)) {
                this.setBillingPeriod(billingPeriod);
            }
        }

        /**
         * Ticket bonus init
         *
         * @return {Void}
         */
        SingleLineTicket.prototype.bonus = {};

        /**
         * Sets Ticket bonus
         *
         * @return {Void}
         */
        SingleLineTicket.prototype.setBonus = function (bonus) {
          this.bonus = bonus;
        };

        /**
         * Fets Ticket bonus
         *
         * @return {Object} ticket's bonus
         */
        SingleLineTicket.prototype.getBonus = function () {
          return this.bonus;
        };

        /**
         * Calculates subtotal depending on bonus applied
         *
         * @return {Number} ticekts new price
         */
        SingleLineTicket.prototype.calculateDiscount = function (total) {
          var bonus = this.getBonus();
          var newTotal = 0;
          // if bonus is product specific
          if(bonus.awardtype === 'subscription'){
            newTotal = total - bonus.discountAmount;
          } else if(bonus.awardtype === 'discount' && bonus.awardValueType === 'pct'){ // if percentage discount
              newTotal = bonus.chargeAmount;
          }else if(bonus.awardtype === 'discount' && bonus.awardValueType === 'abs'){ // if absolute value discount
              newTotal = total - (bonus.discountAmount / 100) <= 0 ? 0 : total - bonus.discountAmount / 100;
          }

          return newTotal;
        };

        /**
         * Calculate sub total of ticket
         *
         * @return {Number} Sub total
         */
        SingleLineTicket.prototype.getSubTotal = function() {

            // Billing period multiplier
            var billingMultiplier = 1;

            // Calculate the billing price for subscriptions
            if(this.playingPeriod == IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {
                if(this.billingPeriod == IWConstants.BILLING_PERIODS.MONTHLY) {
                    // 4 weeks
                    billingMultiplier = 4;
                }
                else if(this.billingPeriod == IWConstants.BILLING_PERIODS.QUARTERLY) {
                    // 4 weeks by 3 months
                    billingMultiplier = 12;
                }
            }

            // Calculate initial total
            var total = (this.lines.length * this.linePrice * this.drawDays.length) * this.playingPeriodLength * billingMultiplier;

            // Set total to 0 if play days is 0
            total = (this.drawDays.length === 0 || this.lines.length === 0) ? 0 : total;

            // Set internal total
            if(this.bonus.userId){
              total = this.calculateDiscount(total);
            }

            this.subTotal = total;

            // Trigger event hooks
            onTotalPriceChangeHandler(this);

            // Return total
            return total;
        };

        /**
         * Set the lottery lines
         *
         * @param {Array} lines     Lines of numbers
         */
        SingleLineTicket.prototype.setLines = function(lines) {
            // Valid lines
            var validLines = [];

            // Loop through and validate all the lines
            angular.forEach(lines, function(line) {
                var lineValid = validateLineFormat(line);
                if(lineValid) {
                    validLines.push(lineValid);
                }
            });

            // Now if we have valid lines at the end then set the new array
            if(validLines.length > 0) {
                this.lines = validLines;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Add a line of numbers to the ticket
         *
         * @param {Object} line     Line object with primary and secondary numbers
         */
        SingleLineTicket.prototype.addLine = function(line) {
            $log.log('Adding lines', line);
            // Validate the line
            var validLine = validateLineFormat(line);

            $log.log(validLine);

            // If valid add to our list
            if(validLine) {
                this.lines.push(validLine);
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Remove a line of numbers
         *
         * @param  {Number} index Index of line
         */
        SingleLineTicket.prototype.removeLine = function(index) {
            var item = this.lines.splice(index, 1)[0] || {};

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Empty the lines of numbers on ticket
         */
        SingleLineTicket.prototype.emptyLines = function() {
            this.lines = [];

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set the days to play the lottery
         *
         * @param {Array} playingDays Array of days to play
         */
        SingleLineTicket.prototype.setDrawDays = function(drawDays) {

            // List of valid days
            var days = [];

            // Loop through each day and make sure its valid
            angular.forEach(drawDays, function(day) {
                if(angular.isNumber(day) && day <= 7) {
                    days.push(day);
                }
            });

            // Set the days
            this.drawDays = days;

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Toggle the playing day by value
         *
         * @param  {Number} day Number of day (1 = monday etc...)
         */
        SingleLineTicket.prototype.toggleDrawDay = function(day) {

            // Find the index of the day
            var index = this.drawDays.indexOf(day);

            // Toggle the day
            if(index > -1) {
                var item = this.drawDays.splice(index, 1);
            }
            else {
                if(day <= 7) {
                    this.drawDays.push(day);
                }
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set the playing period
         *
         * @param {String} playingPeriod    Playing period from IWConstants.PLAYING_PERIOD
         */
        SingleLineTicket.prototype.setPlayingPeriod = function(playingPeriod, weeks) {
            if(angular.isDefined(playingPeriod) && Object.keys(IWConstants.PLAYING_PERIODS).indexOf(playingPeriod.toUpperCase()) > -1) {
                this.playingPeriod = playingPeriod;
                if(playingPeriod === IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {
                    this.setBillingPeriod(IWConstants.BILLING_PERIODS.MONTHLY);
                    this.playingPeriodLength = 1;
                }
                else {
                    this.playingPeriodLength = playingPeriod == IWConstants.PLAYING_PERIODS.SET_WEEKS ? (weeks || 1) : 1
                    this.billingPeriod = false;
                }
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set the number of weeks to play (For when playing period is X weeks)
         *
         * @param {Number} playingLength    Number of weeks to play
         */
        SingleLineTicket.prototype.setPlayingLength = function(playingLength) {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS) {
                this.playingPeriodLength = playingLength;
            }
            else {
                this.playingPeriodLength = 1;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Increase the number of weeks to play for
         *
         * @return {Number}     New number of weeks
         */
        SingleLineTicket.prototype.increasePlayingLength = function() {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS) {
                this.playingPeriodLength++;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Decrease the number of weeks to play for
         *
         * @return {Number}     New number of weeks
         */
        SingleLineTicket.prototype.decreasePlayingLength = function() {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS && this.playingPeriodLength > 1) {
                this.playingPeriodLength--;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set how the user should be billed
         *
         * @param {String} billingPeriod    Billing period from IWConstants.BILLING_PERIOD
         */
        SingleLineTicket.prototype.setBillingPeriod = function(billingPeriod) {

            // We can only set billing periods for subscriptions
            if(angular.isDefined(billingPeriod) && this.playingPeriod === IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {

                // Check it is a valid period
                if(Object.keys(IWConstants.BILLING_PERIODS).indexOf(billingPeriod.toUpperCase()) > -1) {
                    this.billingPeriod = billingPeriod;
                }
                else {
                    this.billingPeriod = IWConstants.BILLING_PERIODS.MONTHLY;
                }
            }
            else {
                this.billingPeriod = false;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Recreate ticket from JSON data stored in local storage (for cart service)
         *
         * @param  {String} jsonData JSON data
         * @return {void}
         */
        SingleLineTicket.prototype.restoreFromJSON = function(jsonData) {

            // Convert json data to object
            var jsonData = angular.fromJson(jsonData);

            // Build lottery data
            var lottery = {
                name: jsonData.name,
                drawTypeId: jsonData.drawTypeId,
                id: jsonData.productId,
                ticket: {
                    price: jsonData.linePrice,
                    currency: jsonData.currency
                }
            };

            // Apply the lottery data
            this.setData(lottery);

            // Set the lines of numbers
            this.setLines(jsonData.lines);

            // Set the draw days / playing days
            this.setDrawDays(jsonData.drawDays);

            // Apply playing and billing periods
            this.setPlayingPeriod(jsonData.playingPeriod);
            this.setPlayingLength(jsonData.playingPeriodLength);
            this.setBillingPeriod(jsonData.billingPeriod);
        }

        /**
         * Set the single line information
         *
         * @param {Object} lotteryData    Single line information
         */
        SingleLineTicket.prototype.setData = function(lotteryData) {
            // Check the data was passed in
            if(angular.isDefined(lotteryData)) {
                this.name = lotteryData.name || this.name;
                this.drawTypeId = lotteryData.drawTypeId || this.drawTypeId;
                this.productId = lotteryData.id || this.id;

                // Add the ticket information
                var ticketInfo = angular.isDefined(lotteryData.ticket) ? lotteryData.ticket : false;
                if(ticketInfo) {
                    this.linePrice = angular.isDefined(ticketInfo.price) ? ticketInfo.price : this.linePrice;
                    this.currency = angular.isDefined(ticketInfo.currency) ? ticketInfo.currency : this.currency;
                }

                // Set internal syndicate data
                vm.lotteryData = lotteryData;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Converts the data to the format used for purchasing on the API
         *
         * @return {Object}     Converted object
         */
        SingleLineTicket.prototype.toCart = function() {

            // Cart object
            var cartObject = {
                productId: this.productId,
                periodCount: this.playingPeriodLength,
                price: this.subTotal * 100,
                currencyCode: this.currency,
                startDate: angular.isDefined(this.startDate) ? this.startDate : new Date().toISOString(),
                recurring: this.playingPeriod == IWConstants.PLAYING_PERIODS.SUBSCRIPTION ? true : false,
                rows: [],
                //syndicate: {},
                weekDayMask: 32
            };

            // Lines of numbers
            var lines = [];

            // Format the lines of numbers correctly
            angular.forEach(this.lines, function(line) {
                var _line = {
                    primaryNumbers: line.primaryNumbers.join('-'),
                    secondaryNumbers: line.secondaryNumbers.join('-')
                };
                lines.push(_line);
            });

            // Calculate week day mask
            var weekdayDefaultMask = [0,1,2,4,8,16,32,64];

            var weekDayMask = 0;

            for (var i = this.drawDays.length - 1; i >= 0; i--) {
                weekDayMask += weekdayDefaultMask[this.drawDays[i]];
            }

            cartObject.weekDayMask = weekDayMask;

            // Add to cart object
            cartObject.rows = []; //lines;

            // SPLIT TICKET'S LINES INTO INDIVIDUAL TICKETS
            // =================================
            var cartArr = [];
            angular.forEach(lines, function(line) {
                var ticket = angular.copy(cartObject);
                ticket.rows = [line];
                ticket.price = ticket.price / lines.length;
                cartArr.push(ticket);
            });
            // =================================

            return cartArr;

            //return cartObject;
        };

        /**
         * Add a hook to total price change even
         *
         * @return {Void}
         */
        SingleLineTicket.prototype.onTotalPriceChange = function(callback) {
            eventHooks.onTotalPriceChange.push(callback);
        };

        /**
         * Trigger event price change hooks
         *
         * @param  {Object} instance Instance of SyndicateTicket
         * @return {void}
         */
        function onTotalPriceChangeHandler(instance) {
            for(var i = 0; i < eventHooks.onTotalPriceChange.length; i++) {
                try {
                    eventHooks.onTotalPriceChange[i](instance);
                }
                catch(hookError) {
                    // $exceptionHandler(hookError);
                }
            }
        }

        /**
         * Validate a line by it's properties
         *
         * @param  {Object}         line    Line of numbers
         * @return {Object|Boolean}         Object if success or boolean if false
         */
        function validateLineFormat(line) {
            // Variable to store the final formatted secondary numbers
            var secondary = [];

            // Check primary number is correct format
            if(angular.isDefined(line.primaryNumbers) && angular.isArray(line.primaryNumbers)) {

                // Format the secondary numbers (we don't have to be as strict on this because some lotteries don't have this)
                if(angular.isDefined(line.secondaryNumbers) && line.secondaryNumbers) {
                    if(angular.isNumber(line.secondaryNumbers)) {
                        secondary = [line.secondaryNumbers];
                    }
                    else if(angular.isString(line.secondaryNumbers)) {
                        var parsed = parseInt(line.secondaryNumbers, 10);
                        secondary = !isNaN(parsed) ? parsed : [];
                    }
                    else {
                        secondary = line.secondaryNumbers;
                    }
                }

                // Temp arrays for secondary numbers
                var _primary = [],
                    _secondary = [];

                // Loop through each primary number nad make sure it is a number
                angular.forEach(line.primaryNumbers, function(number) {
                    if(angular.isNumber(number)) {
                        _primary.push(number);
                    }
                    else if(angular.isString(number)) {
                        var parsed = parseInt(number, 10);
                        if(!isNaN(parsed)) {
                            _primary.push(parsed);
                        }
                    }
                });

                // Loop through each secondary number to make sure it is a number
                angular.forEach(secondary, function(number) {
                    if(angular.isNumber(number)) {
                        _secondary.push(number);
                    }
                    else if(angular.isString(number)) {
                        var parsed = parseInt(number, 10);
                        if(!isNaN(parsed)) {
                            _secondary.push(parsed);
                        }
                    }
                });

                // Add the line
                return {
                    primaryNumbers: _primary,
                    secondaryNumbers: _secondary
                };
            }
            else {
                //$log.log('Line format invalid. Please refer to the documentation', line);
                return false;
            }
        }

        // Return single line ticket instance
        return SingleLineTicket;
    }

})();
