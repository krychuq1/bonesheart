(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('SyndicateTicket', SyndicateTicket);

    /** @ngInject */
    function SyndicateTicket($log, IWConstants) {
        var vm = this;

        /**
         * Event hooks
         *
         * @type {Object}
         */
        var eventHooks = {
            onTotalPriceChange: []
        };

        /**
         * Internal syndicate data
         *
         * @type {Object}
         */
        vm.syndicateData = {};

        /**
         * New instance of syndicate ticket for cart
         *
         * @param {Object} syndicate            Syndicate data
         * @param {Number} shares               Number of claimed shares
         * @param {String} playingPeriod        Playing period CONSTANT
         * @param {Number} playingPeriodLength  Number of weeks to play for
         * @param {String} billingPeriod        Billing period CONSTANT
         */
        function SyndicateTicket(syndicate, shares, playingPeriod, playingPeriodLength, billingPeriod) {
            this.name = false;
            this.productId = 0;
            this.drawTypeId = 0;
            this.syndicateId = 0;
            this.shares = {
                total: 0,
                remaining: 0,
                claimed: 0
            };
            this.sharePrice = 0;
            this.playingPeriod = IWConstants.PLAYING_PERIODS.SUBSCRIPTION;
            this.playingPeriodLength = 1;
            this.billingPeriod = IWConstants.BILLING_PERIODS.MONTHLY;
            this.productType = IWConstants.PRODUCT_TYPES.SYNDICATE;
            this.subTotal = 0;
            this.currency = IWConstants.APP.defaultCurrency;

            // Set interanl syndicate data
            if(angular.isDefined(syndicate)) {
                this.setData(syndicate);
            }

            // Set shares
            if(angular.isDefined(shares)) {
                this.setShares(shares);
            }

            // Set playing period
            if(angular.isDefined(playingPeriod)) {
                this.setPlayingPeriod(playingPeriod, playingPeriodLength);
            }

            // Set billing period
            if(angular.isDefined(billingPeriod)) {
                this.setBillingPeriod(billingPeriod);
            }
        }

        /**
         * Ticket bonus init
         *
         * @return {Void}
         */
        SyndicateTicket.prototype.bonus = {};

        /**
         * Sets Ticket bonus
         *
         * @return {Void}
         */
        SyndicateTicket.prototype.setBonus = function (bonus) {
          this.bonus = bonus;
        };

        /**
         * Fets Ticket bonus
         *
         * @return {Object} ticket's bonus
         */
        SyndicateTicket.prototype.getBonus = function () {
          return this.bonus;
        };

        /**
         * Calculates subtotal depending on bonus applied
         *
         * @return {Number} ticekts new price
         */
        SyndicateTicket.prototype.calculateDiscount = function (total) {
          var bonus = this.getBonus();
          var newTotal = 0;
          // if bonus is product specific
          if(bonus.awardtype === 'subscription'){
            newTotal = total - bonus.discountAmount;
          } else if(bonus.awardtype === 'discount' && bonus.awardValueType === 'pct'){ // if percentage discount
              newTotal = bonus.chargeAmount;
          }else if(bonus.awardtype === 'discount' && bonus.awardValueType === 'abs'){ // if absolute value discount
              newTotal = total - (bonus.discountAmount / 100) <= 0 ? 0 : total - bonus.discountAmount / 100;
          }

          return newTotal;
        };

        /**
         * Calculate sub total of ticket
         *
         * @return {Number} Sub total
         */
        SyndicateTicket.prototype.getSubTotal = function() {

            // Billing period multiplier
            var billingMultiplier = 1;

            // Calculate the billing price for subscriptions
            if(this.playingPeriod == IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {
                if(this.billingPeriod == IWConstants.BILLING_PERIODS.MONTHLY) {
                    // 4 weeks
                    billingMultiplier = 4;
                }
                else if(this.billingPeriod == IWConstants.BILLING_PERIODS.QUARTERLY) {
                    // 4 weeks by 3 months
                    billingMultiplier = 12;
                }
            }

            // Calculate initial total
            var total = (this.shares.claimed * this.sharePrice) * this.playingPeriodLength * billingMultiplier;

            // Total is 0 if 0 shares are claimed
            total = this.shares.claimed === 0 ? 0 : total;

            // Set internal total
            if(this.bonus.userId){
              total = this.calculateDiscount(total);
            }
            this.subTotal = total;

            // Trigger event hooks
            onTotalPriceChangeHandler(this);

            // Return total
            return total;
        };

        /**
         * Set the amount of shares a user wants to claim
         *
         * @param   {Number} shareCount     Number of desired shares
         * @return  {Number}                Current amount of claimed shares
         */
        SyndicateTicket.prototype.setShares = function(shareCount) {
            if(shareCount > this.shares.remaining) {
                this.shares.claimed = this.shares.remaining;
            }
            else {
                this.shares.claimed = shareCount;
            }

            // Calculate total
            this.getSubTotal();

            // Return number of claimed share
            return this.shares.claimed;
        }

        /**
         * Increase amount of shares the user wants to purchase
         *
         * @return {Number}     Current amount of claimed shares
         */
        SyndicateTicket.prototype.increaseShares = function() {
            if(this.shares.claimed < this.shares.remaining && this.shares.claimed < this.shares.total) {
                this.shares.claimed++;
            }

            // Calculate total price
            this.getSubTotal();

            // Return number of claimed share
            return this.shares.claimed;
        }

        /**
         * Decrease amount of shares the user wants to purchase
         *
         * @return {Number}     New amount of claimed shares
         */
        SyndicateTicket.prototype.decreaseShares = function() {
            if(this.shares.claimed > 0) {
                this.shares.claimed--;
            }

            // Calculate total price
            this.getSubTotal();

            // Return number of claimed shares
            return this.shares.claimed;
        }

        /**
         * Set the playing period
         *
         * @param {String} playingPeriod    Playing period from IWConstants.PLAYING_PERIOD
         */
        SyndicateTicket.prototype.setPlayingPeriod = function(playingPeriod, weeks) {
            if(Object.keys(IWConstants.PLAYING_PERIODS).indexOf(playingPeriod.toUpperCase()) > -1) {
                this.playingPeriod = playingPeriod;
                if(playingPeriod === IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {
                    this.setBillingPeriod(IWConstants.BILLING_PERIODS.MONTHLY);
                    this.playingPeriodLength = 1;
                }
                else {
                    this.playingPeriodLength = playingPeriod == IWConstants.PLAYING_PERIODS.SET_WEEKS ? (weeks || 1) : 1
                    this.billingPeriod = false;
                }
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set the number of weeks to play (For when playing period is X weeks)
         *
         * @param {Number} playingLength    Number of weeks to play
         */
        SyndicateTicket.prototype.setPlayingLength = function(playingLength) {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS) {
                this.playingPeriodLength = playingLength;
            }
            else {
                this.playingPeriodLength = 1;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Increase the number of weeks to play for
         *
         * @return {Number}     New number of weeks
         */
        SyndicateTicket.prototype.increasePlayingLength = function() {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS) {
                this.playingPeriodLength++;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Decrease the number of weeks to play for
         *
         * @return {Number}     New number of weeks
         */
        SyndicateTicket.prototype.decreasePlayingLength = function() {
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SET_WEEKS && this.playingPeriodLength > 1) {
                this.playingPeriodLength--;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Set how the user should be billed
         *
         * @param {String} billingPeriod    Billing period from IWConstants.BILLING_PERIOD
         */
        SyndicateTicket.prototype.setBillingPeriod = function(billingPeriod) {

            // We can only set billing periods for subscriptions
            if(this.playingPeriod === IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {

                // Check it is a valid period
                if(Object.keys(IWConstants.BILLING_PERIODS).indexOf(billingPeriod.toUpperCase()) > -1) {
                    this.billingPeriod = billingPeriod;
                }
                else {
                    this.billingPeriod = IWConstants.BILLING_PERIODS.MONTHLY;
                }
            }
            else {
                this.billingPeriod = false;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Recreate ticket from JSON data stored in local storage (for cart service)
         *
         * @param  {String} jsonData JSON data
         * @return {void}
         */
        SyndicateTicket.prototype.restoreFromJSON = function(jsonData) {

            // Convert json data to object
            var jsonData = angular.fromJson(jsonData);

            // Build a basic syndicate object
            var syndicate = {
                name: jsonData.name,
                id: jsonData.productId,
                drawTypeId: jsonData.drawTypeId,
                syndicateId: jsonData.syndicateId,
                shares: jsonData.shares.total,
                tickets: {
                    price: jsonData.sharePrice,
                    currency: jsonData.currency
                }
            };

            // Apply the syndicate data
            this.setData(syndicate);

            // Apply the number of claimed shares
            this.setShares(jsonData.shares.claimed);

            // Apply playing and billing periods
            this.setPlayingPeriod(jsonData.playingPeriod);
            this.setPlayingLength(jsonData.playingPeriodLength);
            this.setBillingPeriod(jsonData.billingPeriod);
        }

        /**
         * Set the syndicate information
         *
         * @param {Object} syndicateData    Syndicate information
         */
        SyndicateTicket.prototype.setData = function(syndicateData) {
            // Check the data was passed in
            if(angular.isDefined(syndicateData)) {
                this.name = syndicateData.name || this.name;
                this.drawTypeId = syndicateData.drawTypeId || this.drawTypeId;
                this.productId = syndicateData.id || this.id;
                this.syndicateId = syndicateData.syndicateId || this.syndicateId;
                this.shares.total = 100; // Always 100 shares -> syndicateData.shares || this.shares.total;
                this.shares.remaining = syndicateData.shares || this.shares.remaining;

                // Add the ticket information
                var ticketInfo = angular.isDefined(syndicateData.tickets) ? syndicateData.tickets : false;
                if(ticketInfo) {
                    this.sharePrice = angular.isDefined(ticketInfo.price) ? ticketInfo.price : this.sharePrice;
                    this.currency = angular.isDefined(ticketInfo.currency) ? ticketInfo.currency : this.currency;
                }

                // Set the claimed shares again - this makes sure the user can never claim more shares than reamining shares
                this.setShares(this.shares.claimed);

                // Set internal syndicate data
                vm.syndicateData = syndicateData;
            }

            // Calculate total price
            this.getSubTotal();
        }

        /**
         * Converts the data to the format used for purchasing on the API
         *
         * @return {Object}     Converted object
         */
        SyndicateTicket.prototype.toCart = function() {
            var cartObject = {
                productId: this.productId,
                periodCount: this.playingPeriodLength,
                price: this.subTotal * 100,
                currencyCode: this.currency,
                startDate: angular.isDefined(this.startDate) ? this.startDate : new Date().toISOString(),
                recurring: this.playingPeriod == IWConstants.PLAYING_PERIODS.SUBSCRIPTION ? true : false,
                rows: [],
                syndicate: {
                    shareCount: this.shares.claimed
                }
            };

            return [cartObject];
        }

        /**
         * Add a hook to total price change even
         *
         * @return {Void}
         */
        SyndicateTicket.prototype.onTotalPriceChange = function(callback) {
            eventHooks.onTotalPriceChange.push(callback);
        }

        /**
         * Trigger event price change hooks
         *
         * @param  {Object} instance Instance of SyndicateTicket
         * @return {void}
         */
        function onTotalPriceChangeHandler(instance) {
            for(var i = 0; i < eventHooks.onTotalPriceChange.length; i++) {
                try {
                    eventHooks.onTotalPriceChange[i](instance);
                }
                catch(hookError) {
                    // $exceptionHandler(hookError);
                }
            }
        }

        // Return syndicate tickets
        return SyndicateTicket;
    }

})();
