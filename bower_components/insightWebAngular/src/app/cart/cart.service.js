(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwCart', CartService);

    /** @ngInject */
    function CartService($q, $http, $log, $iwStorage, $iwSession, $filter, SingleLineTicket, SyndicateTicket, IWConstants) {
        var vm = this;

        /**
         * Restoring flag to stop spamming the server with new cart
         *
         * @type {Boolean}
         */
        vm.restoring = false;

        /**
         * Cart service
         *
         * @type {Object}
         */
        var service = {
            init: init,
            getCart: getCart,
            addItem: addItem,
            removeItem: removeItem,
            empty: empty,
            isEmpty: isEmpty,
            getItems: getItems,
            getSubTotal: getSubTotal,
            getItemByProperty: getItemByProperty,
            commit: commit,
            restore: restore,
            save: save,
            applyBonus : applyBonus
        };
        return service;

        /**
         * Initialise the serivce (first time app is run)
         *
         * @return {void}
         */
        function init() {
            vm.$cart = {
                items: [],
                subTotal: 0,
                totals: {
                    all: 0,
                    subscriptions: 0,
                    single: 0
                }
            };

            // Restore cart
            restore();

            // Link into the new session to clear a users cart
            $iwSession.onSessionDestroyed(function() {
                empty();
            });
        }

        /**
         * Get the cart object
         *
         * @return {Object}     Cart object
         */
        function getCart() {
            return vm.$cart;
        }

        /**
         * Set the cart object
         *
         * @param {Object} cart  Cart object
         */
        function setCart(cart) {
            // Set the cart
            vm.$cart = cart;

            // Trigger cart changed event
            onCartChanged('set', cart);

            // Return cart
            return getCart();
        }

        /**
         * Empty the cart
         *
         * @return {void}
         */
        function empty() {
            // Empty the items
            vm.$cart.items = [];

            // Trigger cart changed event
            onCartChanged('emptied');

            // Remove from local storage
            $iwStorage.remove('cart');

            // Update server
            save();
        }

        /**
         * Check if cart is empty
         *
         * @return {Boolean}    TRUE if empty or FALSE if not
         */
        function isEmpty() {
            return (vm.$cart.items.length > 0 ? false : true);
        }

        /**
         * Add a new item ot the cart
         *
         * @param {Object|Class} item   New item
         */
        function addItem(item, startDate) {
            // Add a start date property to the cart items
            item.startDate = angular.isDefined(startDate) ? startDate : new Date().toISOString();

            // Add item
            if(item.subTotal > 0) {
                vm.$cart.items.push(item);
            }

            // Save the joined cart
            if(!vm.restoring && item.subTotal > 0) {
                save();
            }

            // Trigger cart changed event
            onCartChanged('added', item);
        }

        /**
         * Remove an item
         *
         * @param  {Int} index Index of item in total items
         * @return {void}
         */
        function removeItem(index) {
            // Remove the item
            var item = vm.$cart.items.splice(index, 1)[0] || {};

            // Save the cart
            save();

            // Trigger cart changed event
            onCartChanged('removed', item);
        }

        /**
         * Get all the items in the cart
         *
         * @return {Array} Array of items
         */
        function getItems() {
            return getCart().items;
        }

        /**
         * Get an item by a property and value
         *
         * @param  {String}     property    Property to search
         * @param  {Mixed}      value       Value to match
         * @return {Object}                 Found item
         */
        function getItemByProperty(property, value) {

        }

        /**
         * Calculate the sub total of the cart
         *
         * @return {Number}  Total price of cart
         */
        function getSubTotal() {
            var total = 0,
                subscriptionTotal = 0,
                nowTotal = 0;

            // Loop through each item in the cart and add the totals
            angular.forEach(getItems(), function(item) {

                console.log('item calculating', item.subTotal);

                // Item sub total
                var itemSubtotal = item.getSubTotal();
                console.log(itemSubtotal);

                // Add to main sub total
                total += itemSubtotal;
                console.log('total is' ,total);

                // Add to playing type totals
                if(item.playingPeriod == IWConstants.PLAYING_PERIODS.SUBSCRIPTION) {
                    subscriptionTotal += itemSubtotal;
                }
                else {
                    nowTotal += itemSubtotal;
                }
            });

            // Format to final totla
            total = +parseFloat(total).toFixed(2);
            subscriptionTotal = +parseFloat(subscriptionTotal).toFixed(2);
            nowTotal = +parseFloat(nowTotal).toFixed(2);

            // Set internally
            vm.$cart.subTotal = total;

            // Total breakdowns
            vm.$cart.totals = {
                all: total,
                subscriptions: subscriptionTotal,
                single: nowTotal
            };

            // Set internally
            return total;
        }

        /**
         * Converts a json cart to an array of items
         *
         * @param  {String} jsonCart    Json string of the cart
         * @return {Array}              Array of cart items
         */
        function convertJsonToItems(jsonCart) {

            // Blank array of cart items
            var cartItems = [];

            // If we have data begin conversion
            if(jsonCart) {

                // Set the cart to the items array
                jsonCart = jsonCart.items || [];

                // Loop through each item
                angular.forEach(jsonCart, function(item, index) {

                    // Create Single Line Ticket - NOTE: THERE IS A REASON THIS IS SPLIT UP WITH THE SAME CODE it's a @TODO
                    if(item.productType === IWConstants.PRODUCT_TYPES.SINGLE_LINE || item.productType == 'singleline') {

                        // Create a blank ticket
                        var singleLine = new SingleLineTicket();

                        // Restore ticket from JSON
                        singleLine.restoreFromJSON(item);

                        // Add the start date property
                        singleLine.startDate = item.startDate;

                        // Add to our items list
                        cartItems.push(singleLine);
                    }

                    // Create Syndicate Ticket
                    else if(item.productType === IWConstants.PRODUCT_TYPES.SYNDICATE) {

                        // Create a blank ticket
                        var syndicate = new SyndicateTicket();

                        // Restore ticket from JSON
                        syndicate.restoreFromJSON(item);

                        // Add the start date property
                        syndicate.startDate = item.startDate;

                        // Add to our items list
                        cartItems.push(syndicate);
                    }
                });
            }

            // Return cart items
            return cartItems;
        }


        /**
         * Restore the cart from local storage
         */
        function restore() {

            // Set restoring flag (stops saving items until restore is complete)
            vm.restoring = true;

            // Fetch cart products from the server
            fetchServer()
                .then(restoreServer)
                .finally(restoreLocal);

            // Server cart items
            var serverItems = [],
                localItems  = [];

            // Restore the server cart first
            function restoreServer(cartData) {

                // Server cart
                var serverCart = cartData.data;

                // Get an array of converted items
                serverItems = convertJsonToItems(serverCart);
            }

            // Restore the local cart
            function restoreLocal() {

                // Get the local cart JSON
                var localCart = $iwStorage.get('cart');

                // Convert from json
                var localCartData = angular.fromJson(localCart);

                // Get an array of converted items
                var localItems = convertJsonToItems(localCartData);

                // Join the arrays
                var joinedItems = localItems.concat(serverItems);

                // Filter out duplicate items based on start date timestamp
                var uniqueItems = $filter('unique')(joinedItems, 'startDate');

                // Loop through each item and add to our cart
                angular.forEach(uniqueItems, function(cartItem) {
                    addItem(cartItem, cartItem.startDate);
                });

                // Reset restoring flag
                vm.restoring = false;

                // Save the cart
                if(uniqueItems.length > 0) {
                    save();
                }
            }
        }

        /**
         * Save the cart to local storage
         */
        function save() {

            // Get the json for the cart
            var cartJson = angular.toJson(getCart());

            // Save in storage
            $iwStorage.add('cart', cartJson);

            // Save to server
            updateServer();

            // Trigger on cart changed
            onCartChanged('saved', cartJson);
        }

        /**
         * Cart changed event
         *
         * @return {void}
         */
        function onCartChanged(event, data) {

            // Calculate sub total
            getSubTotal();

            // Save to storage
            if(event == 'added' || event == 'removed' || event == 'emptied') {
                //save();
            }

            // If cart was saved then update on server
            if(event == 'saved' && !vm.restoring) {
                //updateServer();
                //$log.log('Saving the cart, updating server');
            }
        }

        /**
         * Update the cart on the server
         *
         * @return {Promise}  Promise
         */
        function updateServer() {
            var deferred = $q.defer();

            var request = {};
            angular.copy(vm.$cart, request);
            request.addAuth = false;

            //
            $http.post(IWConstants.API_ENDPOINTS.CART.UPDATE + $iwSession.getAuthCode(), request)
               .then(updateSuccess, updateFailed);

            return deferred.promise;

            function updateSuccess(response) {
                //$log.log('Updated the cart', response);
            }

            function updateFailed(response) {
                //$log.error('Failed to update cart', response);
            }
        }

        /**
         * Fetch the cart from the server
         *
         * @return {Promise}  Promise
         */
        function fetchServer() {
            var deferred = $q.defer();

            //
            $http.post(IWConstants.API_ENDPOINTS.CART.FETCH + $iwSession.getAuthCode(), {})
               .then(fetchSuccess, fetchFailed);

            return deferred.promise;

            function fetchSuccess(response) {
                deferred.resolve(response);
            }

            function fetchFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Apply bonus to products
         *
         * @return {Void} Void
         */

        function applyBonus(bonuses){
          // for each
          angular.forEach(bonuses,function (bonus) {
            // the item that was rewarded
            var cartItemRewarded = _.find(vm.$cart.items, function(item) {
              var searchStr = bonus.awardtype === 'discount' ? item.currency === bonus.currency && item.productId.toString() === bonus.awardProductId[0] : item.currency === bonus.currency && item.subTotal === bonus.discountAmount && item.productId.toString() === bonus.awardProductId[0];
              return searchStr;
            });
            // add bonus to the ticket
            cartItemRewarded.setBonus(bonus);
            // calculate new ticket price
            cartItemRewarded.getSubTotal();
            // update cart
            getSubTotal();
            // save the cart
            save();
          });
        }

        /**
         * Commit to purchasing the items in the cart
         *
         * @return {Promise} Promise
         */
        function commit() {
            var deferred = $q.defer();

            // Order request object
            var order = {
                frontendId: IWConstants.APP.frontEndId,
                source: 'USER',
                orderList: [],
                addAuth: false
            };

            // Add each item to our order list in correct format
            angular.forEach(vm.$cart.items, function(item) {
                //order.orderList.push(item.toCart());

                // MAKE ITEMS IN CORRECT FORMAT
                var items = angular.copy( item.toCart() );
                order.orderList = order.orderList.concat(items);
                // ==============================
            });

            $log.log('Ordering using:', order);

            // Send our order list
            $http.post(IWConstants.API_ENDPOINTS.CART.COMMIT + $iwSession.getAuthCode(), order)
               .then(orderSuccess, orderFailed);

            return deferred.promise;

            function orderSuccess(response) {
                $log.log('Order success', response);
                // Empty the cart
                empty();
                deferred.resolve(response);
            }

            function orderFailed(response) {
                $log.log('Order failed', response);
                deferred.reject(response);
            }
        }
    }

})();
