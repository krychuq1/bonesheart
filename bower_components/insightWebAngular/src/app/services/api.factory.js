(function() {
    angular
        .module('insightWebAngular')
        .factory('insightApiInterceptor', InsightApiInterceptor);

    // Inject dependencies
    InsightApiInterceptor.$inject = ['$q', '$log', '$iwSession', 'IWConstants'];

    /** @ngInject */
    function InsightApiInterceptor($q, $log, $iwSession, IWConstants) {

        var service = {
            request: request,
            response: response
        };
        return service;

        function request(config) {
            if(config.url.indexOf(IWConstants.API_ROOT.SERVICE) > -1 || config.url.indexOf(IWConstants.API_ROOT.CASHIER) > -1) {
            //if(config.url.indexOf('7lottos') > -1) {
                // Unless config.data.addAuth == false then add the authcode
                if(!(angular.isDefined(config.data) && angular.isDefined(config.data.addAuth) && !config.data.addAuth))
                {
                    // Create a data object for the request if there isn't one already
                    if(angular.isUndefined(config.data)) {
                        config.data = {};
                    }

                    // Add the authcode from the sessions ervice
                    var authCode = $iwSession.getAuthCode();
                    if(authCode) {
                        config.data.authCode = authCode;
                    }
                }

                // Remove the add auth flag if it exists
                if(angular.isDefined(config.data) && angular.isDefined(config.data.addAuth)) {
                    delete config.data.addAuth;
                }
            }
            return config;
        }

        function response(response) {
            if(response.config.url.indexOf(IWConstants.API_ROOT.SERVICE) > -1 || response.config.url.indexOf(IWConstants.API_ROOT.CASHIER) > -1) {
            //if(response.config.url.indexOf('7lottos') > -1) {
                if(response.data.status == 'FAILURE' || response.data.status == 'FALIURE') {
                    // Invalid authcode - reset session
                    if(response.data.errorCode == 'invalid_auth_code') {
                        //$iwSession.create();
                        $iwSession.reset();
                    }

                    // Reject
                    return $q.reject(response);
                }
            }
            return response;
        }
    }

})();