    (function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwMedia', MediaQueryService);

    /** @ngInject */
    function MediaQueryService($log, $filter, $window, $rootScope, $document) {
        //var vm = this;

        // List of CSS media queries
        var cssQueries = {};

        // Array of all media queries
        var queries = {};

        // Array of MediaQueryList objects
        var mqls = {};

        // Array of media queries and match status
        var results = {};

        // Flag to tell us on the first time the service is used grab the CSS list of media queries
        var initiated = false;


        // Public methods
        $mdMedia.getQuery = getQuery;
        $mdMedia.dump = _dump;

        return $mdMedia;

        /**
         * Media query status
         * 
         * @param  {String} query   CSS media query name or custom media query string
         * @return {Boolean}        Current match status of query
         */
        function $mdMedia(query) {

            // Initiate our service on the first time
            if(!initiated) {
                init();
            }

            // Validate the media quiery
            var validated = queries[query];

            if (angular.isUndefined(validated)) {
                validated = queries[query] = validate(query);
            }

            // Try to get the query if it has already been added
            var result = results[validated];

            // Add the query if it doesn't exists and get the current match status
            if (angular.isUndefined(result)) {
                result = add(validated);
            }

            // Return current match status of query
            return result;
        }

        /**
         * Dump the internal data of the service
         * 
         * @return {void}
         */
        function _dump() {
            $log.log('queries', queries);
            $log.log('mqls', mqls);
            $log.log('results', results);
        }

        /**
         * Run this the first time the service is used to add out CSS
         * media queries to the service automatically
         * 
         * @return {void}
         */
        function init() {
            // Get the list of CSS media queries
            cssQueries = getCssQueryList();

            // Set initiated flag to true to stop this running again
            initiated = true;
        }

        /**
         * Get a list of media queries defined in the SCSS
         * 
         * @return {Array} Array of queries
         */
        function getCssQueryList() {

            // Add the media query LIST meta tag if it doesn't exist
            if(angular.element('.insight-mq-list').length < 1) {
                var el = angular.element('<meta class="insight-mq-list">');
                //angular.element(document.body).append(el); // jshint ignore:line
                $document.find('body').eq(0).append(el);
            }

            // Extract the media queries
            var queryList = angular.element('.insight-mq-list').css('font-family');

            // Return the list as object
            return parseStyleToObject(queryList);
        }

        /**
         * Add a new media query watcher
         * 
         * @param {Boolean} query Something
         */
        function add(query) {
            var result = mqls[query];

            // Create a new matchMedia query and add to our global list if the
            // same query doesn't already exsist
            if (!result) {
                result = mqls[query] = $window.matchMedia(query);
            }

            // Add a new media query listener
            result.addListener(onQueryChange);

            // Return current match status of the new query
            return (results[result.media] = !!result.matches);
        }

        /**
         * Checks if the query is in out global CSS query list and if not
         * makes sure the custom query is the correct format
         * 
         * @param  {String} query   Media query name or query
         * @return {String}         Meedia query value e.g (min-width: 100px)
         */
        function validate(query) {
            return cssQueries[query] || ((query.charAt(0) !== '(') ? ('(' + query + ')') : query);
        }

        /**
         * On the query change update the match status
         * 
         * @param  {String} query Query string / name to watch
         * @return {void}
         */
        function onQueryChange(query) {
            $rootScope.$evalAsync(function() {
                results[query.media] = !!query.matches;
            });
        }

        /**
         * Get the  MediaQueryList object for a media query
         * 
         * @param  {String} name    Name of the media query
         * @return {Object}         MediaQueryList object
         */
        function getQuery(name) {
            return mqls[queries[name]];
        }

        /**
         * Convert the query string to a javascript object
         * Thank you: https://github.com/sindresorhus/query-string
         * 
         * @param  {String} str Query string
         * @return {Object}     Object from converted string
         */
        function parseStyleToObject(queryString) {
            var styleObject = {};

            if (typeof queryString !== 'string') {
            //if(angular.isString(queryString)) {
                return styleObject;
            }

            queryString = queryString.trim().slice(1, -1); // browsers re-quote string style values

            if (!queryString) {
                return styleObject;
            }

            styleObject = queryString.split('&').reduce(function(ret, param) {
                var parts = param.replace(/\+/g, ' ').split('=');
                var key = parts[0];
                var val = parts[1];
                key = decodeURIComponent(key);

                // missing `=` should be `null`:
                // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
                //val = val === undefined ? null : decodeURIComponent(val);
                val = angular.isUndefined(val) ? null : decodeURIComponent(val);

                if (!ret.hasOwnProperty(key)) {
                    ret[key] = val;
                }
                else if(angular.isArray(ret[key])) {
                //else if (Array.isArray(ret[key])) {
                    ret[key].push(val);
                }
                else {
                    ret[key] = [ret[key], val];
                }
                return ret;
              }, {}
            );

            return styleObject;
        }
    }

})();