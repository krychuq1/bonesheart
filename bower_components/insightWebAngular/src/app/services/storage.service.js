/**
 * Storage Provider
 * @namespace Providers
 */
(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .provider('$iwStorage', storageServiceProvider);

    // Inject dependencies
    storageServiceProvider.$inject = [];

    /** @ngInject */
    function storageServiceProvider() {

        /**
         * Storage service
         * 
         * @type {Factory}
         */
        this.$get = storageService;

        /**
         * Storage type
         * 
         * @type {String}
         */
        this.storageType = 'localStorage';

        /**
         * Storage prefix
         * 
         * @type {String}
         */
        this.prefix = 'iw';

        /**
         * Array of keys to exclude from the clear() command. In
         * order to remove these items use remove() method
         * 
         * @type {Array}
         */
        this.clearExclude = ['cart'];

        /**
         * Prefix appender
         * 
         * @type {String}
         */
        this.prefixAppender = '.';

        /**
         * Set the storage prefix
         * 
         * @param {string} prefix Storage prefix
         */
        this.setPrefix = function(prefix) {
            this.prefix = prefix;
        };

        /**
         * Set storage type
         * 
         * @param {string} storageType localStorage or sessionStorage
         */
        this.setStorageType = function(storageType) {
            if(storageType.toUpperCase() == 'sessionStorage'.toUpperCase()) {
                this.storageType = 'sessionStorage';
            }
            else {
                this.storageType = 'localStorage';
            }
        };

        /**
         * Inject dependencies to storage service
         * 
         * @type {Array}
         */
        storageService.$inject = ['$window', '$log', '$exceptionHandler'];

        /* @ngInject */
        function storageService($window, $log, $exceptionHandler) {
            var vm = this;

            /**
             * Storage item prefix
             * 
             * @type {string}
             */
            var prefix = vm.prefix + vm.prefixAppender;

            /**
             * Is storage supported
             * 
             * @type {bool}
             */
            var supported = storageSupported();

            /**
             * Storage event hooks
             * 
             * @type {Object}
             */
            var eventHooks = {
                onItemAdded: [],
                onItemRemoved: [],
                onStorageEmptied: []
            };

            /**
             * Old storage item count (used for event hook checks)
             * 
             * @type {int}
             */
            var oldStorageCount = length();

            /**
             * Storage service functions
             * @type {Object}
             */
            var service = {
                get: getItem,
                add: setItem,
                remove: removeItem,
                clear: clear,
                decimate: decimate,
                length: length,
                lengthAll: lengthAll,
                onItemAdded: onItemAdded,
                onItemRemoved: onItemRemoved,
                onStorageEmptied: onStorageEmptied,
                prefixKey: prefixKey,
                unprefixKey: unprefixKey,
                isSupported: storageSupported,
                getStorageType: getStorageType
            };


            // Setup - attach event listeners
            init();

            // Return service
            return service;

            /**
             * Setup the storage service
             * 
             * @return {void}
             */
            function init() {
                try {
                    if(supported) {
                        // Attach event listern to storage change event
                        if($window.addEventListener) {
                            $window.addEventListener('storage', storageEventHandler, false);
                        }
                        else if($window.attachEvent) {
                            $window.attachEvent('onstorage', storageEventHandler);
                        }
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                }
            }

            /**
             * Storage event handler
             * 
             * @param  {StorageEvent} e StorageEvent object
             * @return {void}
             */
            function storageEventHandler(e) {
                if (!e) {
                    e = $window.event;
                }

                // Ignore modernizr & service support cheker events
                if(e.key !== '__storage_test__' && e.key !== 'modernizr') {
                    // Check for event changes
                    checkEventHooks(e);   
                }
            }

            /**
             * Check what changed happened and fire event hooks
             *
             * @param  {StorageEvent} [event] StorageEvent object
             * @return {void}
             */
            function checkEventHooks(event) {
                // Exactly the same, nothing changed
                if(oldStorageCount !== length()) {

                    // Current item count
                    var currentCount = length();


                    // Emtpied, no more items left owned by the app 
                    if(oldStorageCount !== 0 && currentCount === 0) {
                        onStorageEmptiedHandler(event);
                    }
                    else {
                        // Item was added
                        if(oldStorageCount < currentCount) {
                            onItemAddedHandler(event);
                        }
                        
                        // Item was removed
                        if(oldStorageCount > currentCount) {
                            onItemRemovedHandler(event);
                        }
                    }

                    // Set the new storage item count
                    oldStorageCount = currentCount;
                }
            }

            /**
             * Register item added callback
             * 
             * @return {void}
             */
            function onItemAdded(callback) {
                eventHooks.onItemAdded.push(callback);
            }

            /**
             * Register item removed callback
             * 
             * @return {void}
             */
            function onItemRemoved(callback) {
                eventHooks.onItemRemoved.push(callback);
            }

            /**
             * Register storage emptied callback
             * 
             * @return {void}
             */
            function onStorageEmptied(callback) {
                eventHooks.onStorageEmptied.push(callback);
            }

            /**
             * Item added event handlers
             * 
             * @param  {StorageEvent} [event] StorageEvent object
             * @return {void}
             */
            function onItemAddedHandler(event) {
                for(var i = 0; i < eventHooks.onItemAdded.length; i++) {
                    try {
                        eventHooks.onItemAdded[i](event);
                    }
                    catch(hookError) {
                        $exceptionHandler(hookError);
                    }
                }
            }

            /**
             * Item removed event handlers
             *
             * @param  {StorageEvent} [event] StorageEvent object
             * @return {void}
             */
            function onItemRemovedHandler(event) {
                for(var i = 0; i < eventHooks.onItemRemoved.length; i++) {
                    try {
                        eventHooks.onItemRemoved[i](event);
                    }
                    catch(hookError) {
                        $exceptionHandler(hookError);
                    }
                }
            }

            /**
             * Storage emptied event handlers
             * 
             * @return {void}
             */
            function onStorageEmptiedHandler() {
                for(var i = 0; i < eventHooks.onStorageEmptied.length; i++) {
                    try {
                        eventHooks.onStorageEmptied[i]();
                    }
                    catch(hookError) {
                        $exceptionHandler(hookError);
                    }
                }
            }

            /**
             * Add an item to storage
             * 
             * @param {string} key   Storage key
             * @param value          Value to store
             * @return {void}
             */
            function setItem(key, value) {
                try {
                    if(supported) {
                        // Add the item to storage (serialize using toJson)
                        $window[vm.storageType].setItem(prefixKey(key), angular.toJson(value));

                        // Fire the added item event
                        checkEventHooks();
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                }
            }

            /**
             * Get item from storage
             * 
             * @param  {string} key Storage key
             * @return {object|string|bool|null}     Storage value
             */
            function getItem(key) {
                try {
                    if(supported) {
                        // Get the item
                        var item = $window[vm.storageType].getItem(prefixKey(key));

                        // Try to de-serialize the stored object
                        try {
                            return angular.fromJson(item);
                        }
                        catch(e) {
                            return item;
                        }
                    }
                    else {
                        return null;
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                    return null;
                }
            }

            /**
             * Remove item from storage
             * 
             * @param  {key} key Stoage key
             * @return {void}
             */
            function removeItem(key) {
                try {
                    if(supported) {
                        // Remove the item
                        $window[vm.storageType].removeItem(prefixKey(key));

                        // Fire the remove item event
                        checkEventHooks();
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                }
            }

            /**
             * Remove all items owned by the app
             * 
             * @return {void}
             */
            function clear() {
                try {
                    if(supported && length() > 0) {
                        // Regular expression to match only the app keys
                        var regex = new RegExp('^' + prefix);

                        // Loop through all keys and remove only the keys in the app
                        for(var key in $window[vm.storageType]) {
                            if(regex.test(key) && !(vm.clearExclude.indexOf(unprefixKey(key)) > -1)) {
                                removeItem(key.substr(prefix.length));
                            }
                        }

                        // Fire the storage emptied event
                        checkEventHooks();
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                }
            }

            /**
             * Wipe everything from storage, even items not owned by us
             * 
             * @return {void}
             */
            function decimate() {
                try {
                    if(supported && lengthAll() > 0) {
                        // Clear everything from storage
                        $window[vm.storageType].clear();

                        // Fire the storage emptied event
                        checkEventHooks();
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                }
            }

            /**
             * Get number of items in storage owned by the app
             * 
             * @return {int} Item count
             */
            function length() {
                try {
                    if(supported) {
                        var count = 0;
                        var storage = $window[vm.storageType];
                        for(var i = 0; i < storage.length; i++) {
                            if(storage.key(i).indexOf(prefix) === 0 ) {
                                count++;
                            }
                        }
                        return count;
                    }
                    else {
                        return 0;
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                    return 0;
                }
            }
            
            /**
             * Get number of all items in storage
             * 
             * @return {int} Item count
             */
            function lengthAll() {
                try {
                    if(supported) {
                        return $window[vm.storageType].length;
                    }
                    else {
                        return 0;
                    }
                }
                catch(storageError) {
                    $exceptionHandler(storageError);
                    return 0;
                }
            }

            /**
             * Add app prefix to storage key
             * 
             * @param  {string} key Key to prefix
             * @return {string}     Prefixed key
             */
            function prefixKey(key) {
                return prefix + key;
            }

            /**
             * Remove app prefix from storage key
             * 
             * @param  {string} key Prefixed key
             * @return {string}     Unprefixed key
             */
            function unprefixKey(key) {
                return key.substr(prefix.length);
            }

            /**
             * Get current storage type
             * 
             * @return {string} Storage type (localStorage or sessionStorage)
             */
            function getStorageType() {
                return vm.storageType;
            }

            /**
             * Check if storage is supported
             * 
             * @return {bool} Is supported or not
             */
            function storageSupported() {
                try {
                    var storage = $window[vm.storageType],
                        x = '__storage_test__';
                    storage.setItem(x, x);
                    storage.removeItem(x);
                    return true;
                }
                catch(e) {
                    return false;
                }
            }
        }
    }
})();