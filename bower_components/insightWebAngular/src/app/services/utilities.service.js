/**
 * Utilities Service
 * @namespace Service
 */
(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwUtils', utilitiesService);

    /**
     * @namespace Utilities
     * @desc Application wide utility functions
     * @memberOf Services
     */
    function utilitiesService($q, $log, IWConstants) {
        var vm = this;

        /**
         * Session service
         * 
         * @type {Object}
         */
        var service = {
            init: init
        };
        return service;

        /**
         * Initialise the session service
         * 
         * @return {void}
         */
        function init() {
            $log.log('Utilities initialised');
        }
    }
})();