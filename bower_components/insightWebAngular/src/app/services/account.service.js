(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwAccount', AccountService);

    /** @ngInject */
    function AccountService($log, $q, $http, $timeout, $iwStorage, $iwCart, $iwSession, IWConstants, $iwBonus,$iwProducts ) {
        var vm = this;

        /**
         * Account object
         *
         * @type {Object}
         */
        vm.$account = {
            profile: $iwStorage.get('profile') || {},
            balance: 0
        };

        /**
         * Account service
         *
         * @type {Object}
         */
        var service = {
            init: init,
            login: login,
            logout: logout,
            register: register,
            completeRegistration: completeRegistration,
            resetPassword: resetPassword,
            updatePassword: updatePassword,
            updateEmail: updateEmail,
            updateProfile: updateProfile,

            setDepositLimits: setDepositLimits,
            setWagerLimits: setWagerLimits,
            setLossLimits: setLossLimits,

            getLimits: getLimits,
            getDepositLimits: getDepositLimits,
            getWagerLimits: getWagerLimits,
            getLossLimits: getLossLimits,

            getTransactionHistory: getTransactionHistory,
            getPurchaseHistory: getPurchaseHistory,

            getBio: getBio,
            getProfile: getProfile,
            isAuthenticated: isAuthenticated,
            getAccount: getAccount,
            getBalance: getBalance,
            getSubscriptions: getSubscriptions,
            getSubscriptionDetails: getSubscriptionDetails
        };

        init();

        return service;

        /**
         * Initialise the serivce (first time app is run)
         *
         * @return {void}
         */
        function init() {

            // Hook into destroyed session handlers to logout the user
            $iwSession.onSessionDestroyed(function() {
                vm.$account.profile = {};
                vm.$account.balance = 0;
            });

            // If the user is already logged in let's refresh their data
            // this will also log them out if their session has expired
            // the interceptor will broadcast sessionExpired
            if($iwSession.isAuthenticated()) {

                // Refersh profile data
                getProfile();

                // Get the balance
                getBalance();
            }
        }

        /**
         * Login a user
         *
         * @param  {string} email       Email address (username)
         * @param  {password} password  Password
         * @return {Promise}            Promise object upon completion
         */
        function login(email, password) {
            // Deferred object - chain of operations
            var deferred = $q.defer();

            // Login request
            var request = {
                brand: IWConstants.APP.brand,
                frontEnd: IWConstants.APP.frontEnd,
                emailId: email,
                password: password
            };

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.LOGIN, request)
                .then(loginSuccess, loginFailed);

            // Return promise object
            return deferred.promise;

            function loginSuccess(response) {

                // Set user authentication
                $iwSession.setAuthenticated(true);

                // Add user id to storage
                $iwStorage.add('extId', response.data.extId);

                // Fetch the user profile (will automatically save to session)
                getProfile();

                // Get balance
                getBalance();

                // Get the cart
                $iwCart.restore();

                // Get applicable packages for this user
                $iwBonus.init();

                // Resolve
                deferred.resolve(response.data);
            }

            function loginFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Logout a user
         *
         * @return {void}
         */
        function logout() {
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.LOGOUT).finally(function() {
                $iwSession.reset();

                // refresh bonuses
                $iwBonus.init();
            });
        }

        /**
         * Get users biography
         *
         * @return {Promise} User biography
         */
        function getBio() {
            // Deferred object
            var deferred = $q.defer();

            // Make the request
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PROFILE_INFO_SHORT).then(getBioSuccess, getBioFailed);

            return deferred.promise;

            function getBioSuccess(response) {
                deferred.resolve(response);
            }

            function getBioFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get user profile (different from bio)
         *
         * @return {Promise} User profile
         */
        function getProfile() {
            var deferred = $q.defer();

            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PROFILE_INFO_FULL).then(getProfileSuccess, getProfileFailed);

            return deferred.promise;

            function getProfileSuccess(response) {

                // Set the session currency and language from account data
                $iwSession.setCurrency(response.data.currency);
                $iwSession.setLanguage(response.data.accLanguage);

                // Set profile data
                setProfile(response.data);

                deferred.resolve(response);
            }

            function getProfileFailed(response) {
                deferred.reject(response.data);
            }
        }

        function setProfile(profile) {
            // Format the data so it's nicer
            var profileData = {
                externalId      : profile.externalId,
                email           : profile.email,
                firstName       : profile.fName,
                lastName        : profile.lName,
                playRealStatus  : profile.playRealStatus,
                signupDate      : profile.signupDate,
                lastUpdated     : profile.lastUpdated,
                endUserId       : profile.endUserId,
                address         : profile.address,
                city            : profile.city,
                zip             : profile.zip,
                country         : profile.country,
                dob             : new Date(profile.dob),
                phone           : profile.phone,
                source          : profile.source,
                bioListId       : profile.bioListId,
                title           : profile.title,
                language        : profile.accLanguage
            };

            // Set the profile data on the account object
            vm.$account.profile = profileData;

            // Set profile data in storage
            $iwStorage.add('profile', profileData);
        }


        function register(request) {
            // Deferred object - chain of operations
            var deferred = $q.defer();

            // Login request
            var requestData = {
                brand           : IWConstants.APP.brand,
                frontEnd        : IWConstants.APP.frontEnd,
                accLanguage     : $iwSession.getLanguage(),
                accCurrency     : $iwSession.getCurrency(),
                fName           : request.firstName,
                lName           : request.lastName,
                emailId         : request.email,
                password        : request.password,
                phone           : request.phone,
                isTncAccepted   : request.terms,
                bTag            : '',
                addAuth         : true
            };

            console.log('register with data from angular framework -----',requestData);

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.REGISTER_QUICK, requestData)
                .then(registerSuccess, registerFailed);

            // Return promise object
            return deferred.promise;

            function registerSuccess(response) {
                deferred.resolve(response.data);
            }

            function registerFailed(response) {
                deferred.reject(response.data);
            }
        }

        function completeRegistration(request) {
            // Deferred object - chain of operations
            var deferred = $q.defer();

            // Login request
            var request = {
                brand           : IWConstants.APP.brand,
                frontEnd        : IWConstants.APP.frontEnd,
                title           : request.title,
                address         : request.address,
                city            : request.city,
                zip             : request.zip,
                country         : request.countryCode,
                freePrizeOptIn  : request.prize,
                dob: {
                    day         : request.dob.day,
                    month       : request.dob.month,
                    year        : request.dob.year
                },
                accCurrency     : request.currency,
                addAuth         : true,
                regStep2        : true
            };

            // Convert the date of birth to a time stamp (this was lifted from current angular app)
            var dob = new Date(request.dob.year, request.dob.month - 1, request.dob.day);
            request.dob = dob.getTime() - (dob.getTimezoneOffset() * 60000);

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PROFILE_UPDATE, request)
               .then(registerCompleteSuccess, registerCompleteFailed);

            // Return promise object
            return deferred.promise;

            function registerCompleteSuccess(response) {
                deferred.resolve(response.data);
            }

            function registerCompleteFailed(response) {
                deferred.reject(response);
            }
        }

        function resetPassword(email) {
            // Deferred object - chain of operations
            var deferred = $q.defer();

            // Request
            var request = {
                brand           : IWConstants.APP.brand,
                frontEnd        : IWConstants.APP.frontEnd,
                emailId: email,
                addAuth: true,
            };

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PASSWORD_RESET, request)
                .then(resetSuccess, resetFailed);

            // Return promise object
            return deferred.promise;

            function resetSuccess(response) {
                // Resolve
                deferred.resolve(response.data);
            }

            function resetFailed(response) {
                deferred.reject(response);
            }
        }


        /**
         * Account page stuff
         */
        function updatePassword(oldPassword, newPassword, confirmPassword) {
           // Deferred object - chain of operations
            var deferred = $q.defer();

            // Request
            var request = {
                oldPassword: oldPassword,
                newPassword: newPassword,
                confirmPassword: confirmPassword,
                addAuth: true
            };

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PASSWORD_UPDATE, request)
                .then(updatePasswordSuccess, updatePasswordFailed);

            // Return promise object
            return deferred.promise;

            function updatePasswordSuccess(response) {
                // Resolve
                deferred.resolve(response);
            }

            function updatePasswordFailed(response) {
                deferred.reject(response);
            }
        }

        function updateEmail(newEmail) {
            var deferred = $q.defer();

            var request = {
                emailId     : newEmail,
                brand       : IWConstants.APP.brand,
                frontEnd    : IWConstants.APP.frontEnd,
                authCode    : true
            };

            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.EMAIL_UPDATE, request)
               .then(updateEmailSuccess, updateEmailFailed);

            return deferred.promise;

            function updateEmailSuccess(response) {

                // Load the profile again
                getProfile();

                // Resolve promise
                deferred.resolve(response);
            }

            function updateEmailFailed(response) {
                deferred.reject(response);
            }
        }


        /**
         * Getting deposit limits
         */
        function getLimits() {
            var deferred = $q.defer();

            $q.all([
                getDepositLimits(),
                getWagerLimits(),
                getLossLimits()
            ]).then(getLimitsSuccess, getLimitsFailed);;

            // Return promise object
            return deferred.promise;

            function getLimitsSuccess(response) {
                // Combined data format
                var limits = {
                    deposit: response[0],
                    wager: response[1],
                    loss: response[2]
                };

                // Resolve
                deferred.resolve(limits);
            }

            function getLimitsFailed(response) {
                deferred.reject(response);
            }
        }

        function _getLimits(url) {
            var deferred = $q.defer();

            // Request
            var request = {
                addAuth: true
            };

            // Post
            $http.post(url, request)
                .then(getLimitsSuccess, getLimitsFailed);

            // Return promise object
            return deferred.promise;

            function getLimitsSuccess(response) {

                // Nice data format
                var limits = {
                    daily: {
                        currency: response.data.dailyLimit.currency,
                        timestamp: response.data.dailyLimit.requestedTimeStamp,
                        rgStatus: response.data.dailyLimit.rgStatus,
                        limit: response.data.dailyLimit.value
                    },
                    weekly: {
                        currency: response.data.weeklyLimit.currency,
                        timestamp: response.data.weeklyLimit.requestedTimeStamp,
                        rgStatus: response.data.weeklyLimit.rgStatus,
                        limit: response.data.weeklyLimit.value
                    },
                    monthly: {
                        currency: response.data.monthlyLimit.currency,
                        timestamp: response.data.monthlyLimit.requestedTimeStamp,
                        rgStatus: response.data.monthlyLimit.rgStatus,
                        limit: response.data.monthlyLimit.value
                    },
                    semiannual: {
                        currency: response.data.oneEightyDaysLimit.currency,
                        timestamp: response.data.oneEightyDaysLimit.requestedTimeStamp,
                        rgStatus: response.data.oneEightyDaysLimit.rgStatus,
                        limit: response.data.oneEightyDaysLimit.value
                    }
                };

                // Resolve
                deferred.resolve(limits);
            }

            function getLimitsFailed(response) {
                deferred.reject(response);
            }
        }

        function getDepositLimits() {
            return _getLimits(IWConstants.API_ENDPOINTS.RESPONSIBLITIES.GET.DEPOSIT_LIMITS);
        }

        function getWagerLimits() {
            return _getLimits(IWConstants.API_ENDPOINTS.RESPONSIBLITIES.GET.WAGER_LIMITS);
        }

        function getLossLimits() {
            return _getLimits(IWConstants.API_ENDPOINTS.RESPONSIBLITIES.GET.LOSS_LIMITS);
        }


        /**
         * Setting deposit limits
         */
        function setLimits(limits, url) {
            var deferred = $q.defer();

            // Request
            var request = {
                dailyLimit: {
                    value: limits.daily
                },
                weeklyLimit: {
                    value: limits.weekly
                },
                monthlyLimit: {
                    value: limits.monthly
                },
                oneEightyDaysLimit: {
                    value: limits.semiannual
                },
                addAuth: true
            };

            // Post
            $http.post(url, request)
                .then(setLimitsSuccess, setLimitsFailed);

            // Return promise object
            return deferred.promise;

            function setLimitsSuccess(response) {
                deferred.resolve(response);
            }

            function setLimitsFailed(response) {
                deferred.reject(response);
            }
        }

        function setDepositLimits(limits) {
            return setLimits(limits, IWConstants.API_ENDPOINTS.RESPONSIBLITIES.UPDATE.DEPOSIT_LIMITS);
        }

        function setWagerLimits(limits) {
            return setLimits(limits, IWConstants.API_ENDPOINTS.RESPONSIBLITIES.UPDATE.WAGER_LIMITS);
        }

        function setLossLimits(limits) {
            return setLimits(limits, IWConstants.API_ENDPOINTS.RESPONSIBLITIES.UPDATE.LOSS_LIMITS);
        }

        function getTransactionHistory(startDate, endDate, size, offset) {
            var deferred = $q.defer();

            var request = {
                addAuth: true,
                size: size || 10,
                offset: offset || 0,
                startDate: startDate,
                endDate: endDate
            };

            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.TRANSACTION_HISTORY, request)
                .then(transactionSuccess, transactionError);

            return deferred.promise;

            function transactionSuccess(response) {
                deferred.resolve(response);
            }

            function transactionError(response) {
                deferred.reject(response);
            }
        }

        function getPurchaseHistory(startDate, endDate) {
            var deferred = $q.defer();

            var request = {
                addAuth: false,
                dateOfPurchaseFrom: startDate,
                dateOfPurchaseTo: endDate,
                frontendId: IWConstants.APP.frontEndId,
                extId: null
            };

            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PURCHASE_HISTORY + $iwSession.getAuthCode(), request)
                .then(transactionSuccess, transactionError);

            return deferred.promise;

            function transactionSuccess(response) {
                deferred.resolve(response);
            }

            function transactionError(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get the account object for binding
         *
         * @return {Object}     Account object
         */
        function getAccount() {
            return vm.$account;
        }

        /**
         * Get account balance
         *
         * @return {Promise} Promise
         */
        function getBalance() {
            var deferred = $q.defer();

            var request = {
                addAuth: true
            };

            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.BALANCE, request)
                .then(balanceSuccess, balanceFailed);

            return deferred.promise;

            function balanceSuccess(response) {
                deferred.resolve(response);
                setBalance(response.data.balance);
            }

            function balanceFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Set the account balance
         *
         * @param {Number} balance Balance to set
         */
        function setBalance(balance) {
            vm.$account.balance = balance;
        }

        /**
         * Update a users profile
         *
         * @param  {Object} request  Profile data
         * @return {Promise}         Promise
         */
        function updateProfile(request) {

            // Deferred object - chain of operations
            var deferred = $q.defer();

            // Profile request
            var profileRequest = {
                brand           : IWConstants.APP.brand,
                frontEnd        : IWConstants.APP.frontEnd,
                title           : vm.$account.profile.title,
                address         : request.address   || vm.$account.profile.address,
                city            : request.city      || vm.$account.profile.city,
                zip             : request.zip       || vm.$account.profile.zip,
                phone           : request.phone     || vm.$account.profile.phone,
                dob             : vm.$account.profile.dob,
                country         : vm.$account.profile.country,
                addAuth         : true
            };

            // Convert the date of birth to a time stamp (this was lifted from current angular app)
            profileRequest.dob = profileRequest.dob.getTime() - (profileRequest.dob.getTimezoneOffset() * 60000);

            // Post login details
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.PROFILE_UPDATE, profileRequest)
               .then(profileUpdateSuccess, profileUpdateFailed);

            // Return promise object
            return deferred.promise;

            function profileUpdateSuccess(response) {
                deferred.resolve(response.data);
            }

            function profileUpdateFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get a list of active subscriptions / tickets
         *
         * @return {Array} Array of subscriptions
         */
        function getSubscriptions() {
            var deferred = $q.defer();

            // Subscriptions request
            var subscriptionRequest = {
                addAuth: false,
                frontendId: IWConstants.APP.frontEndId
            };

            // Send the request
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.SUBSCRIPTIONS.ALL + $iwSession.getAuthCode(), subscriptionRequest)
                .then(subscriptionsSuccess, subscriptionsFailed);

            // Return the promise
            return deferred.promise;

            // Successful request
            function subscriptionsSuccess(response) {

                // Array of subscriptions
                var subscriptions = [];

                // Loop through each result and format the data
                angular.forEach(response.data.response, function(result) {

                    // Better data structure for subscription
                    var subscription = {
                        name: result.lotteryName,
                        drawTypeId: result.drawTypeId,
                        productId: false,
                        productType: false,
                        subscriptionId: result.subscriptionId,
                        tickets: [
                            {
                                primaryNumbers: result.primaryNumbers,
                                secondaryNumbers: result.secondaryNumbers || []
                            }
                        ],
                        lastDraw: {
                            id: result.lastDrawId,
                            date: result.lastDrawDateTime
                        },
                        expires: result.expiryDate,
                        subscription: new Date(result.expiryDate).getFullYear() >= 9999
                    };

                    // Extra fields depending on product type
                    if(result.productType == 'SYNDICATE') {
                        subscription.productType = IWConstants.PRODUCT_TYPES.SYNDICATE;

                        // @TODO: When the API supports multiple syndicates we need to get a way to make sure something
                        // like this works. For now it just finds the first syndicate
                        subscription.productId = $iwProducts.toProductId(result.drawTypeId);
                    }
                    else if(result.productType == 'SINGLE_LINE') {
                        subscription.productType = IWConstants.PRODUCT_TYPES.SINGLE_LINE;
                        subscription.productId = $iwProducts.toProductId(result.drawTypeId);
                    }

                    // Add to our final array
                    subscriptions.push(subscription);
                });

                // Resolve with the array of subscriptions
                deferred.resolve(subscriptions);
            }

            // Failed request
            function subscriptionsFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get details for a subscription
         *
         * @param  {Number} subscriptionId Subscription ID
         * @return {Object}                Details of subscription
         */
        function getSubscriptionDetails(subscriptionId) {
            var deferred = $q.defer();

            // Subscription details request
            var detailsRequest = {
                subscriptionId: subscriptionId,
                languageId: $iwSession.getLanguage(),
                frontendId: IWConstants.APP.frontEndId,
                addAuth: false
            };

            // Send the request
            $http.post(IWConstants.API_ENDPOINTS.ACCOUNT.SUBSCRIPTIONS.DETAILS + $iwSession.getAuthCode(), detailsRequest)
                .then(detailsSuccess, detailsFailed);

            // Return the promise
            return deferred.promise;

            // Successful request
            function detailsSuccess(response) {
                deferred.resolve(response.data);
            }

            // Failed request
            function detailsFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Check if user is logged in (they should have a user id)
         *
         * @return {boolean} True/Fale for login status
         */
        function isAuthenticated() {
            return $iwSession.isAuthenticated();
        }
    }

})();
