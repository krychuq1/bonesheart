/**
 * Session Service
 * @namespace Service
 */
(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwSession', sessionService);

    /**
     * @namespace Session
     * @desc Application wide session handling
     * @memberOf Services
     */
    function sessionService($q, $log, $iwStorage, $injector, IWConstants) {
        var vm = this;

        /**
         * $http service. We manually inject this later to get around the
         * circular dependecy problem
         * 
         * @type {Object}
         */
        var $http = {};

        /**
         * Session event hooks
         * 
         * @type {Object}
         */
        var eventHooks = {
            onSessionCreated: [],
            onSessionDestroyed: [],
            onSessionReset: []
        };


        /**
         * Session object
         * 
         * @type {Object}
         */
        vm.$session = {
            authCode: $iwStorage.get('authCode') || null,
            authenticated: !!$iwStorage.get('extId') || false,
            currency: $iwStorage.get('currency') || IWConstants.APP.defaultCurrency,// APP_CONFIG.defaultCurrency,
            language: $iwStorage.get('language') || IWConstants.APP.defaultLanguage// APP_CONFIG.defaultLanguage
        };

        /**
         * Session service
         * 
         * @type {Object}
         */
        var service = {
            init: init,

            create: create,
            destroy: destroy,
            reset: reset,
            getSession: getSession,

            getAuthCode: getAuthCode,
            setAuthCode: setAuthCode,

            isAuthenticated: isAuthenticated,
            setAuthenticated: setAuthenticated,

            getLanguage: getLanguage,
            setLanguage: setLanguage,

            getCurrency: getCurrency,
            setCurrency: setCurrency,

            onSessionCreated: onSessionCreated,
            onSessionDestroyed: onSessionDestroyed,
            onSessionReset: onSessionReset
        };
        return service;

        /**
         * Initialise the session service
         * 
         * @return {void}
         */
        function init() {
            // Inject $http service
            $http = $injector.get('$http');

            // REMOVE THE FOLLOWING HOOKS IF YOU DON'T WANT AN AUTCODE TO ALWAYS 
            // EXIST.

            // // Hook into storage emptied event to reset session
            $iwStorage.onStorageEmptied(function storageEmptied(ev) {
                //reset();
                //$log.log('Storage was emptied', ev);
            });

            // If authCode is removed from storage reset the session
            $iwStorage.onItemRemoved(function storageItemRemoved(event) {
                // Authcode was removed so clear everything in storage which will trigger
                // the onStorageEptied event which we use to create a new session.
                if(event && $iwStorage.unprefixKey(event.key) == 'authCode') {
                    //$iwStorage.clear();
                    create();
                    $log.log('Requesting new authcode');
                }
            });

            // If we don't have an authcode lets create a new session
            if(!getAuthCode()) {
                create();
            }
        }

        /**
         * Create a new session
         * 
         * @return {Promise} Promise object
         */
        function create() {
            // Deferred object
            var deferred = $q.defer();

            // Create a new session
            $http.post(IWConstants.API_ENDPOINTS.SESSION.CREATE, {addAuth: false})
                 .then(createSuccess, createFailed);

            // Return promise
            return deferred.promise;

            /**
             * Session created
             * 
             * @param  {object} response Response from server
             * @return {void}
             */
            function createSuccess(response) {
                // Set the internal authcode
                setAuthCode(response.data.authCode);

                // Store authcode in storage
                $iwStorage.add('authCode', response.data.authCode);

                // Call created callbacks
                onSessionCreateHandler();

                // Resolve promise
                deferred.resolve(response);
            }

            /**
             * Failed to create session
             * 
             * @param  {response} response Response from server
             * @return {void}
             */
            function createFailed(response) {
                // Reject promise
                deferred.reject(response);
            }
        }

        /**
         * Destroy the session
         * 
         * @return {void}
         */
        function destroy() {
            // Clear data from HTML storage
            $iwStorage.clear();

            // Reset session object
            vm.$session.authCode = null;
            vm.$session.authenticated = false;
            vm.$session.currency = IWConstants.APP.defaultCurrency;
            vm.$session.language = IWConstants.APP.defaultLanguage;

            // Call destroyed callbacks
            onSessionDestroyedHandler();
        }

        /**
         * Destroy current session and create a new one
         *
         * @return {Promise} Create session promise
         */
        function reset() {
            // Destory session
            destroy();

            // Call reset callbacks
            onSessionResetHandler();

            // Create session
            return create();
        }

        /**
         * Get the session object
         * 
         * @return {Object} Session object
         */
        function getSession() {
            return vm.$session;
        }

        /**
         * Get current authcode
         * 
         * @return {String} Authcode
         */
        function getAuthCode() {
            return vm.$session.authCode;
        }

        /**
         * Set the authcode
         *
         * @param {String} Authcode
         */
        function setAuthCode(authCode) {
            // Set authcode in session
            vm.$session.authCode = authCode;

            // Add authcode to storage
            $iwStorage.add('authCode', authCode);
        }


        /**
         * Get use authenticated status
         * 
         * @return {Boolean}    Authenticated status
         */
        function isAuthenticated() {
            return vm.$session.authenticated;
        }

        /**
         * Set use authenticated status
         * 
         * @param {Boolean}     authenticated   Authenticated status
         */
        function setAuthenticated(authenticated) {
            vm.$session.authenticated = !!authenticated;
        }

        /**
         * Get session language
         * 
         * @return {String}     Language string
         */
        function getLanguage() {
            return vm.$session.language;
        }

        /**
         * Set session language
         * 
         * @param {String} language     Language string
         */
        function setLanguage(language) {
            // Set language in session
            vm.$session.language = language;

            // Add language to storage
            $iwStorage.add('language', language);
        }

        /**
         * Get session currency
         * 
         * @return {String}     Currency string
         */
        function getCurrency() {
            return vm.$session.currency;
        }

        /**
         * Set the session currency
         * 
         * @param {String} currency     Currency string
         */
        function setCurrency(currency) {
            // Set currency in session
            vm.$session.currency = currency;

            // Add currency to storage
            $iwStorage.add('currency', currency);
        }

        /**
         * Add a created session callback hook
         * 
         * @return {void}
         */
        function onSessionCreated(callback) {
            eventHooks.onSessionCreated.push(callback);
        }

        /**
         * Add a destroyed session callback hook
         * 
         * @return {void}
         */
        function onSessionDestroyed(callback) {
            eventHooks.onSessionDestroyed.push(callback);
        }

        /**
         * Add a reset session callback hook
         * 
         * @return {void}
         */
        function onSessionReset(callback) {
            eventHooks.onSessionReset.push(callback);
        }

        /**
         * Created session callbacksrs
         *
         * @param {Promise} session      Create session promise
         * @return {void}
         */
        function onSessionCreateHandler(session) {
            for(var i = 0; i < eventHooks.onSessionCreated.length; i++) {
                try {
                    eventHooks.onSessionCreated[i](session);
                }
                catch(hookError) {
                    //$exceptionHandler(hookError);
                }
            }
        }

        /**
         * Destroyed session callbacksrs
         *
         * @return {void}
         */
        function onSessionDestroyedHandler() {
            for(var i = 0; i < eventHooks.onSessionDestroyed.length; i++) {
                try {
                    eventHooks.onSessionDestroyed[i]();
                }
                catch(hookError) {
                    //$exceptionHandler(hookError);
                }
            }
        }

        /**
         * Reset session callbacks
         * 
         * @param {Promise} session      Create session promise
         * @return {void}
         */
        function onSessionResetHandler(session) {
            for(var i = 0; i < eventHooks.onSessionReset.length; i++) {
                try {
                    eventHooks.onSessionReset[i](session);
                }
                catch(hookError) {
                    //$exceptionHandler(hookError);
                }
            }
        }
    }
})();