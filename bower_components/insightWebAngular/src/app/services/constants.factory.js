(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('IWConstants', iwConstantsFactory)
        .factory('$iwConstants', iwConstantsFactory);

    /** @ngInject */
    function iwConstantsFactory($log, IWConfig) {
        //var vm = this;

        /**
         * Default app configuration
         *
         * @type {Object}
         */
        var IWConfigOptions = {

            // App settings
            APP: {
                domain              : '7lottos.com',
                brand               : '7lottos',
                frontEnd            : '7lottos',
                frontEndId          : 1,
                defaultLanguage     : 'en',
                defaultCurrency     : 'EUR'
            },

            // Use production environment
            PRODUCTION: true,

            // API endpoint config
            API_ROOTS: {
                STAGING: {
                    SERVICE: 'http://win-stg-api.7lottos.com:8080/',
                    CASHIER: 'http://win-stg-cashier.7lottos.com:8080/'
                },
                PRODUCTION: {
                  SERVICE: 'https://api.dr.insight.cloud/',
                  CASHIER: 'https://cashier.dr.insight.cloud/'
                }
            }
        };


        $log.log('Config:', IWConfig);

        // Merge deafult config with any overrides
        if(angular.isDefined(IWConfig)) {
            angular.merge(IWConfigOptions, IWConfig);
        }

        $log.log('-> Config:', IWConfigOptions);

        /**
         * Base / root URLs for API
         *
         * @type {String}
         */
        var API_ROOT = IWConfigOptions.PRODUCTION ? IWConfigOptions.API_ROOTS.PRODUCTION : IWConfigOptions.API_ROOTS.STAGING;

        /**
         * API Endpoints built based on the configuration
         *
         * @type {Object}
         */
        var API_ENDPOINTS = {
            // Session
            SESSION: {
                CREATE                  : API_ROOT.SERVICE + 'wsg-session-1.0/session/createSession',
                SESSION_TIME            : API_ROOT.SERVICE + 'wsg-session-1.0/session/getCurrentSessionTimeInSec'
            },

            // Accoount
            ACCOUNT: {
                REGISTER_QUICK          : API_ROOT.SERVICE + 'wsg-b2b-1.0/register/registrationStep1',
                REGISTER_IS_COMPLETE    : API_ROOT.SERVICE + 'wsg-b2b-1.0/register/isRegistrationStep2Complete',
                REGISTER_FULL           : API_ROOT.SERVICE + 'wsg-b2b-1.0/register/createUser',
                LOGIN                   : API_ROOT.SERVICE + 'wsg-b2b-1.0/login/userLogin',
                LOGOUT                  : API_ROOT.SERVICE + 'wsg-b2b-1.0/logout/userLogout',
                PASSWORD_RESET          : API_ROOT.SERVICE + 'wsg-b2b-1.0/login/forgotPassword',
                PASSWORD_UPDATE         : API_ROOT.SERVICE + 'wsg-b2b-1.0/userInfo/updateUserPassword',
                PROFILE_INFO_FULL       : API_ROOT.SERVICE + 'wsg-b2b-1.0/widget/userInfo/getUserInfo',
                PROFILE_INFO_SHORT      : API_ROOT.SERVICE + 'wsg-b2b-1.0/bioProfile/getBio',
                PROFILE_UPDATE          : API_ROOT.SERVICE + 'wsg-b2b-1.0/bioProfile/bioUpdate',
                EMAIL_UPDATE            : API_ROOT.SERVICE + 'wsg-b2b-1.0/userInfo/updateUserEmail',
                TNC_UPDATE              : API_ROOT.SERVICE + 'wsg-b2b-1.0/userInfo/updateTnC',
                BALANCE                 : API_ROOT.CASHIER + 'wsg-wallet-b2b-1.0/wallet/getBalanceWithAuthCode',
                PURCHASE_HISTORY        : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/getplayslips?authCode=',
                TRANSACTION_HISTORY     : API_ROOT.CASHIER + 'wsg-wallet-b2b-1.0/wallet/getTransactionHistoryWithAuthCode',
                SUBSCRIPTIONS: {
                    ALL                 : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/getmyaccountcarouseldata?authCode=',
                    DETAILS             : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/getusersubscriptionticketdetails?authCode='
                }
            },

            // Cashier
            CASHIER: {
                BALANCE                 : API_ROOT.CASHIER + 'wsg-wallet-b2b-1.0/wallet/getBalanceWithAuthCode',
                DEPOSIT                 : API_ROOT.CASHIER + 'b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/initiatedeposittxn?authCode=',
                DEPOSIT_STATUS          : API_ROOT.CASHIER + 'b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/gettxnstatus?authCode='
            },

            // Responsible gaming
            RESPONSIBLITIES: {
                GET: {
                    DEPOSIT_LIMITS      : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/getRGDepositLimits',
                    LOSS_LIMITS         : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/getRGLossesLimits',
                    WAGER_LIMITS        : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/getRGWageringLimits',
                    COOL_OFF_STATUS     : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/getRGCoolOffStatus'
                },
                UPDATE: {
                    DEPOSIT_LIMITS      : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/updateRGDepositLimits',
                    LOSS_LIMITS         : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/updateRGLossLimits',
                    WAGER_LIMITS        : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/updateRGWagerLimits',
                    COOL_OFF_STATUS     : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/updateRGCoolOffStatus',
                    RG_LIMITS           : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/updateRGLimits'// Not actually used in anything?
                },
                SELF_EXCLUDE            : API_ROOT.SERVICE + 'wsg-b2b-1.0/responsibleGaming/setRgClosed'
            },

            // Products
            PRODUCTS: {
                ALL                     : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/getbuyticketslotterydetailslist',

                // Single line specific
                SINGLELINE: {
                    GENERATE_TICKET     : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/generatetickets',
                    LAST_NUMBER         : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/getusernumbercombinations',
                    RESULTS             : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/getcachedlotteryresultspagedata',
                    DRAWS               : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/getdrawingresult',
                },

                // Syndicate specific
                SYNDICATES: {
                    SYNDICATE           : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/getsyndicates',
                    TICKETS             : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/nus/7lottos/getsyndicatetickets'
                }
            },

            CART: {
                FETCH                   : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/fetchpayslip?authCode=',
                UPDATE                  : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/updatepayslip?authCode=',
                COMMIT                  : API_ROOT.SERVICE + 'b2b-7lottos-1.0/b2b/us/7lottos/createsubscriptions?authCode=',
            },

            // Payment
            PAYMENTS: {
                INIT                    : API_ROOT.CASHIER + 'b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/initiatedeposittxn?authCode=',
                STATUS                  : API_ROOT.CASHIER + 'b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/gettxnstatus?authCode='
            },

            // Bonus
           BONUS: {
               PROMOS                  : API_ROOT.CASHIER + 'promo-core-controller-1.0/service/promo/viewCachedPromos',
               OFFERS                  : API_ROOT.CASHIER + 'promo-core-controller-1.0/service/promo/checkOffers',
               STATUS                  : API_ROOT.CASHIER + 'b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/gettxnstatus?authCode='
           }
        };

        /**
         * Constants object
         */
        var self = {
            /**
             * Application configuration
             */
            APP: IWConfigOptions.APP,

            /**
             * API Root Url
             */
            API_ROOT: API_ROOT,

            /**
             * API URLs
             */
            API_ENDPOINTS: API_ENDPOINTS,

            /**
             * Product types
             */
            PRODUCT_TYPES: {
                SYNDICATE       : 'syndicate',
                SINGLE_LINE     : 'singleLine'
            },

            /**
             * Playing duration
             */
            PLAYING_PERIODS: {
                ONCE            : 'once',
                WEEKLY          : 'weekly',
                SUBSCRIPTION    : 'weekly',
                SET_WEEKS       : 'set_weeks'
            },

            /**
             * Billing periods
             */
            BILLING_PERIODS: {
                WEEKLY          : 'weekly',
                MONTHLY         : 'monthly',
                QUATERLY        : 'quarterly',
                QUARTERLY       : 'quarterly'
            }
        };

        return self;
    }

})();
