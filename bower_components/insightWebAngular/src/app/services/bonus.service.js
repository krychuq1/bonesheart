(function() {
  'use strict';

  angular
    .module('insightWebAngular')
    .factory('$iwBonus', BonusService);

  /** @ngInject */
  function BonusService($log, $http, $q, IWConstants, $iwSession) {
    var vm = this;

    vm.$bonus = {
      'promotions': [],
    };


    /**
     * Bonus service
     *
     * @type {Object}
     */
    var service = {
      init: init,
      checkApplicableBonuses: checkApplicableBonuses,
      checkOffers: checkOffers
    };

    init();

    return service;

    /**
     * Initialise the serivce (first time app is run)
     *
     * @return {void}
     */
    function init() {
      $log.log('Bonus service loaded');
      // get promos
      getPromos();
    }

    /**
     * get All Avaible Promos
     *
     * @return {Promise}
     **/
    function getPromos() {
      // Deferred object - chain of operations
      var deferred = $q.defer();

      // Post login details
      $http.get(IWConstants.API_ENDPOINTS.BONUS.PROMOS)
        .then(promoSuccess, promoFailed);

      // Return promise object
      return deferred.promise;

      function promoSuccess(response) {

        // Log the output
        $log.debug(response.data);

        //assign to local variable
        vm.$bonus.promotions = response.data.response;
        console.log('--> prmos', vm.$bonus.promotions);

        // Resolve
        deferred.resolve(response.data);
      }

      function promoFailed(response) {
        deferred.reject(response);
      }
    }

    /**
     * Bonus Construct
     *
     * @return {Object}
     */
    function Bonus(bonus, qualifier) {
      this.bonus = {
        "promoId": bonus.promoId,
        "qualifierType": bonus.qualifierType,
        "targetType": bonus.targetType,
        "playerCanOptin": bonus.playerCanOptin,
        "awardType": bonus.awardDetail[qualifier].awardType,
        "valueType": bonus.awardDetail[qualifier].valueType,
        "value": bonus.awardDetail[qualifier].value,
        "minRefvalue": bonus.awardDetail[qualifier].minRefvalue,
        "maxValue": bonus.awardDetail[qualifier].maxValue,
        "currency": bonus.awardDetail[qualifier].currency,
        "isFirstBuy": bonus.qualifierDetail[qualifier].isFirstBuy,
        "typeDetail": bonus.qualifierDetail[qualifier].typeDetail,
        "qualifierValue": bonus.qualifierDetail[qualifier].qualifierValue, // minimum to qualify
        "qualifierMinValue": bonus.qualifierDetail[qualifier].qualifierMinValue, // minimum to qualify
        "qualifierMaxValue": bonus.qualifierDetail[qualifier].qualifierMaxValue, // max products to apply discount to
      };

      /**
       * init method
       *
       * @return {void}
       */
      this.init = function() {
        console.log('initialized bonus');
      };

      /**
       * Returns the lottery ticket price after a discout bonus has been applied
       *
       * @return {String}
       */
      this.calculateDiscount = function(ticketprice) {
        var newPrice = 0;
        if (this.bonus.valueType === 'pct') {
          newPrice = this.bonus.value / 100 * ticketprice;
        }

        //return value
        return newPrice.toFixed(1);
      };
    }

    /**
     * checks if a user qualfies for a bonus
     *
     * @return {Promise}
     */
    function checkOffers(cart) {
      console.log('cart-----', cart);
      var count = 0;
      var request = [];
      angular.forEach(cart.items, function(item) {
        console.log(item);
        var requestRow = {
          'authCode': $iwSession.getAuthCode(),
          'productId': item.productId,
          'amount': item.subTotal,
          'currency': item.currency,
          'qualifier': 'subscription',
          'vertical': 'lottery'
        };
        request.push(requestRow);
      });

      // Deferred object - chain of operations
      var deferred = $q.defer();

      // Post login details
      $http({
        'method': 'POST',
        'url': IWConstants.API_ENDPOINTS.BONUS.OFFERS,
        headers: {
          'Content-Type': 'application/json'
        },
        'data': request,
      }).then(apiSuccess, apiError);

      // Return promise object
      return deferred.promise;

      // api success cb
      function apiSuccess(response) {

        // Log the output
        console.log('--- offers response', response);
        var offers = response.data.response;
        deferred.resolve(offers);
      }

      // api error cb
      function apiError(response) {
        deferred.reject(response);
      }

      // return promise
      return deferred.promise;
    }

    /**
     * Checks if lottery meets bonus criteria
     *
     * @return {Boolean}
     */
    function search(array, lottery, bonusType) {
      var match = false;
      var index = 0;
      // type of lottery
      var productType = lottery.productType.toLowerCase();
      var objectKey = productType === 'singleline' ? 'ticket' : 'tickets';
      var productId, currency;
      // lottery id
      productId = lottery.productId !== undefined ? lottery.productId : lottery.id;
      productId = productId.toString();
      // lottery ticket currency
      currency = lottery.productId !== undefined ? lottery.currency : lottery[objectKey].currency;

      // console.log(productId,productType,currency);
      angular.forEach(array, function(row, i) {
        // if qualifier array in bonus
        if (bonusType === 'qualifier') {
          // match found
          if (row.currency === currency) {
            console.log('----> matching ', row.currency, lottery.currency);
            match = true;
            index = i;
            return;
          } else {}
        } else { // if vertical array in bonus
          if (row.gameSubType === productId && row.gameType === productType) {
            // match found
            match = true;
            index = i;
            return;
          } else {}
        }
      });

      // return match
      return {
        'matched': match,
        'index': index
      };
    }

    /**
     *Filters All Bonuses To find matches
     *
     * @return {Object}
     */
    // TODO: also check if a user is logged in and compare with isFirstBuy
    function filterBonuses(lottery, account) {
      // counter
      var count = 0;
      var foundBonus = {};
      angular.forEach(vm.$bonus.promotions, function(bonus) {
        // console.log('--> bonuse id', bonus.promoId);
        var qualify = search(bonus.qualifierDetail, lottery, 'qualifier');
        var vertical = search(bonus.verticalGamesDtls, lottery, 'vertical');
        // console.log('vertical = ' + vertical + ' qualify = ' + qualify);
        // if both a are true make new Bonus
        // TODO : if multiple applicable bonuses choose best option
        if (vertical.matched && qualify.matched) {
          // console.log(vertical.matched, qualify.matched);
          if (!_.isEmpty(account)) { // logged player
            console.log('--- account', account);
            if (account.playRealStatus === 'play' && bonus.qualifierDetail[qualify.index].isFirstBuy === 1) {
              // new Bonus
              console.log(' bonus>', bonus);
              foundBonus = new Bonus(bonus, qualify.index);
              return;
            } else if (account.playRealStatus === 'real' && bonus.qualifierDetail[qualify.index].isFirstBuy === 1) {} else if (bonus.qualifierDetail[qualify.index].isFirstBuy === 0) {
              foundBonus = new Bonus(bonus, qualify.index);
              return;
            }
          } else { // no user logged id
            foundBonus = new Bonus(bonus, qualify.index);
            return;
          }

        }
      });
      // return bonus if any found
      return foundBonus;
    }

    /**
     * checks if a lottery qualiefies for a promo
     *
     * @return {Object}
     **/
    function checkApplicableBonuses(lottery, account) {
      console.log('---> get account = ', account);
      // find if lottery is applicable a promot
      var promo = filterBonuses(lottery, account);
      return promo;
    }
  }

})();
