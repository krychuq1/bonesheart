(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($log, $scope, IWConstants, SingleLineProducts, SyndicateProducts, $timeout, $iwStorage, $iwCart,
                            $iwSession, $iwAccount, SingleLineTicket, $iwProducts, $http, $stateParams, $iwPayments, $iwMedia, $window) {
        var vm = this;

        /**
         * Payments service
         */
        vm.deposit = deposit;
        vm.checkDeposit = checkDeposit;

        vm.depositData = {
            amount: 3,
            cardNumber: 4242424242424242,
            expirationMonth: '04',
            expirationYear: '2019',
            cvv: 122,
            firstName: 'Ben',
            lastName: 'Wasere'
        };

        vm.depositStatus = {
            checking: false,
            success: false,
            error: false,
            message: ''
        };

        // Check the deposit 
        if(angular.isDefined($stateParams.txnId)) {
            checkDeposit();
        }

        function deposit() {

            // Deposit request
            var request = {
                amount: vm.depositData.amount,
                cardNumber: vm.depositData.cardNumber,
                expirationMonth: vm.depositData.expirationMonth,
                expirationYear: vm.depositData.expirationYear,
                cvv: vm.depositData.cvv,
                firstName: vm.depositData.firstName,
                lastName: vm.depositData.lastName,
                redirectionURL: $window.location.href
            };

            // Call the deposit function
            $iwPayments.deposit('wirecard', request)
                .then(depositSuccess, depositFailed);

            function depositSuccess(response) {
                $log.log('DP Success -> ', response);
            }

            function depositFailed(response) {
                $log.log('DP Failed -> ', response);
            }
        }

        function checkDeposit() {
            var transactionId = $stateParams.txnId || '1239';

            // Deposit id
            // transactionId = '23953'; // '1239';
            // transactionId = '1239';

            // Set the local feedback
            setDepositFeedback(true, false, false, '');

            // Check the status
            $iwPayments.checkStatus(transactionId)
                .then(depositSuccess, depositFailed);

            function depositSuccess(response) {
                setDepositFeedback(false, true, false, '');
                $log.log(response);
            }

            function depositFailed(response) {
                $log.log(response);
                if(response.data.status == 'SUCCESS') {
                    setDepositFeedback(false, false, true, response.data.response.processorResponseMessage);
                }
                else {
                    setDepositFeedback(false, false, true, response.data.errorDescription);
                }
            }
        }

        function setDepositFeedback(checking, success, error, message) {
            vm.depositStatus.checking = checking;
            vm.depositStatus.success = success;
            vm.depositStatus.error = error;
            vm.depositStatus.message = message || 'Unknown error - probably posted_to_processor';
        }


        /**
         * Media query service
         */
        vm.queryOneString = '(max-width: 900px)';
        vm.queryOne = $iwMedia(vm.queryOneString);

        vm.queryTwoString = '(min-width: 1500px)';
        vm.queryTwo = $iwMedia(vm.queryTwoString);

        vm.queryThreeString = 'medium only';
        vm.queryThree = $iwMedia(vm.queryTwoString);

        $scope.$watch(function() { return $iwMedia(vm.queryOneString); }, function() {
            vm.queryOne = $iwMedia(vm.queryOneString);
        });

        $scope.$watch(function() { return $iwMedia(vm.queryThreeString); }, function() {
            vm.queryThree = $iwMedia(vm.queryThreeString);
        });


        /**
         * Session
         */
        vm.session = $iwSession.getSession();

        // 
        vm.newSession = function() {
            $iwSession.reset();
        };

        vm.demoAcc = {
            username: 'ben@weareprompt.com',
            password: 'potnoodle'
        };

        vm.login = function() {
            // $iwAccount.login('lulu4@ne0.us', 'qwertyuiop').then(
            // $iwAccount.login('ben@weareprompt.com', 'potnoodle').then(
            $iwAccount.login(vm.demoAcc.username, vm.demoAcc.password).then(
                function success(response) {
                    $log.log('Login success', response);
                }, function failed(response) {
                    $log.log('Login failed', response);
                });
        }

        vm.register = function() {

            var request = {
                firstName: 'Ben',
                lastName: 'Thomson',
                email: 'ben@weareprompting.com' + (Math.floor(Math.random() * 1000) + 1),
                password: '123456789',
                phone: '000000000',
                terms: true
            };

            $log.log('Register with', request);

            vm.demoAcc.username = request.email;
            vm.demoAcc.password = request.password;

            $iwAccount.register(request).then(
                function success(response) {
                    $log.log('Register success', response);
                }, function failed(response) {
                    $log.log('Register failed', response);
                });
        };

        vm.registerTwo = function() {

            var request = {
                title: 'Mr',
                address: '123 Fake Street',
                city: 'London',
                zip: 'SW 123',
                countryCode: 'AL',
                prize: false,
                dob: {
                    day: '14',
                    month: '05',
                    year: '1993'
                },
                currency: 'EUR' 
            };

            $iwAccount.completeRegistration(request).then(
                function success(response) {
                    $log.log('Complete reg success', response);
                },
                function error(response) {
                    $log.log('Complete reg error', response);
                });
        }

        vm.resetPass = function() {
            $iwAccount.resetPassword(vm.demoAcc.username).then(
                function success(response) {
                    $log.log('Reset success', response);
                },
                function error(response) {
                    $log.log('Reset error', response);
                });
        }

        vm.updateEmail = function() {
            var randomEmail = 'ben@weareprompting.com' + (Math.floor(Math.random() * 1000) + 1);

            $log.log('Updating email from ' + vm.session.profile.eMail + ' to ' + randomEmail);

            $iwAccount.updateEmail(randomEmail).then(
                function success(response) {
                    $log.log('Update email success', response);
                },
                function error(response) {
                    $log.error('Update email error', response);
                });
        };

        vm.loadProfile = function() {
            $iwAccount.getProfile().then(
                function success(response) {
                    $log.log('Load profile success', response);
                },
                function error(response) {
                    $log.log('Load profile error', response);
                });
        };

        vm.loginBen = function() {
            vm.demoAcc.username = 'ben@weareprompt.com';
            vm.demoAcc.password = 'potnoodle';
            vm.login();
        };


        /**
         * Single Line Products
         */
        vm.singleLine = {
            results: []
        };

        vm.singleLineResults = function() {
            $log.log('Attempting to get results');

            SingleLineProducts.results().then(
                function success(response) {
                    vm.singleLine.results = response;
                },
                function error(response) {
                    vm.singleLine.results = response;
                }
            );
        }

        vm.syndicate = {
            syndicates: SyndicateProducts.all(),
            tickets: []
        };

        vm.syndicateTickets = function() {
            // SyndicateProducts.tickets(10010).then(
            //     function success(response) {
            //         vm.syndicate.tickets = response;
            //     },
            //     function error(response) {
            //         vm.syndicate.tickets = response;
            //     }
            // );
            
            // SyndicateProducts.getSyndicateData(443).then(
            //     function success(response) {
            //         vm.syndicate.tickets = response;
            //     },
            //     function error(response) {
            //         vm.syndicate.tickets = response;
            //     }
            // );
        }
        vm.syndicateTickets();

        vm.syndicateAll = function() {
            SyndicateProducts.load().then(
                function success(response) {
                    vm.syndicate.syndicates = response;
                    $log.error('LOADED OK');
                },
                function error(response) {
                    vm.syndicate.syndicates = response;
                    $log.error('LOADED NOOOOO');
                }
            );
        }


        /**
         * Deposit Limit
         */
        vm.currentLimits = {
        };

        vm.newLimits = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        vm.allLimits = {};

        vm.limitType = 'deposit';

        vm.getAllLimits = function() {
            $log.log('Getting all limits');
            vm.allLimits = {};

            $iwAccount.getLimits()
                .then(function success(response) {
                    vm.allLimits = response;
                }, getFailed);

            function getFailed(response) {
                $log.error(response.data);
            }
        }

        vm.getLimits = function() {
            vm.currentLimits = {};

            if(vm.limitType == 'deposit') {
                $log.log('Fetching DEPOSIT limits');
                $iwAccount.getDepositLimits(vm.newLimits)
                    .then(function success(response) {
                        vm.currentLimits = response;
                    }, getFailed);
            }
            else if(vm.limitType == 'wager') {
                $log.log('Fetching WAGER limits');
                $iwAccount.getWagerLimits(vm.newLimits)
                    .then(function success(response) {
                        vm.currentLimits = response;
                    }, getFailed);
            }
            else if(vm.limitType == 'loss') {
                $log.log('Fetching LOSS limits');
                $iwAccount.getLossLimits(vm.newLimits)
                    .then(function success(response) {
                        vm.currentLimits = response;
                    }, getFailed);
            }

            function getFailed(response) {
                $log.error('Failed to get limits for ' + vm.limitType, response.data);
            }
        }

        vm.setLimits = function() {

            if(vm.limitType == 'deposit') {
                $iwAccount.setDepositLimits(vm.newLimits)
                    .then(setLimitSuccess, setLimitFailed);
            }
            else if(vm.limitType == 'wager') {
                $iwAccount.setWagerLimits(vm.newLimits)
                    .then(setLimitSuccess, setLimitFailed);
            }
            else if(vm.limitType == 'loss') {
                $iwAccount.setLossLimits(vm.newLimits)
                    .then(setLimitSuccess, setLimitFailed);
            }

            // Change limit view
            vm.currentLimits = vm.allLimits[vm.limitType];

            function setLimitSuccess(response) {
                $log.log('Limit success', response);
            }

            function setLimitFailed(response) {
                $log.log('Limit failed', response);
            }
        }

        vm.changeLimitType = function(type) {
            vm.limitType = type;
        }

        /**
         * Cart Stuff
         */
        vm.cart = $iwCart.getCart();// $iwCart.get();
        vm.cartServer = {};
        vm.cartTotalPrice = 0;


        vm.cartActions = {
            addItem     : addCartItem,
            removeItem  : removeCartItem,
            emptyCart   : emptyCart,
            getSubTotal : getSubTotal
        };

        function addCartItem() {

            // List of tickets in our basket
            var tickets = [
                {
                    primaryNumbers: ['1,2,3,4,5,6,7,8'],
                    secondaryNumbers: ['1,2']
                }
            ];

            // Days to play for the tickets
            var playingDays = [
                1,  // Monday
                2   // Tueday
            ];

            // Playing period (Once, Weekly (subscription) or for X weeks)
            var playingPeriod = IWConstants.PLAY_OPTIONS.ONCE; // 'weekly';

            // ===================================
            
            // Get the lottery object
            var lottery = SingleLineProducts.get(433);

            // Build new SingleLine cart item
            var cartItem = new SingleLineTicket(lottery, tickets, playingDays, playingPeriod, 1, false);

            $iwCart.addItem(cartItem);
        }
        function removeCartItem() {
            $iwCart.removeItem(0);
        }
        function emptyCart() {
            $iwCart.empty();
        }
        function getSubTotal() {
            $log.log('Calculating totals');
            vm.cartTotalPrice = $iwCart.getSubTotal();
        }


        vm.fetchCart = function() {
            $log.log('Fetching LOCAL cart');
            vm.cart = $iwCart.getCart();
        };

        vm.updateCart = function() {
            $log.log('Updating LOCAL cart');


            var local = [
                {
                    name: 'John',
                    items: 100
                },
                {
                    name: 'Steve',
                    items: 90
                },
                {
                    name: 'Ben',
                    items: 3
                }
            ];

            var remote = [
                {
                    name: 'Ben',
                    items: 100
                },
                {
                    name: 'Alex',
                    items: 2
                }
            ];

            // function mergeByProperty(arr1, arr2, prop) {
            //     _.each(arr2, function(arr2obj) {
            //         var arr1obj = _.find(arr1, function(arr1obj) {
            //             return arr1obj[prop] === arr2obj[prop];
            //         });

            //         arr1obj ? _.extend(arr1obj, arr2obj) : arr1.push(arr2obj);
            //     });
            // }

            var merged = angular.merge(local, remote);

            $log.log(merged);

                // {
                //   "items": [
                //     {
                //       "lines": [
                //         {
                //           "primaryNumbers": [
                //             "1,2,3,4,5,6,7,8"
                //           ],
                //           "secondaryNumbers": []
                //         }
                //       ],
                //       "playingDays": [
                //         1,
                //         2
                //       ],
                //       "playingPeriod": "once",
                //       "playingPeriodLength": 1,
                //       "billingPeriod": false,
                //       "total": 0,
                //       "linePrice": 0,
                //       "productType": "singleline",
                //       "name": false,
                //       "drawTypeId": 0,
                //       "productId": 0,
                //       "currency": "SEK",
                //       "valid": false
                //     }
                //   ],
                //   "subTotal": 0
                // }


        };

        vm.fetchCartServer = function() {
            $log.log('Fetching SERVER cart');

            $iwCart.fetchServer().then(
                function success(response) {
                    $log.log('Server cart success', response);
                },
                function error(response) {
                    $log.log('Server cart error', response);
                });
        };

        vm.updateCartServer = function() {
            $log.log('Updating SERVER cart');
        };




        /**
         * Single Line Ticket
         */

        vm.billPeriod = 'weekly';

        vm.singleCartItem = new SingleLineTicket();

        vm.cartTotal = 0;

        vm.singleCart = {
            setLottery      : singleSetLottery,

            // Tickets
            setTickets      : singleSetTickets,
            addTicket       : singleAddTicket,
            emptyTickets    : singleEmptyTickets,

            // Playing period
            setPlaying      : setPlaying,

            // Billing period
            setBilling      : setBillingPeriod,

            // Total price
            calculateTotal  : calculateTotal,

            // Prune
            prune      : pruneItems,

            setPlayingDays: setPlayingDays
        };

        vm.singleCartItem.onTotalPriceChange(priceChangeHook);

        function priceChangeHook(instance) {
            //$log.log('PRICE CHANGED');
        }

        function singleSetLottery() {
            $log.log('Setting lottery');
        }

        // Tickets
        function singleSetTickets() {
            var tickets = [
                {
                    primaryNumbers: ['1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13'],
                    secondaryNumbers: ['2, 2, 3, 4']
                },
                {
                    primaryNumbers: ['1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13'],
                    secondaryNumbers: ['2, 2, 3, 4']
                },
                {
                    secondaryNumbers: ['2, 2, 3, 4']
                }
            ];
            vm.singleCartItem.setLines(tickets);
        }

        function singleEmptyTickets() {
            vm.singleCartItem.emptyLines();
        }

        function singleAddTicket() {
            var ticket = {
                primaryNumbers: ['1, 2, 3, 4, 5'],
                secondaryNumbers: ['2']
            }
            vm.singleCartItem.addLine(ticket);
        }

        function setPlayingDays(amount) {

            var days = [];

            for (var i = 0; i < amount; i++) {
                days.push(i + 1);
            }

            vm.singleCartItem.setPlayingDays(days);
        }

        // Playing period
        function setPlaying(period) {
            // Just once
            if(period == 'once') {
                vm.singleCartItem.setPlayPeriod(IWConstants.PLAY_OPTIONS.ONCE);
            }

            // Every week
            else if(period == 'every') {
                vm.singleCartItem.setPlayPeriod(IWConstants.PLAY_OPTIONS.SUBSCRIPTION);
            }

            // Set number of weeks
            else if(period == 'set') {
                vm.singleCartItem.setPlayPeriod(IWConstants.PLAY_OPTIONS.SET_WEEKS, 7);
            }

            // Try setting a fake period
            else {
                vm.singleCartItem.setPlayPeriod();
            }
        }

        function setBillingPeriod(period) {
            if(period == 'weekly') {
                vm.singleCartItem.setBillingPeriod(IWConstants.BILLING_PERIODS.WEEKLY);
            }
            else if(period == 'monthly') {
                vm.singleCartItem.setBillingPeriod(IWConstants.BILLING_PERIODS.MONTHLY);
            }
            else if(period == 'quaterly') {
                vm.singleCartItem.setBillingPeriod(IWConstants.BILLING_PERIODS.QUATERLY);
            }
        }

        function calculateTotal() {

            // Get total price
            var total = vm.singleCartItem.getTotal();

            // Set total price
            vm.cartTotal = vm.singleCartItem.getTotal();
        }

        function pruneItems() {
            $log.log('Starting pruning...');
            vm.singleCartItem.prune();
        }


        vm.updateSingleItem = function() {

            var cartItem = vm.singleCartItem;

            cartItem.setTickets([]);
        };

        vm.setLottery = function() {
            var lottery = SingleLineProducts.get(432);
            vm.singleCartItem.setData(lottery);
        }

        vm.buildSingleItem = function() {

            // ===================================
            // = Data set in the controller
            // ===================================
            
            // List of tickets in our basket
            var tickets = [
                {
                    primaryNumbers: ['1,2,3,4,5,6,7,8'],
                    secondaryNumbers: ['1,2']
                },
                {
                    primaryNumbers: ['1,2,3,4,5,6,7,8'],
                    secondaryNumbers: ['1,2']
                }
            ];

            // Days to play for the tickets
            var playingDays = [
                1,  // Monday
                2   // Tueday
            ];

            // Playing period (Once, Weekly (subscription) or for X weeks)
            var playingPeriod = IWConstants.PLAY_OPTIONS.ONCE; // 'weekly';

            var playingPeriodLength = 10;

            // Billing period (Weekly, Monthly or every 3 months)
            var billingPeriod = IWConstants.BILLING_PERIODS.WEEKLY; // 'monthly';


            // ===================================
            
            // Get the lottery object
            var lottery = SingleLineProducts.get(433);

            // Build new SingleLine cart item
            var cartItem = new SingleLineTicket(lottery, tickets, playingDays, playingPeriod, playingPeriodLength, billingPeriod);

            // Set to variable
            vm.singleCartItem = cartItem;
        };

        vm.generateTickets = function() {
            $log.log('Generating');
            var date = "2016-09-10T03:00:00.0000000+02:00";

            SingleLineProducts.generateLine(2, date).then(
                function success(response) {
                    $log.log('Success', response);
                },
                function error(response) {
                    $log.log('Error', response);
                });
        }

        vm.getTransactions = function() {
            $iwAccount.getPurchaseHistory().then(
                function success(response) {
                    $log.log(response);
                },
                function error(response) {
                    $log.log(response);
                });
        }

        vm.getBalance = function() {
            $iwAccount.getBalance().then(
                function success(response) {

                },
                function error(response) {

                });
        }
    }

})();
