(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider

        .state('playground', {
            //abstract: true,
            url: '/playground',
            templateUrl: 'app/playground/playground.html',
            controller: 'PlaygroundController',
            controllerAs: 'vm'
        })
        // .state('playground.home', {
        //     url: '',
        //     templateUrl: 'app/playground/session/session.html',
        //     controller: 'SessionDemoController',
        //     controllerAs: 'vm'
        // })
        .state('playground.session', {
            url: '/session',
            templateUrl: 'app/playground/session/session.html',
            controller: 'SessionDemoController',
            controllerAs: 'vm'
        })
        .state('playground.account', {
            url: '/account',
            templateUrl: 'app/playground/account/account.html',
            controller: 'AccountDemoController',
            controllerAs: 'vm'
        })
        .state('playground.lottery', {
            url: '/lottery',
            templateUrl: 'app/playground/lottery/lottery.html',
            controller: 'LotteryDemoController',
            controllerAs: 'vm'
        })
        .state('playground.syndicate', {
            url: '/syndicate',
            templateUrl: 'app/playground/syndicate/syndicate.html',
            controller: 'SyndicateDemoController',
            controllerAs: 'vm'
        })
        .state('playground.cart', {
            url: '/cart',
            templateUrl: 'app/playground/cart/cart.html',
            controller: 'CartDemoController',
            controllerAs: 'vm'
        })
        .state('playground.cashier', {
            url: '/cashier',
            templateUrl: 'app/playground/cashier/cashier.html',
            controller: 'CashierDemoController',
            controllerAs: 'vm'
        })
        .state('playground.media', {
            url: '/media',
            templateUrl: 'app/playground/media/media.html',
            controller: 'MediaDemoController',
            controllerAs: 'vm'
        })
        .state('playground.utils', {
            url: '/utilities',
            templateUrl: 'app/playground/utilities/utilities.html',
            controller: 'UtilsDemoController',
            controllerAs: 'vm'
        });


        $urlRouterProvider.otherwise('/playground/session');
    }

})();
