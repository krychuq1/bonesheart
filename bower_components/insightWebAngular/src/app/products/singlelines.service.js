(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('SingleLineProducts', SingleLineProducts);

    /** @ngInject */
    function SingleLineProducts($log, $q, $filter, $http, $iwSession, IWConstants, $timeout) {
        var vm = this;

        /**
         * Internal array of single line products
         *
         * @type {Array}
         */
        vm.$products = [];

        /**
         * Flag to tell us if the products are loaded or not
         *
         * @type {Boolean}
         */
        vm.loaded = false;

        /**
         * Products service
         *
         * @type {Object}
         */
        var service = {
            init: init,
            load: load,
            get: get,
            all: all,
            one: one,
            generateLine: generateLine,
            results: results,
            drawDetails : drawDetails,
            convertId: convertId,
            loaded: isLoaded
        };

        return service;

        /**
         * Initialise the serivce
         *
         * @return {void}
         */
        function init() {
            $log.log('SingleLineProducts service loaded');

            // Initialise the singleline products object
            vm.$products = [];

            // Load the products
            load();
        }

        /**
         * Load the products from the server
         *
         * @return {Promise}  Promise
         */
        function load() {
            // Deferred object
            var deferred = $q.defer();

            // Set loading flag
            vm.loaded = false;

            // Request params
            var request = {
                addAuth         : false,
                frontendId      : IWConstants.APP.frontEndId,
                currencyCode    : $iwSession.getCurrency(),
                languageId      : $iwSession.getLanguage()
            };

            // Request a list of products
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.ALL, request).then(loadProductsSuccess, loadProductsFailed);

            // Return promise object
            return deferred.promise;

            function loadProductsSuccess(response) {
                // Process the response data and only get the single line products
                var products = processProductList(response);

                // Empty the current array of single line products
                emptyProducts();

                // Add the new products to the array
                addProduct(products);

                // Set loaded flag
                vm.loaded = true;

                // Resolve the promise
                deferred.resolve(all());
            }

            function loadProductsFailed(response) {
                vm.loaded = true;
                deferred.reject(response);
            }
        }

        /**
         * Alias - Return a single product based on product ID
         *
         * @param  {Int} id     Product id
         * @return {Object}     Product object
         */
        function get(id) {
            return one(id);
        }

        /**
         * Return a single product based on product ID
         *
         * @param  {Int} id     Product id
         * @return {Object}     Product object
         */
        function one(id) {
            var found = $filter('filter')(vm.$products, {id: id},
                function(actual, expected) {
                    return actual == expected
                }
            )[0];
            return angular.isDefined(found) ? found : null;
        }

        /**
         * Return all products
         *
         * @return {Array} Array of products
         */
        function all() {
            return vm.$products;
        }

        /**
         * Return all lottery results
         *
         * @return {Promise} Array of lottery results
         */
        function results() {
            // Deferred object
            var deferred = $q.defer();

            // Copied from the old code for now
            var startDate = new Date();
            startDate.setDate(startDate.getDate() - 60);
            startDate = startDate.toISOString();
            var endDate = new Date();
            endDate = endDate.toISOString();

            // Request params
            var request = {
                startDate   : startDate, //'2016-07-08T20:46:08.867Z',
                endDate     : endDate, //'2016-09-06T20:46:08.867Z',
                frontendId  : IWConstants.APP.frontEndId,
                addAuth     : false,
            };

            // Request a list of products
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.SINGLELINE.RESULTS, request)
                .then(resultsSuccess, resultsFailed);

            // Return promise object
            return deferred.promise;

            function resultsSuccess(response) {
                deferred.resolve(response.data.response.lotteryResults);
            }

            function resultsFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Return all draw results
         *
         * @param {startDate}
         * @param {endDate}
         *
         * @return {Promise} Array of lottery draws
         */
        function drawDetails(startDate,endDate) {
            // Deferred object
            var deferred = $q.defer();

            // Request params
            var request = {
                startDate   : startDate, //'2016-07-08T20:46:08.867Z',
                endDate     : endDate, //'2016-09-06T20:46:08.867Z',
                frontendId  : IWConstants.APP.frontEndId,
                addAuth     : false,
            };

            // Request a list of products
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.SINGLELINE.DRAWS, request)
                .then(resultsSuccess, resultsFailed);

            // Return promise object
            return deferred.promise;

            function resultsSuccess(response) {
                deferred.resolve(response.data.response.arr);
            }

            function resultsFailed(response) {
                deferred.reject(response);
            }
        }

        function generateLine(drawTypeId, drawDate) {
            // Deferred object
            var deferred = $q.defer();

            // Request params
            var request = {
                addAuth     : false,
                count       : 1,
                drawTypeId  : drawTypeId,
                date        : drawDate
            };

            // Request a list of products
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.SINGLELINE.GENERATE_TICKET, request)
                .then(generateSuccess, generateFailed);

            // Return promise object
            return deferred.promise;

            function generateSuccess(response) {
                var lineData = response.data.response.arr[0];

                var line = {
                    primaryNumbers: lineData.primaryNumbers.split('-'),
                    secondaryNumbers: lineData.secondaryNumbers.split('-')
                };

                deferred.resolve(line);
            }

            function generateFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Empty the internal array of products
         *
         * @return {void}
         */
        function emptyProducts() {
            vm.$products.length = 0;
        }

        /**
         * Add a product or array of products to the internal array
         *
         * @param {Object|Array} product Single product or array of products
         */
        function addProduct(product) {
            if(product.constructor === Array) {
                angular.forEach(product, function(item) {
                    vm.$products.push(item);
                })
            }
            else {
                vm.$products.push(product);
            }
        }

        /**
         * Seperate the single line products from the others
         *
         * @param  {HttpPromise}    response Server response
         * @return {Array}          Array of single line items
         */
        function processProductList(response) {
            // Products list
            var products = response.data.response.lotteryDetails;

            // Request data
            var requestData = response.config.data;

            // Single line lotteries
            var singleLineProducts = [];

            // Loop through the returned data and grab the single line products
            angular.forEach(products, function(product) {
                if (product.drawTypeId > 0 && product.productDetailsList.length > 0) {
                    // Only get details from the first item in the list
                    var details = product.productDetailsList[0];

                    // Construct new single line data object
                    var singleLine = {
                        drawTypeId: product.drawTypeId,
                        id: details.productId,
                        name: product.lotteryName,
                        jackpot: {
                            original: {
                                value: product.jackpotAmount / 100,
                                currency: product.currency
                            },
                            local: {
                                value: product.jackpotBaseAmount / 100,
                                currency: angular.isDefined(requestData.currencyCode) ? requestData.currencyCode : details.ticketCurrency
                            }
                        },
                        numbers: {
                            primary: {
                                list: product.primaryNumbers,
                                min: product.minPrimaryCount,
                                max: product.maxPrimaryCount,
                                total: product.primaryNumberCount
                            },
                            secondary: {
                                list: product.secondaryNumbers,
                                min: product.minSecondaryCount,
                                max: product.maxSecondaryCount,
                                total: product.secondaryNumberCount
                            }
                        },
                        ticket: {
                            price: details.ticketPrice / 100,
                            currency: details.ticketCurrency,
                            drawDays: details.drawDayList
                        },
                        drawDates: {
                            next: product.drawDate,
                            all: product.availableDrawDates,
                            cutOff: product.availableDrawCutoffDates
                        },
                        productType: IWConstants.PRODUCT_TYPES.SINGLE_LINE
                    };

                    // Push to new single line lottery array
                    singleLineProducts.push(singleLine);
                }
            });

            // Return the list of singleline products
            return singleLineProducts;
        }


        /**
         * Get product id from draw type id
         *
         * @param  {Number} drawTypeId      Draw type id
         * @return {Number}                 Product id
         */
        function convertId(drawTypeId) {
            var found = $filter('filter')(vm.$products, {drawTypeId: drawTypeId},
                function(actual, expected) {
                    return actual == expected
                }
            )[0];

            if(angular.isDefined(found)) {
                return found.id;
            }
            else {
                return false;
            }
        }

        /**
         * Check if products have been loaded yet
         *
         * @return {Boolean} True if loaded false if not
         */
        function isLoaded() {
            return vm.loaded;
        }
    }

})();
