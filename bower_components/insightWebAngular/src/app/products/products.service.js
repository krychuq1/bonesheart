(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwProducts', ProductsService);

    /** @ngInject */
    function ProductsService($log, $q, $filter, SingleLineProducts, SyndicateProducts) {
        //var vm = this;

        /**
         * Products service
         * 
         * @type {Object}
         */
        var service = {
            init: init,

            // Load all products
            load: load,

            loaded: isLoaded,

            // Individual product options
            SingleLines: SingleLineProducts,
            Syndicates: SyndicateProducts,

            toProductId: toProductId
        };
        return service;

        /**
         * Initialise the serivce (first time app is run)
         * 
         * @return {void}
         */
        function init() {
            //$log.log('Products service loaded');

            // Load the initial list of products
            load();
        }

        /**
         * Load all products
         * 
         * @return {Promise} Promise
         */
        function load() {
            // Deferred object
            var deferred = $q.defer();

            // Load all products
            $q.all([
                SingleLineProducts.load(),
                SyndicateProducts.load()
            ])
            .then(loadProductsSuccess, loadProductsFailed);

            // Return promise object
            return deferred.promise;

            function loadProductsSuccess(response) {
                deferred.resolve(response);
            }

            function loadProductsFailed(response) {
                deferred.reject(response);
            }
        }


        /**
         * Convert a draw type ID to a product id
         * 
         * @param  {Number} drawTypeId      Draw type ID
         * @return {Number|Boolean}         Number if id found or FALSE if not
         */
        function toProductId(drawTypeId) {

            // Get the array of syndicates and single lines
            var singleLines = angular.copy(SingleLineProducts.all()),
                syndicates  = angular.copy(SyndicateProducts.all());

            // Join the two arrays
            var joined = singleLines.concat(syndicates);

            // Try to find the product
            var found = $filter('filter')(joined, {drawTypeId: drawTypeId}, 
                function(actual, expected) { 
                    return actual == expected 
                }
            )[0];

            $log.log(found);

            // Return result
            return angular.isDefined(found) ? found.id : false;
        }

        /**
         * Check if products have been loaded yet
         * 
         * @return {Boolean} True if loaded false if not
         */
        function isLoaded() {
            return SingleLineProducts.loaded() && SyndicateProducts.loaded();
        }
    }

})();