(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('SyndicateProducts', SyndicateProducts);

    /** @ngInject */
    function SyndicateProducts($log, $q, $filter, $iwSession, $http, IWConstants) {
        var vm = this;

        /**
         * Internal array of syndicates
         *
         * @type {Array}
         */
        vm.$syndicates = [];

        vm._$syndicates = [];

        /**
         * Flag to tell us if the products are loaded or not
         *
         * @type {Boolean}
         */
        vm.loaded = false;

        /**
         * Syndicate public functions
         *
         * @type {Object}
         */
        var service = {
            init: init,
            load: load,
            get: get,
            all: all,
            one: one,
            loaded: isLoaded
        };
        return service;

        /**
         * Initialise the serivce
         *
         * @return {void}
         */
        function init() {
            $log.log('SyndicateProducts service loaded');

            // Initialise the syndicates array
            vm.$syndicates = [];

            // Load the syndicates
            load();
        }

        /**
         * Load the syndicates from the server
         *
         * @return {Promise}  Promise
         */
        function load() {

            // Deferred object
            var deferred = $q.defer();

            // Chain the commands 1 by 1
            getSyndicates()
                // Syndicate list results
                .then(function() { return loadSyndicateShares(); }, loadProductsFailed)

                // Shares results
                .then(function() { return loadSyndicateTickets(); }, loadProductsFailed)

                // Tickets results (final)
                .then(loadProductsSuccess, loadProductsFailed);


            // Return promise object
            return deferred.promise;

            function loadProductsSuccess(response) {
                //deferred.resolve(response);

                emptyProducts();

                // Add the new syndicates to the array
                addProduct(vm._$syndicates);

                // Resolve the promise
                deferred.resolve(all());
            }

            function loadProductsFailed(response) {
                deferred.reject(response);
            }
        }


        /**
         * Load syndicate products from API and apply to syndicates
         *
         * @return {Promise}    Promise
         */
        function loadSyndicateTickets() {

            var deferred = $q.defer();

            // Syndicate shares promises
            var promises = [];

            // Create an array of promises for the syndicate shares
            angular.forEach(vm._$syndicates, function(syndicate) {
                promises.push(
                    getSyndicateTickets(syndicate.syndicateId)
                );
            });

            // Load all shares
            $q.all(promises).then(loadTicketsSuccess, loadTicketsFailed);

            function loadTicketsSuccess(response) {
                // Assign data to correcy syndicate
                angular.forEach(vm._$syndicates, function(syndicate, index) {
                    syndicate.tickets.list = groupTickets(response[index]);
                });

                // Resolve promise
                deferred.resolve();
            }

            function loadTicketsFailed(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }

        /**
         * Load syndicate shares from API and apply to syndicates
         *
         * @return {Promise}    Promise
         */
        function loadSyndicateShares() {
            var deferred = $q.defer();

            // Syndicate shares promises
            var promises = [];

            // Create an array of promises for the syndicate shares
            angular.forEach(vm._$syndicates, function(syndicate) {
                promises.push(
                    getSyndicateShares(syndicate.id)
                );
            });

            // Load all shares
            $q.all(promises).then(loadSyndicatesSuccess, loadSyndicatesFailed);

            function loadSyndicatesSuccess(response) {

                // Assign data to correcy syndicate
                angular.forEach(vm._$syndicates, function(syndicate, index) {
                    syndicate.shares = response[index].shareCount;
                    syndicate.syndicateId = response[index].syndicateId;
                });

                // Resolve promise
                deferred.resolve();
            }

            function loadSyndicatesFailed(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }


        /**
         * Get a list of syndicates from the API
         *
         * @return {Promise}    Promise
         */
        function getSyndicates() {
            // Deferred object
            var deferred = $q.defer();

            // Request params
            var request = {
                addAuth: false,
                frontendId: IWConstants.APP.frontEndId,
                currencyCode: $iwSession.getCurrency(),
                languageId: $iwSession.getLanguage()
            };

            // Request a list of ALL products
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.ALL, request).then(loadProductsSuccess, loadProductsFailed);

            // Return promise object
            return deferred.promise;

            function loadProductsSuccess(response) {
                // Process the response data and only get the syndicate products
                var syndicates = processProductList(response);

                vm._$syndicates = syndicates;

                // Resolve the promise
                //deferred.reject('NOPE');
                deferred.resolve(syndicates);
            }

            function loadProductsFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get shares and syndicate id for a syndicate product id
         *
         * @param  {Int} productId      Product id
         * @return {Promise}            Promise
         */
        function getSyndicateShares(productId) {

            // Deferred object
            var deferred = $q.defer();

            // Request params
            var request = {
                frontendId: IWConstants.APP.frontEndId,
                productId: productId,
                maxCount: 0,
                addAuth: false
            };

            //
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.SYNDICATES.SYNDICATE, request)
                .then(syndicateDataSuccess, syndicateDataFailed);

            // Return promise object
            return deferred.promise;

            function syndicateDataSuccess(response) {
                deferred.resolve(response.data.response.syndicates[0]);
            }

            function syndicateDataFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get shares and syndicate id for a syndicate
         *
         * @param  {Int} productId      Syndicate id
         * @return {Promise}            Promise
         */
        function getSyndicateTickets(syndicateId) {

            // Deferred object
            var deferred = $q.defer();

            // Request params
            var request = {
                frontendId: IWConstants.APP.frontEndId,
                syndicateId: syndicateId,
                addAuth: false
            };

            // Request tickets for syndicate id
            $http.post(IWConstants.API_ENDPOINTS.PRODUCTS.SYNDICATES.TICKETS, request)
                .then(ticketsSuccess, ticketsFailed);

            // Return promise object
            return deferred.promise;

            function ticketsSuccess(response) {
                deferred.resolve(response.data.response.tickets);
            }

            function ticketsFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Alias - Return a single syndicate based on product ID
         *
         * @param  {Int}    id      Product id
         * @return {Object}         Product object
         */
        function get(id) {
            return one(id);
        }

        /**
         * Return a single syndicate based on product ID
         *
         * @param  {Int}    id      Product id
         * @return {Object}         Product object
         */
        function one(id) {
            var found = $filter('filter')(vm.$syndicates, {id: id},
                function(actual, expected) {
                    return actual == expected
                }
            )[0];
            return angular.isDefined(found) ? found : null;
        }

        /**
         * Return all syndicates
         *
         * @return {Array} Array of syndicates
         */
        function all() {
            return vm.$syndicates;
        }

        /**
         * Empty the internal array of syndicates
         *
         * @return {void}
         */
        function emptyProducts() {
            vm.$syndicates.length = 0;
        }

        /**
         * Add a product or array of syndicates to the internal array
         *
         * @param {Object|Array}  product   Single product or array of syndicates
         */
        function addProduct(product) {
            if(product.constructor === Array) {
                angular.forEach(product, function(item) {
                    vm.$syndicates.push(item);
                })
            }
            else {
                vm.$syndicates.push(product);
            }
        }

        /**
         * Seperate the syndicate products from the others
         *
         * @param  {HttpPromise} response   Server response
         * @return {Array}                  Array of single line items
         */
        function processProductList(response) {
            // Products list
            var products = response.data.response.lotteryDetails;

            // Request data
            // var requestData = response.config.data;

            // Syndicate products
            var syndicateProducts = [];

            // Check we have data returned
            if(angular.isDefined(products) && products.length > 0) {

                // Loop through every product returned from the API
                angular.forEach(products, function(product) {

                    // Only get syndicate products
                    if(product.drawTypeId === 0 && product.productDetailsList.length > 0) {

                        // LPM and the Java doesn't currently support multiple syndicate groups. Because of this we are going to
                        // make life a bit easier and just make every syndicate variant it's own product for now.
                        angular.forEach(product.productDetailsList, function(details) {

                            // Reformat the syndicate data
                            var syndicate = {
                                drawTypeId: product.drawTypeId,
                                drawDate: product.drawDate,
                                id: details.productId,
                                name: product.lotteryName,
                                syndicateName: details.productName,
                                jackpot: {
                                    value: product.jackpotAmount / 100,
                                    currency: product.currency
                                },
                                tickets: {
                                    total: details.noOfTickets,
                                    price: details.ticketPrice / 100,
                                    currency: details.ticketCurrency,
                                    list: []
                                },
                                drawDays: details.drawDayList,
                                // Shares -> Grab from another API
                                shares: {
                                    total: 100,
                                    remaining: 0
                                },
                                productType: IWConstants.PRODUCT_TYPES.SYNDICATE
                            };

                            // Add to our syndicates array
                            syndicateProducts.push(syndicate);
                        });
                    }
                });
            }

            // Return the final list of all syndicates
            return syndicateProducts;
        }

        /**
         * Group tickets by draw type id
         *
         * @param  {Array} tickets Tickets
         * @return {Array}         Array of tickets grouped by draw type id
         */
        function groupTickets(tickets) {

            var list = {};

            // Loop through each ticket
            angular.forEach(tickets, function(ticket) {

                // Create ticket key in final array list
                if(angular.isUndefined(list[ticket.drawTypeId])) {
                    list[ticket.drawTypeId] = [];
                }

                // Sort the ticket data out
                ticket.primaryNumbers = ticket.primaryNumbers.split('-');
                ticket.secondaryNumbers = ticket.secondaryNumbers.split('-');

                // Add to array
                list[ticket.drawTypeId].push(ticket);
            });

            // Return final list
            return list;
        }

        /**
         * Check if products have been loaded yet
         *
         * @return {Boolean} True if loaded false if not
         */
        function isLoaded() {
            return vm.loaded;
        }
    }

})();
