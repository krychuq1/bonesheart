(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwPayments', PaymentsService);

    /** @ngInject */
    function PaymentsService($log, $q, $http, WirecardHPP, $window, $iwSession, IWConstants) {
        //var vm = this;

        /**
         * Payments service
         * 
         * @type {Object}
         */
        var service = {
            init: init,
            deposit: deposit,
            checkStatus: checkStatus
        };
        return service;

        /**
         * Initialise the serivce (first time app is run)
         * 
         * @return {void}
         */
        function init() {
            $log.log('Payments service loaded');
        }

        /**
         * Initiate a deposit on the server
         * 
         * @param  {Object} request     Initiate request params
         * @return {Promise}            Promise object
         */
        function initiate(request) {

            // Deferred object
            var deferred = $q.defer();
            
            // Initiate request data
            var initiateRequest = {
                agent           : request.agent || 'system',
                comments        : request.comments || 'Transaction initiated by user',
                amount          : request.amount,
                currency        : request.currency || $iwSession.getCurrency(),
                paymentMethod   : request.paymentMethod || 'cc',
                instrumentId    : request.instrumentId || 0,
                customParams    : request.customParams || {},
                redirectionURL  : request.redirectionURL,
                addAuth         : false
            };

            // Post to API
            $http.post(IWConstants.API_ENDPOINTS.PAYMENTS.INIT + $iwSession.getAuthCode(), initiateRequest)
               .then(initateSuccess, initateFailed);

            // Return promise object
            return deferred.promise;

            function initateSuccess(response) {
                deferred.resolve(response);
            }

            function initateFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Deposit money into account
         * 
         * @param  {String} processor       Processor to use
         * @param  {Object} depositData     Payment data and initiate request params
         * @return {Promoise}               Promise
         */
        function deposit(processor, depositData) {

            // Deferred object
            var deferred = $q.defer();

            // Initiate the deposit request
            initiate(depositData)
                .then(depositSuccess, depositFailed);

            // Return promise object
            return deferred.promise;

            function depositSuccess(response) {

                // Build request
                var wirecardData = getWirecardData(depositData, response.data.response);

                // Initiate payment with wirecard
                WirecardHPP.pay(wirecardData);

                // This probably wont happen much because the payment providers will redirect us off to their site and back again
                deferred.resolve(response);
            }

            function depositFailed(response) {
                deferred.reject(response);
            }
        }


        /**
         * Build the json array object with the correct data to deposit on wirecard
         * 
         * @param  {Object} paymentData                 Payment details
         * @param  {HttpResponse} providerResponse      Server response for payment request
         * @return {Object}                             Wirecard data object
         */
        function getWirecardData(depositData, serverResponse) {

            // Wirecard request data
            var request = {
                'request_time_stamp'        : serverResponse.custom1,
                'request_id'                : serverResponse.txnId,
                'merchant_account_id'       : serverResponse.custom2,
                'transaction_type'          : serverResponse.custom3,
                'requested_amount'          : serverResponse.custom7,
                'requested_amount_currency' : serverResponse.currency,
                'ip_address'                : serverResponse.custom5,
                'redirect_url'              : serverResponse.custom4,
                'locale'                    : $window.navigator.language.toLowerCase(),
                'request_signature'         : serverResponse.custom6,

                'last_name'                 : depositData.lastName,
                'first_name'                : depositData.firstName,
                'payment_method'            : 'creditcard', // paymentData.paymentMethod,
                'card_type'                 : 'visa', // paymentData.cardType,
                'card_security_code'        : depositData.cvv
            };

            // Add extra fields depending on the server response
            if (serverResponse.tokenId) {
                request.token_id = serverResponse.tokenId;
            }
            else {
                request.account_number = depositData.cardNumber;
                request.expiration_month = depositData.expirationMonth;
                request.expiration_year = depositData.expirationYear;
            }

            // Return final request object
            return request;
        }


        /**
         * Check the status of a depsit based on the id
         * 
         * @param  {Int} transactionId      Transaction id
         * @return {Promise}                Promise
         */
        function checkStatus(transactionId) {

            // Deferred object
            var deferred = $q.defer();

            // Replace any text that may not have been removed from the query params
            transactionId = transactionId.replace('?txnId=', '');

            // Post to API
            $http.post(IWConstants.API_ENDPOINTS.PAYMENTS.STATUS + $iwSession.getAuthCode(), transactionId)
               .then(statusSuccess, statusFailed);

            // $http.post('https://cashier.7lottos.com/b2b-wsg-cashier-deposit-controller-1.0/b2b/service/cashier/gettxnstatus?authCode=' + authCode, transactionId)
            //     .then(statusSuccess, statusFailed);

            // Return promise object
            return deferred.promise;

            function statusSuccess(response) {
                if(angular.isDefined(response.data.response.status == 'processor_failed')) {
                    deferred.reject(response);
                }
                else {
                    deferred.resolve(response);
                }
            }

            function statusFailed(response) {
                deferred.reject(response);
            }
        }
    }

})();
