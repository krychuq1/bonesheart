(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .factory('$iwCashier', CashierService);

    /** @ngInject */
    function CashierService($log, $q, $http, $iwSession, IWConstants) {
        //var vm = this;

        /**
         * Cashier service
         * 
         * @type {Object}
         */
        var service = {
            init: init,
            deposit : deposit,
            getBalance: getUserBalance,
            depositStatus: checkDepositStatus
        };

        init();

        return service;

        /**
         * Initialise the serivce (first time app is run)
         * 
         * @return {void}
         */
        function init() {
            $log.log('-> Cashier service loaded');
        }


        function initiate() {

        }

        function deposit() {

        }


        /**
         * Check the status of a pending deposit transaction
         * 
         * @param  {number}     transactionId   Transaction id to check
         * @return {Promise}                    A promise that resolves with status information or rejects on error
         */
        function getDepositStatus(transactionId) {
            var deferred = $q.defer();

            // Deposit status request
            var statusRequest = {};

            // Request transaction information
            $http.post(IWConstants.API_ENDPOINTS.CASHIER.DEPOSIT_STATUS + $iwSession.getAuthCode(), transactionId)
                .then(depositStatusSuccess, depositStatusFailed);

            return deferred.promise;

            function depositStatusSuccess(response) {
                if(angular.isDefined(response.data.response.status == 'processor_failed')) {
                    deferred.reject(response);
                }
                else {
                    deferred.resolve(response);
                }
            }

            function depositStatusFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Get the users balance value
         * 
         * @return {Number} Balance value
         */
        function getBalance() {
            var deferred = $q.defer();

            // Request the balance data
            getBalanceData().then(getBalanceSuccess, getBalanceFailed);

            return deferred.promise;

            /**
             * Successful retrieval of balance
             * 
             * @param  {Object} balanceData     Http response object
             * @return {Number}                 Balance value
             */
            function getBalanceSuccess(balanceData) {
                deferred.resolve(balanceData.balance);
            }

            /**
             * Error with request to API
             * 
             * @param  {Object} response        Http response object
             * @return {Object}                 Error information
             */
            function getBalanceFailed(response) {
                deferred.reject(response);
            }
        }

        /**
         * Gets all the balance data from the API
         * 
         * @return {Object}     Balance information
         */
        function getBalanceData() {
            var deferred = $q.defer();

            // Balance request
            var balanceRequest = {};

            // Request balance
            $http.post(IWConstants.API_ENDPOINTS.CASHIER.BALANCE, balanceRequest)
                .then(getBalanceDataSuccess, getBalanceDataFailed);

            return deferred.promise;

            /**
             * Successful request for data
             * 
             * @param  {Object} response    Http response object
             * @return {Object}             Balance information data
             */
            function getBalanceDataSuccess(response) {
                deferred.resolve(response.data);
            }

            /**
             * Error with request to API
             * 
             * @param  {Object} response    Http response object
             * @return {Object}             Error information
             */
            function getBalanceFDataailed(response) {
                deferred.reject(response);
            }
        }
    }

})();