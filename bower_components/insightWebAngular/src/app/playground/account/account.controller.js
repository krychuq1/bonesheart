(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('AccountDemoController', AccountDemoController);

    /** @ngInject */
    function AccountDemoController($log, $iwAccount, $iwProducts) {
        var vm = this;

        // Account data object
        vm.account = $iwAccount.getAccount();

        // Balance
        vm.balanceResponse = {};
        vm.getBalance = getBalance;

        // Registration step 1
        vm.registerData = {
            firstName: 'Ben',
            lastName: 'Wasere',
            email: 'ben@weareprompting.com' + (Math.floor(Math.random() * 1000) + 1),
            password: '123456789',
            phone: '000000000',
            terms: true
        };
        vm.registerResponse = {};

        // Registration step 1
        vm.registerDataTwo = {
            title: 'Mr',
            address: '123 Fake Street',
            city: 'London',
            zip: 'SW 123',
            countryCode: 'AL',
            prize: false,
            dob: {
                day: '14',
                month: '05',
                year: '1993'
            },
            currency: 'EUR' 
        };
        vm.registerTwoResponse = {};


        vm.transactionResponse = {};
        vm.getTransactions = getTransactions;


        // Login
        vm.loginData = {
            username: 'sevenben@weareprompt.com',
            password: 'potnoodle'
        };
        vm.loginResponse = {};


        // Reset password
        vm.resetPasswordEmail = 'ben@weareprompting.com347';
        vm.resetPasswordResponse = {};

        // Update profile
        vm.profileResponse = {};

        // Subscriptions
        vm.subscriptionResponse = {};
        vm.subscriptionDetails = {};
        vm.subscriptions = [];

        // Actions
        vm.fetchAccount = fetchAccount;
        vm.register = register;
        vm.registerTwo = registerTwo;
        vm.login = login;
        vm.logout = logout;
        vm.resetPassword = resetPassword;
        vm.updateProfile = updateProfile;
        vm.getSubscriptions = getSubscriptions;
        vm.getSubscriptionDetails = getSubscriptionDetails;

        function fetchAccount() {
            $log.log('-> Fetching account');
        }

        function register() {
            $log.log('-> Register step 1');

            $iwAccount.register(vm.registerData).then(
                function success(response) {
                    vm.registerResponse = response;

                    // Reset password email
                    vm.resetPasswordEmail = vm.registerData.email;

                    // Login after register
                    $iwAccount.login(vm.registerData.email, vm.registerData.password)
                        .then(function success(response) {
                            vm.loginResponse = response;
                        });
                },
                function error(response) {
                    vm.registerResponse = response;
                }
            );
        }

        function registerTwo() {
            $log.log('-> Register step 2');

            $iwAccount.completeRegistration(vm.registerDataTwo).then(
                function success(response) {
                    vm.registerTwoResponse = response;
                },
                function error(response) {
                    vm.registerTwoResponse = response;
                });
        }

        function login() {
            $log.log('-> Login');
            $iwAccount.login(vm.loginData.username, vm.loginData.password).then(
                function success(response) {
                    vm.loginResponse = response;
                },
                function error(response) {
                    vm.loginResponse = response;
                }
            );
        }

        function logout() {
            $log.log('-> Logging out');
            $iwAccount.logout();
            vm.loginResponse = {};
        }

        function resetPassword() {
            $log.log('-> Resetting password');

            $iwAccount.resetPassword(vm.resetPasswordEmail).then(
                function success(response) {
                    vm.resetPasswordResponse = response;
                },
                function error(response) {
                    vm.resetPasswordResponse = response;
                }
            );
        }

        function getBalance() {
            $iwAccount.getBalance().then(
                function success(response) {
                    vm.balanceResponse = response;
                },
                function error(response) {
                    vm.balanceResponse = response;
                }
            );
        }

        function updateProfile() {

            var request = {
                address: 'The Lodge 123',
                city: 'Windsor',
                zip: '1212',
                phone: '00000000'
            };

            $iwAccount.updateProfile(request).then(
                function success(response) {
                    vm.profileResponse = response;
                },
                function error(response) {
                    vm.profileResponse = response;
                }
            );
        }

        function getSubscriptions() {
            vm.subscriptionResponse = {};
            $iwAccount.getSubscriptions().then(function success(response) {
                vm.subscriptions = response;
            }, function error(response) {
                $log.log('Ticket failed', response);
                vm.subscriptionResponse = response;
            });
        }

        function getSubscriptionDetails(subscriptionId) {
            vm.subscriptionDetails = {};

            // 
            $iwAccount.getSubscriptionDetails(subscriptionId).then(
                function success(response) {
                    vm.subscriptionDetails = response;
                },
                function error(response) {
                    $log.log('Ticket failed', response);
                    vm.subscriptionDetails = response;
                }
            );
        }

        function getTransactions() {

            var start = '';
            var end = '';

            var startDate = moment().subtract(60, 'days').unix();
            var endDate = moment().unix();

            $log.log(startDate, endDate);

            $iwAccount.getTransactionHistory(startDate, endDate).then(
                function success(response) {
                    $log.log('Transaction worked', response);
                    vm.transactionResponse = response;
                },
                function error(response) {
                    $log.log('Transaction failed', response);
                    vm.transactionResponse = response;
                }
            );
        }
    }

})();
