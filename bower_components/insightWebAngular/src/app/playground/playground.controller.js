(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('PlaygroundController', PlaygroundController);

    /** @ngInject */
    function PlaygroundController($log, $state, $iwSession) {
        var vm = this;

        // $log.log('Playground controller loaded');

        vm.activeTab = 1;

        switch($state.current.name) {
            case 'playground.session':
                vm.activeTab = 1;
                break;
            case 'playground.account':
                vm.activeTab = 2;
                break;
            case 'playground.lottery':
                vm.activeTab = 3;
                break;
            case 'playground.syndicate':
                vm.activeTab = 4;
                break;
            case 'playground.cart':
                vm.activeTab = 5;
                break;
            case 'playground.cashier':
                vm.activeTab = 6;
                break;
            case 'playground.media':
                vm.activeTab = 7;
                break;
            case 'playground.utilities':
                vm.activeTab = 8;
                break;
            default:
                vm.activeTab = 1;
        }
    }

})();
