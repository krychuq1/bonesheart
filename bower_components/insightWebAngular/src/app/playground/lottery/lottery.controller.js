(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('LotteryDemoController', LotteryDemoController);

    /** @ngInject */
    function LotteryDemoController($log, $iwProducts, SingleLineProducts, SingleLineTicket, $iwCart,$iwBonus,$iwAccount) {
        var vm = this;

        //$log.log( 'both', $iwProducts.loaded(), 'sin', SingleLineProducts.loaded() );

        // Loading status
        vm.loading = true;

        // Array of lotteries
        vm.lotteries = [];

        // Selected lottery
        vm.selectedLottery = {};

        // Lottery ticket
        vm.lotteryTicket = new SingleLineTicket();

        // Actions
        vm.addToCart = addToCart;
        vm.restoreJson = restoreJson;
        vm.loadToTicket = loadToTicket;
        vm.checkBonus = checkBonus;

        // Load the lotteries
        SingleLineProducts.load().then(function success(response) {
            vm.lotteries = SingleLineProducts.all();
            vm.selectedLottery = vm.lotteries[0];
            vm.loading = false;
        });

        // Load the results
        SingleLineProducts.results().then(function success(response) {

            $log.log('Results', response);

        });

        function loadToTicket() {
            vm.lotteryTicket.setData(vm.selectedLottery);
        }

        function addToCart() {
            $iwCart.addItem(vm.lotteryTicket);
        }

        function checkBonus(){
          console.log(vm.selectedLottery);
          var account = $iwAccount.getAccount().profile === {} ? undefined : $iwAccount.getAccount().profile;
          var promo =  $iwBonus.checkApplicableBonuses(vm.lotteryTicket,account);
          console.log(promo);
          //console.log(promo.calculateDiscount(3));
          vm.lotteryTicket.promo = promo;
        }

        function restoreJson() {
            var json = '{\r\n  \"name\": \"Austria Lotto 6aus45\",\r\n  \"productId\": 10,\r\n  \"drawTypeId\": 7,\r\n  \"lines\": [\r\n    {\r\n      \"primaryNumbers\": [\r\n        0,\r\n        1,\r\n        2\r\n      ],\r\n      \"secondaryNumbers\": []\r\n    }\r\n  ],\r\n  \"drawDays\": [\r\n    2,\r\n    7,\r\n    1\r\n  ],\r\n  \"linePrice\": 25,\r\n  \"playingPeriod\": \"subscription\",\r\n  \"playingPeriodLength\": 1,\r\n  \"billingPeriod\": \"monthly\",\r\n  \"productType\": \"singleLine\",\r\n  \"subTotal\": 300,\r\n  \"currency\": \"SEK\"\r\n}\r\n';

            // Create a new ticket item
            vm.lotteryTicket = new SingleLineTicket();

            // Restore the ticket
            vm.lotteryTicket.restoreFromJSON(json);
        }

        // // Data for converting id
        // vm.convertId = {
        //     draw: 11,
        //     product: 0
        // };

        // // Lottery item
        // vm.lottery = {};

        // vm.singleLine = new SingleLineTicket();

        // // Loaded

        // vm.convert = function() {
        //     vm.convertId.product = SingleLineProducts.convertId(vm.convertId.draw);
        // }

        // vm.fetch = function() {
        //     SingleLineProducts.load().then(
        //     function success(response) {
        //         vm.lotteries = response;
        //         vm.lottery = response[0];
        //     }, function error(response) {
        //         vm.lotteries = response;
        //     });
        // }

        // vm.fetch();
    }

})();
