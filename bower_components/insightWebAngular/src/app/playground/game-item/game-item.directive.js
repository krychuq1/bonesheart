(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .directive('gameItem', gameItem);

    /** @ngInject */
    function gameItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/playground/game-item/game-item.html',
            scope: {
                data: '='
            },
            link: GameItemLink,
            replace: true,
            controller: GameItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function GameItemLink(scope, iElement, iAttrs, controller, transclude) {

            // 
        }

        /** @ngInject */
        function GameItemController($log, IWConstants) {
            var vm = this;

            // Constants for HTLM access
            vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
            vm.PLAY_OPTIONS = IWConstants.PLAY_OPTIONS;
            vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

            vm.increasePlayWeeks = increasePlayWeeks;
            vm.decreasePlayWeeks = decreasePlayWeeks;

            function decreasePlayWeeks() {
                var weeks = vm.cartItem.playingPeriodLength;
                if(weeks > 1) {
                    weeks = weeks -= 1;
                    vm.cartItem.setPlayPeriod(vm.cartItem.playingPeriod, weeks);
                }
            }

            function increasePlayWeeks() {
                var weeks = vm.cartItem.playingPeriodLength += 1;
                vm.cartItem.setPlayPeriod(vm.cartItem.playingPeriod, weeks);
            }
        }
    }

})();
