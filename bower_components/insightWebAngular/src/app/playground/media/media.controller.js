(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('MediaDemoController', MediaDemoController);

    /** @ngInject */
    function MediaDemoController($log, $scope, $iwMedia, $timeout) {
        var vm = this;

        // $log.log('Cashier demo controller loaded');
        

        vm.queryOneString = '(max-width: 900px)';
        vm.queryOne = $iwMedia(vm.queryOneString);

        vm.queryTwoString = '(min-width: 1500px)';
        vm.queryTwo = $iwMedia(vm.queryTwoString);

        vm.queryThreeString = 'medium';
        vm.queryThree = $iwMedia(vm.queryThreeString);

        $scope.$watch(function() { return $iwMedia(vm.queryOneString); }, function() {
            vm.queryOne = $iwMedia(vm.queryOneString);
        });
        
        $scope.$watch(function() { return $iwMedia(vm.queryTwoString); }, function() {
            vm.queryTwo = $iwMedia(vm.queryTwoString);
        });

        $scope.$watch(function() { return $iwMedia(vm.queryThreeString); }, function() {
            vm.queryThree = $iwMedia(vm.queryThreeString);
        });


        $timeout(function() {
            $log.log(' ');
            $iwMedia.dump();
        }, 2000);
    }

})();
