(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .directive('lotteryTicket', lotteryTicket);

    /** @ngInject */
    function lotteryTicket() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/playground/lottery-ticket/lottery-ticket.html',
            scope: {
                data: '='
            },
            link: LotteryTicketItemLink,
            replace: true,
            controller: LotteryTicketItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function LotteryTicketItemLink(scope, iElement, iAttrs, controller, transclude) {

            // 
        }

        /** @ngInject */
        function LotteryTicketItemController($log, IWConstants) {
            var vm = this;

            // Constants for HTLM access
            vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
            vm.PLAY_OPTIONS = IWConstants.PLAY_OPTIONS;
            vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

            vm.addLine = function() {
                var line = {
                    primaryNumbers: [0,1,2, "52", "40"],
                    secondaryNumbers: [1,2, "", 3]
                };
                vm.data.addLine(line);
            }

            vm.toggleDays = function() {
                if(vm.data.drawDays.length > 0) {
                    vm.data.setDrawDays([]);
                }
                else {
                    vm.data.setDrawDays([1,2, 7,8]);
                }
            }

            // vm.increasePlayWeeks = increasePlayWeeks;
            // vm.decreasePlayWeeks = decreasePlayWeeks;

            // function decreasePlayWeeks() {
            //     var weeks = vm.cartItem.playingPeriodLength;
            //     if(weeks > 1) {
            //         weeks = weeks -= 1;
            //         vm.cartItem.setPlayPeriod(vm.cartItem.playingPeriod, weeks);
            //     }
            // }

            // function increasePlayWeeks() {
            //     var weeks = vm.cartItem.playingPeriodLength += 1;
            //     vm.cartItem.setPlayPeriod(vm.cartItem.playingPeriod, weeks);
            // }
        }
    }

})();
