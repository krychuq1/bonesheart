(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('CartDemoController', CartDemoController);

    /** @ngInject */
    function CartDemoController($log, $iwCart, $iwBonus,IWConstants) {
        var vm = this;

        // Cart object
        vm.cart = $iwCart.getCart();
        vm.bonus = {};

        // Product types
        vm.productTypes = IWConstants.PRODUCT_TYPES;

        vm.emptyCart = emptyCart;
        vm.commitCart = commitCart;
        vm.toObj = toObj;
        vm.checkOffers = checkOffers;

        function emptyCart() {
        	$iwCart.empty();
        }

        function commitCart() {
            $iwCart.commit();
        }

        function toObj() {
            $log.log( $iwCart.toObj() );
        }

        function checkOffers(){
          function successCB(bonuses){
            console.log(bonuses);
            vm.bonus = bonuses;
            if(bonuses.offersRespose !== null){
              $iwCart.applyBonus(bonuses.offersRespose);
            }

          }

          $iwBonus.checkOffers(vm.cart).then(successCB);
        }
    }

})();
