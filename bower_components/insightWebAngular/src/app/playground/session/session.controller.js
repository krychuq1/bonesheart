(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('SessionDemoController', SessionDemoController);

    /** @ngInject */
    function SessionDemoController($log, $iwSession) {
        var vm = this;

        //console.groupEnd();
        //console.group('Session');
        
        // Set session object
        vm.session = $iwSession.getSession();

        // Changing session options
        vm.set = {
            currency: $iwSession.getCurrency(),
            language: $iwSession.getLanguage()
        };

        // Actions
        vm.newSession = newSession;
        vm.destroySession = destroySession;
        vm.resetSession = resetSession;
        vm.changeSettings = changeSettings;

        function newSession() {
            $log.log('-> New session');

            // Create new seesion
            $iwSession.create().then(
                function success(response) {
                    $log.info('--> Success', response);
                },
                function error(response) {
                    $log.error('--> Error', response);
                }
            );
        }

        function destroySession() {
            $log.log('-> Destroy session');
            $iwSession.destroy();
        }

        function resetSession() {
            $log.log('-> Reset session');
            $iwSession.reset();
        }

        function changeSettings() {
            $iwSession.setCurrency(vm.set.currency);
            $iwSession.setLanguage(vm.set.language);
        }
    }

})();
