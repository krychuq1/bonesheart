(function() {
    'use strict';

    angular
        .module('insightWebAngular')
        .controller('SyndicateDemoController', SyndicateDemoController);

    /** @ngInject */
    function SyndicateDemoController($log, SyndicateProducts, SyndicateTicket, $timeout, $iwCart,$iwBonus) {
        var vm = this;

        // Loading status
        vm.loading = true;

        // Array of syndicates
        vm.syndicates = [];

        // Syndicate ticket object
        vm.ticket = new SyndicateTicket();

        // Selected ticket data
        vm.selectedTicket = 0;

        // Actions
        vm.setTicketData = setTicketData;
        vm.newTicket = newTicket;
        vm.restoreTicket = restoreTicket;
        vm.setShares = setShares;
        vm.addToCart = addToCart;
        vm.checkBonus = checkBonus;

        // Load the products
        SyndicateProducts.load().then(function success(response) {
            vm.syndicates = SyndicateProducts.all();
            vm.loading = false;
        },
        function error(error) {
            $log.log('Error loading syndicates');
            $log.error(error);
        });

        function setTicketData() {
            vm.ticket.setData(vm.syndicates[vm.selectedTicket]);
        }
        function checkBonus(){
          console.log(vm.syndicates[vm.selectedTicket],vm.ticket);
          var promo = $iwBonus.checkApplicableBonuses(vm.ticket);
          vm.ticket.promo = promo.bonus || {};
          console.log(promo);
        }

        function newTicket() {
            vm.ticket = new SyndicateTicket();
        }

        function restoreTicket() {
            // Ticket json
            var json = '{\r\n  \"name\": \"Syndicate\",\r\n  \"productId\": 1,\r\n  \"drawTypeId\": 0,\r\n  \"syndicateId\": 10008,\r\n  \"shares\": {\r\n    \"total\": 100,\r\n    \"remaining\": 100,\r\n    \"claimed\": 20\r\n  },\r\n  \"sharePrice\": 300,\r\n  \"playingPeriod\": \"subscription\",\r\n  \"playingPeriodLength\": 1,\r\n  \"billingPeriod\": \"yearly\",\r\n  \"productType\": \"syndicate\",\r\n  \"subTotal\": 0,\r\n  \"currency\": \"SEK\"\r\n}';

            // Create a new ticket item
            vm.ticket = new SyndicateTicket();

            // Restore the ticket
            vm.ticket.restoreFromJSON(json);
        }

        function setShares() {
            vm.ticket.setShares(120);
        }

        function addToCart() {
            $log.log('Adding to cart');
            $iwCart.addItem(vm.ticket);
        }
    }

})();
