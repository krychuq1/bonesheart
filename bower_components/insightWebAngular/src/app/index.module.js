(function() {
    'use strict';

    angular
        .module('insightWebAngular', [
            'ngCookies',
            'ngSanitize',
            'ngResource',
            'ui.router'
        ]);

})();
