/*
|--------------------------------------------------------------------------
| Breakpoints utility
| -------------------------------------------------------------------------
| 
| Provides very useful functions and mixins for doing breakpoints. For
| uage refer to the documentation in confluence.
|
| Dependencies: 'utilities/maps', 'utilities/units'
|
| NOTE: Lifted from Zurb foundation because it's very good and no point
|       in reinventing a very good wheel!
|--------------------------------------------------------------------------
*/


/**
 * Default breakpoints (all pixels converted to em)
 * 
 * @type {Map}
 */
$breakpoints : (
    xsmall  : 0,
    small   : 480px,
    medium  : 768px,
    large   : 1280px,
    xlarge  : 1440px
) !default;

/**
 * Current breakpoint variable
 * 
 * @type {string}
 */
$_current-breakpoint: false !global;


/**
 * Build a breakpoint string based on input
 * 
 * @param  {string|number} $value: 0    Breakpoint name and direction (string) or breakpoint value
 * @return {stinrg}                     Final breakpoint sting
 */
@function breakpoint($value: 0) {
    // Size or keyword
    $bp: nth($value, 1);

    // Value for max-width media queries
    $bp-max: 0;

    // Direction of media query (up, down, or only)
    $dir: if(length($value) > 1, nth($value, 2), up);

    // Eventual output
    $str: '';

    // Is it a named media query?
    $named: false;

    // Try to pull a named breakpoint out of the $breakpoints map
    @if type-of($bp) == 'string' {
        @if map-has-key($breakpoints, $bp) {
            @if $dir == 'only' or $dir == 'down' {
                $bp-max: map-get-next($breakpoints, $bp);
            }
            $bp: map-get($breakpoints, $bp);
            $named: true;
        }
        @else {
            $bp: 0;
            @warn 'breakpoint(): "#{$value}" is not defined in your $breakpoints setting.';
        }
    }

    // Convert any pixel, rem, or unitless value to em
    $bp: em-calc($bp);
    //@warn "Current bp is: '#{$bp}' from '#{$value}'. Direction: '#{$dir}'. Calculate max? #{$bp-max}";
    @if $bp-max and $bp-max != 0 {
        $bp-max: em-calc($bp-max) - (1/16);
    }

    // Conditions to skip media query creation
    // - It's a named breakpoint that resolved to "0 down" or "0 up"
    // - It's a numeric breakpoint that resolved to "0 " + anything
    @if $bp > 0em or $dir == 'only' or $dir == 'down' {
        // `only` ranges use the format `(min-width: n) and (max-width: n)`
        @if $dir == 'only' {
            // Only named media queries can have an "only" range
            @if $named == true {
                // Only use "min-width" if the floor is greater than 0
                @if $bp > 0em {
                    $str: $str + '(min-width: #{$bp})';

                    // Only add "and" to the media query if there's a ceiling
                    @if $bp-max != null {
                        $str: $str + ' and ';
                    }
                }

                // Only use "max-width" if there's a ceiling
                @if $bp-max != null {
                  $str: $str + '(max-width: #{$bp-max})';
                }
            }
            @else {
                @warn 'breakpoint(): Only named media queries can have an `only` range.';
            }
        }

        // `down` ranges use the format `(max-width: n)`
        @else if $dir == 'down' {
            $max: if($named, $bp-max, $bp);

            // Skip media query creation if input value is exactly "0 down",
            // unless the function was called as "small down", in which case it's just "small only"
            @if $named or $bp > 0em {
                @if $max != null {
                    $str: $str + '(max-width: #{$max})';
                }
            }
        }

        // `up` ranges use the format `(min-width: n)`
        @else if $bp > 0em {
            $str: $str + '(min-width: #{$bp})';
        }
    }

    @return $str;
}


/**
 * Breakpoint mixin
 * 
 * @param  {List} $value Breakpoint argument
 */
@mixin breakpoint($value) {
    $str: breakpoint($value);

    // Set current braekpoint name
    $_current-breakpoint: $value !global;

    // If $str is still an empty string, no media query is needed
    @if $str == '' {
        @content;
    }

    // Otherwise, wrap the content in a media query
    @else {
        
        // If already inside a breakpoint then make sure this new breakpoint is at the root of the document
        @if in-breakpoint() and & {
            @at-root (without:all) & {
                @media screen and #{$str} {
                    @content;
                }
            }
        }
        @else {
            @media screen and #{$str} {
                @content;
            }
        }
    }

    // Reset current breakpoint to false
    $_current-breakpoint: false !global;
}


/**
 * Return the current breakpoint name / value
 * 
 * @return {String|Number}      Breakpoint name
 */
@function current-breakpoint($name: false) {

    @if length($_current-breakpoint) > 1 and $name == true {
        @return nth($_current-breakpoint, 1);
    }
    @else {
        @return $_current-breakpoint;
    }
}


/**
 * Check if in a breakpoint or not
 * 
 * @return {Bool} True if inside a breakpoint, false if not
 */
@function in-breakpoint() {
    @return not(not($_current-breakpoint));
}


/**
 * Get the breakpoint direction
 * 
 * @return {String} Direction of breakpoint (up, down or only)
 */
@function breakpoint-direction() {
    @if in-breakpoint() {
        @if length($_current-breakpoint) > 1 {
            @return nth($_current-breakpoint, 2);
        }
        @else {
            @return "up";
        }
    }
    @else {
        @return false;
    }
}

/**
 * Conver a breakpoint map to a serialized string
 * 
 * @param  {Map} $map       Map to convert
 * @return {String}         Serialized map
 */
@function breakpoint-serialize($map) {
    $str: '';
    @each $key, $value in $map {
        $str: $str + $key + '=' + em-calc($value) + '&';
    }
    $str: str-slice($str, 1, -2);
    @return $str;
}



@function breakpoint-query-list($breakpoints) {
    // Final query
    $queryList: ();

    // Possibly query directions
    $directions: ('up', 'only', 'down');

    // Loop through each direction and generate the queries
    @each $direction in $directions {

        @each $key, $value in $breakpoints {
            // Get the brekapoint query
            $query: breakpoint(($key, $direction));

            // If direction is up add the query again but without the direction
            @if $direction == up {
                $queryList: map-set($queryList, $key,  if($query == '' , '(min-width: 0)', $query));
            }

            // Add to final list
            $queryList: map-set($queryList, #{$key + ' ' + $direction},  if($query == '' , '(min-width: 0)', $query));
        }
    }

    @return map-serialize($queryList);
}

@mixin insight-mq-html() {
    .insight-mq-list {
        font-family: '#{breakpoint-query-list($breakpoints)}';
    }

    /**
     * Make breakpoints accessible from the JS
     */
    .insight-mq {
        font-family: '#{breakpoint-serialize($breakpoints)}';
    }
} 