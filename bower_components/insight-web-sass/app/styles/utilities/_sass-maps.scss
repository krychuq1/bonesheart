/*
|--------------------------------------------------------------------------
| Sass Maps utility
| -------------------------------------------------------------------------
| 
| A collection of functions to extend the SASS maps functionality. 
|
| Dependencies: none
|
|--------------------------------------------------------------------------
*/


/**
 * Get the next item in a map (value or key)
 * 
 * @param  {array}          $map        Map of values
 * @param  {string}         $key        Starting key
 * @param  {string}         $return     Return key or value
 * @return {string|number}              Value or key
 */
@function map-get-next($map, $key, $return: value) {
    // Map values
    $values: map-values($map);

    // Flags and index for current item in map
    $i: 1;
    $found: false;

    // Loop through each item and look for current key
    @each $val in map-keys($map) {
        @if $found == false {
            @if ($key == $val) {
                $found: true;
            }
            $i: $i + 1;
        }
    }
    
    // Return value if we have one
    @if $i <= length($map) {
        @return if($return == value, nth($values, $i), nth(map-keys($map), $i));
    }
    @else {
        @return null;
    }
}


/**
 * Get the previous item in a map (value or key)
 * 
 * @param  {array}          $map        Map of values
 * @param  {string}         $key        Starting key
 * @param  {string}         $return     Return key or value
 * @return {string|number}              Value or key
 */
@function map-get-previous($map, $key, $return: value) {
    // Map values and keys
    $values: map-values($map);
    $keys: map-keys($map);

    // Flags and index for previous item in map
    $i: length($map);
    $found: false;

    // Loop through each item in reverse and look for current key
    @for $j from length($map) * 1 through 1 {
        @if $found == false {
            @if $key == nth($keys, $j) {
                $found: true;
            }
            $i: $i - 1;
        }
    }

    // Return value if we have one
    @if $i != 0 {
        @return if($return == value, nth($values, $i), nth($keys, $i));
    }
    @else {
        @return null;
    }
}


/**
 * Fetch nested keys from a map
 * 
 * @param  {map} $map           Map
 * @param  {Arglist} $keys...   Keys to fetch
 * @return {*}                  Fetched item
 */
@function map-deep-get($map, $keys...) {
    @each $key in $keys {
        @if type-of($map) == 'map' {
            $map: map-get($map, $key);
        }
        @else {
            $map: null;
        }
    }
    @return $map;
}


/**
 * Set an item in a map
 * 
 * @param {Map} $map        Map to set item in 
 * @param {String} $key     Map item key
 * @param {*} $value        Map value
 */
@function map-set($map, $key, $value) {
    $new: ($key: $value);
    @return map-merge($map, $new);
}



/**
 * Remove items in map above a given key
 * 
 * @param  {Map} $map       Target map
 * @param  {String} $key    Reference key for removing items above
 * @param  {Bool} $include  Include current key in final list
 * @return {Map}            Map with removed items
 */
@function map-remove-above($map, $key, $include: true) {
    $new: ();
    $complete: false;

    @each $_key, $value in $map {
        @if $complete == false {
            @if $_key == $key {
                $complete: true;
            }
            @else {
                $new: map-set($new, $_key, $value);
            }
        }
    }

    // Add the original value on
    @if $include {
        $new: map-set($new, $key, map-get($map, $key));
    }
    
    // Return the new map
    @return $new;
}


/**
 * Remove items in map below a given key
 * 
 * @param  {Map} $map       Target map
 * @param  {String} $key    Reference key for removing items below
 * @param  {Bool} $include  Include current key in final list
 * @return {Map}            Map with removed items
 */
@function map-remove-below($map, $key, $include: true) {
    // New map of values
    $new: ();

    // Map values and keys
    $values: map-values($map);
    $keys: map-keys($map);

    // Flags and index for previous item in map
    $i: length($map);
    $found: false;

    // Loop through each item in reverse and look for current key
    @for $j from length($map) * 1 through 1 {
        @if $found == false {
            @if $key == nth($keys, $j) {
                $found: true;
            }
            @else {
                $new: map-set($new, nth($keys, $j), nth($values, $j));
            }
        }
    }

    // Add the original value on
    @if $include {
        $new: map-set($new, $key, map-get($map, $key));
    }

    // Return the new map reversed (so in the correct order)
    @return map-reverse($new);
}


/**
 * Reverse the order of a map
 * 
 * @param  {Map} $map   Map to reverse
 * @return {Map}        Reversed map
 */
@function map-reverse($map) {
    
    $keys: map-keys($map);
    $map-reversed: ();
    
    // Check the map isn't empty
    @if length($map) > 0 {

        // Loop through each item and add to the new map 
        @for $i from length($keys) through 1 {
            $map-reversed: map-merge(
                $map-reversed,
                (nth($keys, $i): map-get($map, nth($keys, $i)))
            );
        }
    }

    // Return reversed map
    @return $map-reversed;
}

@function map-serialize($map) {
    $str: '';
    @each $key, $value in $map {
        $str: $str + $key + '=' + $value + '&';
    }
    $str: str-slice($str, 1, -2);
    @return $str;
}



/// jQuery-style extend function
/// About `map-merge()`:
/// * only takes 2 arguments
/// * is not recursive
/// @param {Map} $map - first map
/// @param {ArgList} $maps - other maps
/// @param {Bool} $deep - recursive mode
/// @return {Map}
@function map-extend($map, $maps.../*, $deep */) {
  $last: nth($maps, -1);
  $deep: $last == true;
  $max: if($deep, length($maps) - 1, length($maps));

  // Loop through all maps in $maps...
  @for $i from 1 through $max {
    // Store current map
    $current: nth($maps, $i);

    // If not in deep mode, simply merge current map with map
    @if not $deep {
      $map: map-merge($map, $current);
    } @else {
      // If in deep mode, loop through all tuples in current map
      @each $key, $value in $current {

        // If value is a nested map and same key from map is a nested map as well
        @if type-of($value) == "map" and type-of(map-get($map, $key)) == "map" {
          // Recursive extend
          $value: map-extend(map-get($map, $key), $value, true);
        }

        // Merge current tuple with map
        $map: map-merge($map, ($key: $value));
      }
    }
  }

  @return $map;
}