/* global moment:false, WirecardHPP:false */
(function() {
    'use strict';

    // This should be called window.IWConfig
    window.IWConfig = {

        // // App settings
        APP: {
            domain              : '7lottos.com',
            brand               : '7lottos',
            frontEnd            : '7lottos',
            frontEndId          : 1,
            defaultLanguage     : 'en',
            defaultCurrency     : 'EUR'
        },

        // App settings
        // APP: {
        //     domain              : 'megalotto.com',
        //     brand               : 'megalotto',
        //     frontEnd            : 'megalotto',
        //     frontEndId          : 5,
        //     defaultLanguage     : 'en',
        //     defaultCurrency     : 'EUR'
        // },

        // Use production APIs?
        PRODUCTION: true,

        // API endpoint config
        API_ROOTS: {
            STAGING: {
                //SERVICE: 'http://win-dev-api.7lottos.winnersgroup.com/',        //'http://ec2-52-212-69-103.eu-west-1.compute.amazonaws.com:8080/',
                //CASHIER: 'http://win-dev-cashier.7lottos.winnersgroup.com/',    //'http://ec2-52-211-16-112.eu-west-1.compute.amazonaws.com:8080/'
            },
            PRODUCTION: {
                SERVICE: 'https://api.dr.insight.cloud/',
                CASHIER: 'https://cashier.dr.insight.cloud/'
            }
        }
    };

    window.baseUrl = './';

})();