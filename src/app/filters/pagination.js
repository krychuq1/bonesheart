(function() {

  angular
    .module('sevenLottos')
    .filter('pagination', Pagination);

  function Pagination() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    };
  }
})();
