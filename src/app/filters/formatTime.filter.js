(function() {
  'use strict';

  angular
    .module('insightWebAngular')
    .filter('formatTime', formatTimeFilter);

  function formatTimeFilter() {
    return function(lottery) {
      var date = new Date(lottery.nextDrawDateTimeList[lottery.nextDrawDateTimeList.length - 1]);
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
      var dd = date.getDate().toString();
      return yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
    };
  }
})();
