(function() {

  angular
    .module('sevenLottos')
    .filter('formatDate', formatDateFromDate);

  function formatDateFromDate() {
    return function(input) {
      if (input) {
        var date = new Date(input);
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = date.getDate().toString();
        var hours = date.getHours().toString();
        if (hours < 10) {
          hours = 0 + hours;
        }
        var min = date.getMinutes().toString();
        if (min < 10) {
          min = 0 + min;
        }
        var sec = date.getSeconds().toString();
        if (sec < 10) {
          sec = 0 + sec;
        }
        return (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy + ' , ' + hours + ':' + min + ':' + sec; // padding
      } else {
        return ' - ';
      }
    };
  }
})();
