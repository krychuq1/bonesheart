(function() {
    'use strict';

    angular
        .module('sevenLottos', [
            'ngAnimate',
            'ngCookies',
            'ngSanitize',
            'ngMessages',
            'ngAria',
            'ngResource',
            'ui.router',
            'ngMaterial',
            'angular-svg-round-progressbar',
            'slickCarousel',
            'ngDialog',
            'insightWebAngular',
            'iso.directives',
            'md.data.table',
            'bt.owl-carousel',
            'tmh.dynamicLocale'
        ]);

})();
