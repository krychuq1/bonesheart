(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .config(config);

    /** @ngInject */
    function config($logProvider, ngDialogProvider, $mdThemingProvider, tmhDynamicLocaleProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // ngDialog settings
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-7lotto',
            setOpenOnePerName: true
        });
        ngDialogProvider.setOpenOnePerName(true);

        //tmhDynamicLocaleProvider.localeLocationPattern('/base/node_modules/angular-i18n/angular-locale_{{locale}}.js');
        tmhDynamicLocaleProvider.localeLocationPattern('http://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.8/angular-locale_{{locale}}.js');
        // https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.8/angular-locale_cy.min.js

        // Configure the theme
        $mdThemingProvider.theme('default')
            .primaryPalette('blue', {
                'default': '500'
            })
            .accentPalette('deep-orange', {
                'default': '500'
            })
            .warnPalette('red');
    }

})();
