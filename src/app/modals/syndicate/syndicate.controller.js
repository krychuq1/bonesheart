(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('SyndicateModalController', SyndicateModalController);

    /** @ngInject */
    function SyndicateModalController($scope, $timeout, $log, IWConstants, SyndicateTicket, $iwCart, $iwProducts) {
        var vm = this;

        // Product
        vm.product = $scope.ngDialogData.product;

        // New syndicate ticket
        vm.cartItem = new SyndicateTicket(vm.product);
        vm.cartItem.setShares(1);

        // Constants for HTLM access
        vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
        vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

        // Syndicate lotteries data
        vm.syndicateLotteries = {};

        // Number showing
        vm.showingNumbers = false;
        vm.showNumbers = showNumbers;
        vm.decreaseShares = decreaseShares;

        // Add to cart
        vm.addToCart = addToCart;

        matchTicketData();

        /**
         * Match the ticket list data to lottery names
         * @return {void}
         */
        function matchTicketData() {

            // Loop throguh each lottery without a name
            angular.forEach(vm.product.tickets.list, function(lottery) {

                // Loop through each ticket for that lottery
                angular.forEach(lottery, function(ticket) {

                    // Set name for each lottery
                    if(angular.isUndefined(vm.syndicateLotteries[ticket.drawTypeId])) {
                        vm.syndicateLotteries[ticket.drawTypeId] = {
                            name: getProductName(ticket.drawTypeId),
                            drawTypeId: ticket.drawTypeId,
                            tickets: []
                        }
                    }

                    // Add the ticket
                    var tmpTicket = {
                        drawDay: ticket.drawDay,
                        primaryNumbers: [],
                        secondaryNumbers: []
                    };

                    // Convert numbers
                    angular.forEach(ticket.primaryNumbers, function(number) {
                        if(number) {
                            tmpTicket.primaryNumbers.push(+number);
                        }
                    });
                    angular.forEach(ticket.secondaryNumbers, function(number) {
                        if(number) {
                            tmpTicket.secondaryNumbers.push(+number);
                        }
                    });

                    // Add to final array
                    vm.syndicateLotteries[ticket.drawTypeId].tickets.push(tmpTicket);
                });

            });
        }

        function getProductName(drawTypeId) {
            // Get product id
            var productId = $iwProducts.toProductId(drawTypeId);

            // Get product
            var product = $iwProducts.SingleLines.get(productId);

            // Return the name
            return angular.isDefined(product) ? product.name : '';
        }

        function addToCart() {
            if(vm.cartItem.subTotal > 0) {
                $iwCart.addItem(vm.cartItem);
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
            }
        }

        function showNumbers() {
            vm.showingNumbers = true;

            $log.log(vm.product.tickets.list);
        }

        function decreaseShares() {
            if(vm.cartItem.shares.claimed > 1) {
                vm.cartItem.decreaseShares();
            }
        }
    }

})();
