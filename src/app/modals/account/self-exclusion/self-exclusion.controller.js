(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('SelfExclusionModalController', SelfExclusionModalController);

    /** @ngInject */
    function SelfExclusionModalController($scope, $timeout, $log) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        vm.cooloffs = [
            { name : "12 hrs", timePeriod : 12 },
            { name : "1 day", timePeriod : 1 },
            { name : "7 days", timePeriod : 7 },
            { name : "30 days", timePeriod : 30 },
            { name : "60 days", timePeriod : 60 },
            { name : "180 days", timePeriod : 180 }
        ];

        /**
         * Fields in the modal
         */
        vm.fields = {
            cooloffperiod : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.selfExclusionForm.$valid)
            {
                $log.debug('selfExclusion form is valid')
                // Carry out the saving of the data
                
                

                // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
                vm.processing = false;
            }
            else
            {
                $log.debug('selfExclusion form is not valid')
                vm.processing = false;
            }
        }

    }

})();
