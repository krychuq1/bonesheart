(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('LossLimitsModalController', LossLimitsModalController);

    /** @ngInject */
    function LossLimitsModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        /**
         * Fields in the modal
         */
        vm.fields = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        };

        /**
         * The current values for a users limits
         */
        vm.defaults = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            semiannual: 0
        }

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getLossLimits()
                .then(getLimitsSuccess, getLimitsFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getLimitsSuccess(response) {
                vm.defaults = {
                    daily: response.daily.limit,
                    weekly: response.weekly.limit,
                    monthly: response.monthly.limit,
                    semiannual: response.semiannual.limit
                }
            }

            function getLimitsFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.lossLimitForm.$valid)
            {
                $iwAccount.setLossLimits(vm.fields).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('LossLimitForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('lossLimitForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();
