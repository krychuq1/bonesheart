(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('UpdateEmailModalController', UpdateEmailModalController);

    /** @ngInject */
    function UpdateEmailModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            email : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial email address
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getProfile()
                .then(getEmailSuccess, getEmailFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getEmailSuccess(response) {
                vm.fields = {
                    email: response.data.eMail
                }
            }

            function getEmailFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updateEmailForm.$valid)
            {
                // Carry out the saving of the data
                $iwAccount.updateEmail(vm.fields.email).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateEmail Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdateEmail form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();
