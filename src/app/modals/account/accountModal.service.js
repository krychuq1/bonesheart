(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .service('AccountModalService', AccountModalService);

    /** @ngInject */
    function AccountModalService($q, $log, $mdDialog, ngDialog) {
        // var vm = this;
        
        /**
         * Modal service
         * 
         * @type {Object}
         */
        var service = {
            deposit             : deposit,
            depositLimits       : depositLimits,
            lossLimits          : lossLimits,
            purchaseHistory     : purchaseHistory,
            selfExclusion       : selfExclusion,
            transactionHistory  : transactionHistory,
            updateAddress       : updateAddress,
            updateEmail         : updateEmail,
            updatePassword      : updatePassword,
            wagerLimits         : wagerLimits,
            withdraw            : withdraw
        };
        return service;

        /**
         * Show deposit
         */
        function deposit(/*ev*/) {
            var deferred = $q.defer();

            var depositDialog = ngDialog.open({
                name: 'depositModal',
                template: 'app/modals/account/deposit/deposit.html',
                appendClassName: 'deposit-modal',
                controller: 'DepositModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            depositDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show deposit limits
         */
        function depositLimits(/*ev*/) {
            var deferred = $q.defer();

            var depositLimitsDialog = ngDialog.open({
                name: 'depositLimitModal',
                template: 'app/modals/account/deposit-limits/deposit-limits.html',
                appendClassName: 'deposit-limits-modal',
                controller: 'DepositLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            depositLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show loss limits
         */
        function lossLimits(/*ev*/) {
            var deferred = $q.defer();

            var lossLimitsDialog = ngDialog.open({
                name: 'lossLimitModal',
                template: 'app/modals/account/loss-limits/loss-limits.html',
                appendClassName: 'loss-limits-modal',
                controller: 'LossLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            lossLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show purchase history
         */
        function purchaseHistory(/*ev*/) {
            var deferred = $q.defer();

            var purchaseHistoryDialog = ngDialog.open({
                name: 'lossLimitModal',
                template: 'app/modals/account/purchase-history/purchase-history.html',
                appendClassName: 'purchase-history-modal',
                controller: 'PurchaseHistoryModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            purchaseHistoryDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Self Exclusion Modal
         */
        function selfExclusion(/*ev*/) {
            var deferred = $q.defer();

            var selfExclusionDialog = ngDialog.open({
                name: 'selfExclusionModal',
                template: 'app/modals/account/self-exclusion/self-exclusion.html',
                appendClassName: 'self-exclusion-modal',
                controller: 'SelfExclusionModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            selfExclusionDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Transaction History Modal
         */
        function transactionHistory(/*ev*/) {
            var deferred = $q.defer();

            var transactionHistoryDialog = ngDialog.open({
                name: 'transactionHistoryModal',
                template: 'app/modals/account/transaction-history/transaction-history.html',
                appendClassName: 'transaction-history-modal',
                controller: 'TransactionHistoryModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            transactionHistoryDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Address Modal
         */
        function updateAddress(/*ev*/) {
            var deferred = $q.defer();

            var updateAddressDialog = ngDialog.open({
                name: 'updateAddressModal',
                template: 'app/modals/account/update-address/update-address.html',
                appendClassName: 'update-address-modal',
                controller: 'UpdateAddressModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updateAddressDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Email Modal
         */
        function updateEmail(/*ev*/) {
            var deferred = $q.defer();

            var updateEmailDialog = ngDialog.open({
                name: 'updateEmailModal',
                template: 'app/modals/account/update-email/update-email.html',
                appendClassName: 'update-email-modal',
                controller: 'UpdateEmailModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updateEmailDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Update Password Modal
         */
        function updatePassword(/*ev*/) {
            var deferred = $q.defer();

            var updatePasswordDialog = ngDialog.open({
                name: 'updatePasswordModal',
                template: 'app/modals/account/update-password/update-password.html',
                appendClassName: 'update-password-modal',
                controller: 'UpdatePasswordModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            updatePasswordDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Wager Limits Modal
         */
        function wagerLimits(/*ev*/) {
            var deferred = $q.defer();

            var wagerLimitsDialog = ngDialog.open({
                name: 'wagerLimitsModal',
                template: 'app/modals/account/wager-limits/wager-limits.html',
                appendClassName: 'wager-limits-modal',
                controller: 'WagerLimitsModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            wagerLimitsDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show Withdraw Modal
         */
        function withdraw(/*ev*/) {
            var deferred = $q.defer();

            var withdrawDialog = ngDialog.open({
                name: 'withdrawModal',
                template: 'app/modals/account/withdraw/withdraw.html',
                appendClassName: 'withdraw-modal',
                controller: 'WithdrawModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            withdrawDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

    }

})();