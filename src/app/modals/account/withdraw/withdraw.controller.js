(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('WithdrawModalController', WithdrawModalController);

    /** @ngInject */
    function WithdrawModalController($scope, $timeout, $log) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            amount : '',
            instrument1 : '', //BIC
            instrument : '' //IBAN
        };

        /**
         * Functions in the controller
         */
        vm.requestWithdrawal = requestWithdrawal;

        /**
         * Saves the data from the defined fields above
         */
        function requestWithdrawal()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.withdrawForm.$valid)
            {
                $log.debug('withdraw form is valid')
                // Carry out the saving of the data
                
                // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
                vm.processing = false;
            }
            else
            {
                $log.debug('withdraw form is not valid')
                vm.processing = false;
            }
        }
    }

})();
