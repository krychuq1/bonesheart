(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('TransactionHistoryModalController', TransactionHistoryModalController);

    /** @ngInject */
    function TransactionHistoryModalController($iwAccount, moment) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        vm.transactions = [];

        /**
         * Grab the latest data
         */
        function init()
        {
            vm.processing = false;

            var startDate = moment().subtract(60, 'days').unix();
            var endDate = moment().unix();

            $iwAccount.getTransactionHistory(startDate, endDate)
                .then(transactionsSuccess, transactionsError)
                .finally(function() {
                    vm.processing = false;
                });

            function transactionsSuccess(response) {
                vm.transactions = response.transactionDetailsList;
            }

            function transactionsError(response) {
                
            }
        }

        init();

    }

})();
