(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('UpdatePasswordModalController', UpdatePasswordModalController);

    /** @ngInject */
    function UpdatePasswordModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            oldPassword : '',
            newPassword : '',
            confirmPassword : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updatePasswordForm.$valid)
            {
                $log.debug('UpdatePassword form is valid')
                // Carry out the saving of the data
                
                $iwAccount.updatePassword(vm.fields.oldPassword, vm.fields.newPassword, vm.fields.confirmPassword).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateEmail Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdatePassword form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

    }

})();
