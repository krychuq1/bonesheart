(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('UpdateAddressModalController', UpdateAddressModalController);

    /** @ngInject */
    function UpdateAddressModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

		/**
         * Default processing indicator
         */
        vm.processing = false;

        /**
         * Fields in the modal
         */
        vm.fields = {
            address : '',
            city 	: '',
            zip 	: '',
            phone   : ''
        };

        /**
         * Functions in the controller
         */
        vm.save = save;

        /**
         * Request the initial email address
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = true;

            // Load data
            $iwAccount.getProfile()
                .then(getAddressSuccess, getAddressFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function getAddressSuccess(response) {
                vm.fields = {
                    address : response.data.address,
                    city    : response.data.city,
                    zip     : response.data.zip,
                    phone   : response.data.phone
                }
            }

            function getAddressFailed() {
                //$log.log('Failed', response);
            }
        }

        /**
         * Saves the data from the defined fields above
         */
        function save()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.updateAddressForm.$valid)
            {
                $log.debug('UpdateAddress form is valid')
                // Carry out the saving of the data
                $iwAccount.updateProfile(vm.fields).then(
                    function(){
                        // The query was successful
                        // Close the modal
                        closeModal();
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('UpdateAddress Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('UpdateAddress form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();
