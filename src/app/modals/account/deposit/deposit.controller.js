(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('DepositModalController', DepositModalController);

    /** @ngInject */
    function DepositModalController($scope, $timeout, $log, $iwPayments, $iwAccount, $window) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;


        vm.account = $iwAccount.getAccount();


        /**
         * Fields in the modal
         */
        vm.fields = {
            amount          : "",
            card_name       : "",
            card_number     : "",
            exp_month       : "",
            exp_year        : "",
            cvv             : ""
        };

        vm.months = ["01","02","03","04","05","06","07","08","09","10","11","12"];

        /**
         * Functions in the controller
         */
        vm.deposit = deposit;

        /**
         * Request the initial amount of the limits
         */
        function init()
        {
            // Get the limits from the server
            vm.processing = false;

            // Load Instruments goes here

        }

        /**
         * Saves the data from the defined fields above
         */
        function deposit()
        {
            vm.processing = true;

            // Check if the form is valid
            if($scope.depositForm.$valid)
            {
                var request = {
                    amount: vm.fields.amount,
                    cardNumber: vm.fields.card_number,
                    expirationMonth: vm.fields.exp_month,
                    expirationYear: vm.fields.exp_year,
                    cvv: vm.fields.cvv,
                    firstName: vm.account.profile.firstName,
                    lastName: vm.account.profile.lastName,
                    redirectionURL: $window.location.href
                };

               $iwPayments.deposit('wirecard', request).then(
                    function(response){
                        // The query was successful
                        setFeedback(false, true, false, response.data.errorDesc);
                        // Close the modal
                        $timeout(function() {
                            closeModal();
                        }, 1000);
                    },
                    function(response){
                        // Error with setting limits
                        $log.debug('DepositForm: Response from server', response);
                        // Show feedback to the end user
                        setFeedback(false, false, true, response.data.errorDesc);
                    }
                ).finally(function() {
                    vm.processing = false;
                });
            }
            else
            {
                $log.debug('depositForm form is not valid')
                vm.processing = false;
            }
        }

        /**
         * Set feedback for the end user
         * 
         * @param {boolean} processing
         * @param {boolean} success
         * @param {boolean} error
         * @param {string} message
         */
        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }

        /**
         * Close the modal
         */
        function closeModal()
        {
            // Close the dialog - see: https://github.com/likeastore/ngDialog/issues/145
            /*eslint-disable */
            $scope.closeThisDialog();
            /*eslint-enable */
        }

        init();

    }

})();
