(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('PurchaseHistoryModalController', PurchaseHistoryModalController);

    /** @ngInject */
    function PurchaseHistoryModalController($scope, $timeout, $log, $iwAccount, moment) {
        var vm = this;

        /**
         * Default processing indicator
         */
        vm.processing = true;

        vm.purchases = [];

        /**
         * Grab the latest data
         */
        function init() {
            vm.processing = true;

            var startDate = moment().subtract(60, 'days');
            var endDate = moment();

            // Load data
            $iwAccount.getPurchaseHistory(startDate.format(), endDate.format())
                .then(fetchSuccess, fetchFailed)
                .finally(function() {
                    vm.processing = false;
                });

            // Got results
            function fetchSuccess(response) {
                vm.purchases = response.data.response.playSlips;
            }

            function fetchFailed() {
                //$log.log('Failed', response);
            }
        }

        init();

    }

})();
