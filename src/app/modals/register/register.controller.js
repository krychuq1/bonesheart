(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('RegisterModalController', RegisterModalController);

    /** @ngInject */
    function RegisterModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        // Global feedback object
        vm.feedback = {
            processing: false,
            success: false,
            error: false,
            message: ''
        };

        // Registration data information
        vm.registerData = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            phone: '',
            terms: false
        };

        // Accessible functions
        vm.register = register;

        function register() {
            if($scope.registerForm.$valid && vm.registerData.terms == true && !vm.feedback.processing) {
                setFeedback(true);

                // Try to login with credentials
                $iwAccount.register(vm.registerData)
                    .then(registerSuccess, registerFailed);
            }
            else {
                setFeedback(false);
            }

            /** TODO: Change this so it handles failures */
            function registerSuccess() {
                $iwAccount.login(vm.registerData.email, vm.registerData.password).then(function() {
                    /*eslint-disable */
                    $scope.closeThisDialog(true);
                    /*eslint-enable */
                });
            }

            function registerFailed(response) {
                $log.log('Register failed', response);
                $timeout(function() {
                    setFeedback(false, false, true, response.errorDesc);
                }, 1000);
            }
        }

        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }
    }

})();
