    (function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('LoginModalController', LoginModalController);

    /** @ngInject */
    function LoginModalController($scope, $timeout, $log, $iwAccount) {
        var vm = this;

        // Global feedback object
        vm.feedback = {
            processing: false,
            success: false,
            error: false,
            message: ''
        };

        // Login information
        vm.login = {
            username: '',
            password: ''
        };

        // Reset password status
        vm.reset = {
            email: '',
            visible: false
        };

        // Accessible functions
        vm.login = login;
        vm.forgotPassword = forgotPassword;
        vm.resetPassword = resetPassword;

        function login() {
            if($scope.loginForm.$valid) {
                setFeedback(true);

                // Try to login with credentials
                $iwAccount.login(vm.login.username, vm.login.password)
                    .then(loginSuccess, loginFailed);
            }

            function loginSuccess() {
                $timeout(function() {
                    //$scope.closeThisDialog(true);
                    /*eslint-disable */
                    $scope.closeThisDialog(true);
                    /*eslint-enable */
                }, 1000);
            }

            function loginFailed(response) {
                $timeout(function() {
                    setFeedback(false, false, true, response.data.errorDesc);
                }, 1000);
            }
        }

        function forgotPassword(show) {
            if(show) {
                vm.reset.visible = true
            }
            else {
                vm.reset.visible = false;
            }
        }

        function resetPassword() {
            if($scope.forgotPassword.$valid) {
                setFeedback(true);

                $iwAccount.resetPassword(vm.reset.email)
                    .then(resetSuccess, resetFailed);
            }

            function resetSuccess(response) {
                $log.log('Reset worke', response);
                $timeout(function() {
                    setFeedback(false, true);
                    forgotPassword(false);
                }, 1000);
            }

            function resetFailed(response) {
                setFeedback(false, false, true, response.data.errorDesc || response.data.errorLiteral);
            }
        }


        function setFeedback(processing, success, error, message) {
            vm.feedback = {
                processing: processing || false,
                success: success || false,
                error: error || false,
                message: message || ''
            };
        }
    }

})();
