(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('SingleLineModalController', SingleLineModalController);

    /** @ngInject */
    function SingleLineModalController($scope, $timeout, $log, IWConstants, SingleLineTicket, $iwCart, SingleLineProducts, $iwMedia) {
        var vm = this;

        // Product
        vm.product = $scope.ngDialogData.product;

        // Single Line Ticket
        vm.cartItem = new SingleLineTicket(vm.product);

        // Constants for HTLM access
        vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
        vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

        // Primary numbers
        vm.primaryNumbers = {
            chosen      : [],
            list        : vm.product.numbers.primary.list,
            min         : vm.product.numbers.primary.min,
            max         : vm.product.numbers.primary.max,
            total       : vm.product.numbers.primary.total,
            progress    : 0
        };

        // Secondary numbers
        vm.secondaryNumbers = {
            chosen      : [],
            list        : vm.product.numbers.secondary.list,
            min         : vm.product.numbers.secondary.min,
            max         : vm.product.numbers.secondary.max,
            total       : vm.product.numbers.secondary.total,
            progress    : 0
        };

        // Is mobile flag
        vm.isMobile = !$iwMedia('large');
        $scope.$watch(function() { return $iwMedia('large'); }, function(big) {
            vm.isMobile = !big;
        });

        // Choosing numbers mobile
        vm.choosingNumbers = true;

        // Accessible functions
        vm.choosePrimary = choosePrimary;
        vm.chooseSecondary = chooseSecondary;
        vm.addToCart = addToCart;
        vm.luckyDip = luckyDip;
        vm.chooseNumbers = chooseNumbers;

        function choosePrimary(number) {
            if(vm.primaryNumbers.chosen.indexOf(number) > -1) {
                vm.primaryNumbers.chosen.splice(vm.primaryNumbers.chosen.indexOf(number), 1);
            }
            else {
                if(!(vm.primaryNumbers.chosen.length >= vm.primaryNumbers.min && vm.primaryNumbers.chosen.length <= vm.primaryNumbers.max)) {
                    vm.primaryNumbers.chosen.push(number);
                }
            }

            // Try to add line
            addLine();
        }

        function chooseSecondary(number) {
            if(vm.secondaryNumbers.chosen.indexOf(number) > -1) {
                vm.secondaryNumbers.chosen.splice(vm.secondaryNumbers.chosen.indexOf(number), 1);
            }
            else {
                // if(vm.secondaryNumbers.chosen.length === vm.secondaryNumbers.total) {

                // }
                // else {
                //     vm.secondaryNumbers.chosen.push(number);
                // }
                vm.secondaryNumbers.chosen.push(number);
            }

            // Try to add line
            addLine();
        }

        function addLine() {
            if(vm.primaryNumbers.chosen.length === vm.primaryNumbers.min && vm.secondaryNumbers.chosen.length === vm.secondaryNumbers.min) {
                var line = {
                    primaryNumbers: vm.primaryNumbers.chosen,
                    secondaryNumbers: vm.secondaryNumbers.chosen
                };
                vm.cartItem.addLine(line);
                vm.primaryNumbers.chosen = [];
                vm.secondaryNumbers.chosen = [];
            }
        }

        function luckyDip () {
            SingleLineProducts.generateLine(vm.product.drawTypeId, vm.product.drawDates.next)
                .then(luckyDipSuccess, luckyDipError);

            function luckyDipSuccess(response) {
                vm.cartItem.addLine(response);
                if(vm.isMobile) {
                    vm.choosingNumbers = false;
                }
            }

            function luckyDipError() {}
        }

        function chooseNumbers() {
            vm.choosingNumbers = true;
        }

        function addToCart() {
            if(vm.cartItem.subTotal > 0) {
                $iwCart.addItem(vm.cartItem);
                /*eslint-disable */
                $scope.closeThisDialog();
                /*eslint-enable */
            }
        }
    }

})();
