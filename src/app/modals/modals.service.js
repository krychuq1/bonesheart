(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .service('ModalService', ModalService);

    /** @ngInject */
    function ModalService($q, $log, $mdDialog, ngDialog, SingleLineProducts, SyndicateProducts) {
        // var vm = this;
        
        /**
         * Modal service
         * 
         * @type {Object}
         */
        var service = {
            login: login,
            register: register,
            singleLine: singleLine,
            syndicate: syndicate,

            showLogin: login,
            showRegister: register
        };
        return service;

        /**
         * Show login
         */
        function login(/*ev*/) {
            var deferred = $q.defer();

            var loginDialog = ngDialog.open({
                name: 'loginModal',
                template: 'app/modals/login/login.html',
                appendClassName: 'login-modal',
                controller: 'LoginModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            loginDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Show register
         */
        function register(/*ev*/) {
            var deferred = $q.defer();

            var registerDialog = ngDialog.open({
                name: 'loginModal',
                template: 'app/modals/register/register.html',
                appendClassName: 'register-modal',
                controller: 'RegisterModalController',
                controllerAs: 'vm',
                closeByDocument: false
            });

            registerDialog.closePromise.then(function (data) {
                if(data.value === true) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }

        /**
         * Single Line
         */
        function singleLine(id) {
            $log.log('Showing single line', id);
            var deferred = $q.defer();

            // Fetch the product
            var product = SingleLineProducts.get(id);

            // Check if we have the product
            if(product) {
                var singleLineDialog = ngDialog.open({
                    name: 'singleLineModal',
                    template: 'app/modals/singleline/singleline.html',
                    appendClassName: 'game single-line-modal',
                    controller: 'SingleLineModalController',
                    controllerAs: 'vm',
                    closeByDocument: false,
                    data: {
                        productId: id,
                        product: product
                    }
                });

                singleLineDialog.closePromise.then(function (data) {
                    if(data.value === true) {
                        deferred.resolve(data);
                    }
                    else {
                        deferred.reject(data);
                    }
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }

        /**
         * Syndicate
         */
        function syndicate(id/*, ev*/) {
            $log.log('Showing syndicate', id);
            var deferred = $q.defer();

            // Fetch the syndicate
            var syndicate = SyndicateProducts.get(id);

            $log.log('Found', syndicate);

            // Check if we have the syndicate
            if(syndicate) {
                var syndicateDialog = ngDialog.open({
                    name: 'loginModal',
                    template: 'app/modals/syndicate/syndicate.html',
                    appendClassName: 'game syndicate-modal',
                    controller: 'SyndicateModalController',
                    controllerAs: 'vm',
                    closeByDocument: false,
                    data: {
                        productId: id,
                        product: syndicate
                    }
                });

                syndicateDialog.closePromise.then(function (data) {
                    if(data.value === true) {
                        deferred.resolve(data);
                    }
                    else {
                        deferred.reject(data);
                    }
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }
    }

})();