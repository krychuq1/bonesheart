(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($log, $mdSidenav, $iwProducts) {
        var vm = this;

        vm.navOpen = true;


        // Featured lottery
        vm.featuredLottery = {};

        // 
        vm.featuredSyndicate = {};

        init();

        function init() {

            // Setup the featured syndicate
            if($iwProducts.isLoaded) {
                setFeaturedSyndicate();
            }
            else {
                $iwProducts.Syndicates.load().then(setFeaturedSyndicate);
            }
        }

        /**
         * Set the featured syndicate
         * 
         * @return {void}
         */
        function setFeaturedSyndicate() {

            // Get a list of all syndicates
            var syndicates = $iwProducts.Syndicates.all();

            // Get the cheapest (last one)
            if(syndicates.length > 0) {
                vm.featuredSyndicate = syndicates[syndicates.length - 1];
            }

            vm.featuredLottery = $iwProducts.SingleLines.get(10);
        }



        vm.closeMenu = function() {
            $mdSidenav('mobile-payslip').close();
        }

        vm.toggle = function() {
            $mdSidenav('mobile-payslip').toggle();
        };
    }

})();
