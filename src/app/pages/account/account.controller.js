(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('AccountPageController', AccountPageController);

    /** @ngInject */
    function AccountPageController($log, AccountModalService, $iwAccount) {
        var vm = this;

        vm.deposit = deposit;
        vm.depositLimits = depositLimits;
        vm.lossLimits = lossLimits;
        vm.purchaseHistory = purchaseHistory;
        vm.selfExclusion = selfExclusion;
        vm.transactionHistory = transactionHistory;
        vm.updateAddress = updateAddress;
        vm.updateEmail = updateEmail;
        vm.updatePassword = updatePassword;
        vm.wagerLimits = wagerLimits;
        vm.withdraw = withdraw;
        vm.tickets = [];

        getSubscriptions();

        function getSubscriptions() {
            $log.log('Getting subscriptions');
            $iwAccount.getSubscriptions().then(
                function success(response) {
                    $log.log('Success subscriptions', response);
                    vm.tickets = response;
                },
                function error(response) {
                    $log.log('Error getting subscriptions', response);
                }
            );
        }

        /**
         * Opens the deposit modal
         * @param  {event} ev The event the user has initiated
         */
        function deposit(ev){
            $log.log('Modal Activated: deposit');
            AccountModalService.deposit(ev);
        }

        /**
         * Opens the depositLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function depositLimits(ev){
            $log.log('Modal Activated: depositLimits');
            AccountModalService.depositLimits(ev);
        }

        /**
         * Opens the lossLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function lossLimits(ev){
            $log.log('Modal Activated: lossLimits');
            AccountModalService.lossLimits(ev);
        }

        /**
         * Opens the purchaseHistory modal
         * @param  {event} ev The event the user has initiated
         */
        function purchaseHistory(ev){
            $log.log('Modal Activated: purchaseHistory');
            AccountModalService.purchaseHistory(ev);
        }

        /**
         * Opens the selfExclusion modal
         * @param  {event} ev The event the user has initiated
         */
        function selfExclusion(ev){
            $log.log('Modal Activated: selfExclusion');
            AccountModalService.selfExclusion(ev);
        }

        /**
         * Opens the transactionHistory modal
         * @param  {event} ev The event the user has initiated
         */
        function transactionHistory(ev){
            $log.log('Modal Activated: transactionHistory');
            AccountModalService.transactionHistory(ev);
        }

        /**
         * Opens the updateAddress modal
         * @param  {event} ev The event the user has initiated
         */
        function updateAddress(ev){
            $log.log('Modal Activated: updateAddress');
            AccountModalService.updateAddress(ev);
        }

        /**
         * Opens the updateEmail modal
         * @param  {event} ev The event the user has initiated
         */
        function updateEmail(ev){
            $log.log('Modal Activated: updateEmail');
            AccountModalService.updateEmail(ev);
        }

        /**
         * Opens the updatePassword modal
         * @param  {event} ev The event the user has initiated
         */
        function updatePassword(ev){
            $log.log('Modal Activated: updatePassword');
            AccountModalService.updatePassword(ev);
        }

        /**
         * Opens the wagerLimits modal
         * @param  {event} ev The event the user has initiated
         */
        function wagerLimits(ev){
            $log.log('Modal Activated: wagerLimits');
            AccountModalService.wagerLimits(ev);
        }

        /**
         * Opens the withdraw modal
         * @param  {event} ev The event the user has initiated
         */
        function withdraw(ev){
            $log.log('Modal Activated: withdraw');
            AccountModalService.withdraw(ev);
        }

    }

})();
