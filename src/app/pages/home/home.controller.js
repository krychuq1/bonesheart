(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('HomePageController', HomePageController);

    /** @ngInject */
    function HomePageController($scope, $timeout, $log, SingleLineProducts) {
        var vm = this;

        // Promo area slider settings
        vm.promoSlider = {
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        // Lotteries slider settings
        vm.lotteriesSlider = {
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }
            ]
        };

        // Results slider settings
        vm.resultsSlider = {
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }
            ]
        };

        // Lottery results
        vm.lotteryResults = [];
        vm.lotteries = [];

        // Lottery results
        SingleLineProducts.results().then(function(results) {

            var lotteryResults = [];

            // Only add the first item from the results
            angular.forEach(results, function(result) {
                lotteryResults.push(result[0]);
            });

            vm.lotteryResults = lotteryResults;
        });

        //$log.log(vm.lotteries);
        SingleLineProducts.load().then(function() {
            // Set the lotteries
            vm.lotteries = SingleLineProducts.all();
        });
    }

})();
