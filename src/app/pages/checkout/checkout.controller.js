(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('CheckoutPageController', CheckoutPageController);

    /** @ngInject */
    function CheckoutPageController($scope, $log, $iwCart, $timeout, $iwAccount, $iwSession, $iwPayments, $window, $stateParams, ModalService) {
        var vm = this;

        vm.cart = $iwCart.getCart();
        vm.account = $iwAccount.getAccount();
        vm.session = $iwSession.getSession();
        vm.processOrder = processOrder;
        vm.deposit = deposit;
        vm.login = login;
        vm.register = register;

        vm.depositData = {
            cardNumber: '',
            expirationMonth: '',
            expirationYear: '',
            cvv: ''
        };

        // Processing feedback
        vm.processing = {
            processing: false,
            success: false,
            error: false,
            message: '' 
        };

        // Years
        vm.years = [];

        // Order complete
        vm.orderComplete = false;

        init();

        function init() {

            // Check if we are waiting for a deposit response
            if($stateParams.txnId) {
                checkDepositStatus($stateParams.txnId.replace('?txnId=', ''));
            }

            // Setup the years array
            vm.years = generateYears(10);
        }

        function generateYears(max) {
            var currentYear = new Date().getFullYear();
            var years = [];
            do {
                years.push(currentYear++);
                max--;
            }
            while(max > 0);
            return years;
        }

        function setProcessingStatus(processing, success, error, message) {
            if(processing) {
                vm.processing.processing = true;
                vm.processing.success = false;
                vm.processing.error = false;
                vm.processing.message = false;
            }
            else {
                vm.processing.processing = false;
                vm.processing.success = success;
                vm.processing.error = error;
                vm.processing.message = message;
            }
        }

        function checkDepositStatus(id) {

            // 
            $log.log('Checking deposit status');

            setProcessingStatus(true);

            // Check the status
            $iwPayments.checkStatus(id)
                .then(depositSuccess, depositFailed);

            function depositSuccess(response) {
                $log.log('Deposit success', response);
                setProcessingStatus(false, true, false, 'Success');
            }

            function depositFailed(response) {
                $log.log('Deposit failed', response);
                if(response.data.status == 'SUCCESS') {
                    setProcessingStatus(false, false, true, response.data.response.processorResponseMessage);
                }
                else {
                    setProcessingStatus(false, false, true, response.data.errorDescription);
                }
                //vm.account.balance = 10000;
            }
        }

        function processOrder() {
            $log.log('Processing order');

            setProcessingStatus(true);

            $iwCart.commit()
                .then(orderSuccess, orderFailed);

            function orderSuccess(/* response */) {
                $timeout(function() {
                    setProcessingStatus(false);
                    vm.orderComplete = true;
                }, 1500);
            }

            function orderFailed(response) {
                setProcessingStatus(false, false, true, response.data.errorDescription);
            }
        }

        function deposit() {

            // Deposit request
            var request = {
                amount: vm.cart.subTotal - vm.account.balance,
                cardNumber: vm.depositData.cardNumber,
                expirationMonth: vm.depositData.expirationMonth,
                expirationYear: vm.depositData.expirationYear,
                cvv: vm.depositData.cvv,
                firstName: vm.account.profile.firstName,
                lastName: vm.account.profile.lastName,
                redirectionURL: $window.location.href
            };

            if(vm.depositForm.$valid) {

                // Set feedback
                setProcessingStatus(true);
                $log.log('Depositing with', request);

                $iwPayments.deposit('wirecard', request)
                   .then(depositSuccess, depositFailed);
            }

            function depositSuccess(response) {
                $log.log('DP Success -> ', response);
                //setProcessingStatus(false, true, false, 'Deposit success!');
            }

            function depositFailed(response) {
                $log.log('DP Failed -> ', response);
                setProcessingStatus(false, false, true, response.data.errorDescription);
            }
        }

        function login() {
            // Open the login modal
            ModalService.login().then(loginSuccess);

            function loginSuccess() {
                //$state.go('checkout');
            }
        }

        function register() {
            ModalService.register().then(registerSuccess);

            function registerSuccess(resp) {
                //$log.log('Reg success', resp);
            }
        }
    }

})();
