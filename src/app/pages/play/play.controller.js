(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .controller('PlayPageController', PlayPageController);

    /** @ngInject */
    function PlayPageController($log, IWConstants, $filter, ModalService, SingleLineProducts, SyndicateProducts, $stateParams, $iwCart, $iwProducts) {
        var vm = this;

        //$log.log('On play page', $stateParams);

        vm.products = [];
        vm.syndicates = [];
        vm.games = [];
        vm.play = playSingle;
        vm.playSyndicate = playSyndicate;
        vm.setFilter = setFilter;
        vm.setOrder = setOrder;

        vm.sortType = '_jackpot';
        vm.sortReverse = true;
        vm.sortFilter = '';

        vm.cart = $iwCart.getCart();

        vm.productTypes = IWConstants.PRODUCT_TYPES;

        vm.lottoConfig = {
            european: [
                'Euro Jackpot', 'La Primitiva', 'Euro Millions', 'German Lotto 6aus49', 'UK National Lottery', 'Irish Lotto', 'UK Thunderball', 'France Loto', 'El Gordo de la Primitiva', 'Super 6', 'Glücksspirale', 'Austria 6/45', 'Spiel 77', 'MegaSena', 'SuperEnalotto', 'Swedish Lotto', 'Polish Lotto', 'Polish Lotto Plus', 'Lotería Nacional', 'Finland Lotto', 'BonoLoto'
            ],
            huge: [
                'Euro Jackpot', 'Megamillions', 'Powerball', 'Euro Millions', 'Powerball Australia', 'El Gordo de la Primitiva', 'SuperEnalotto', 'Powerball (South Africa)'
            ],
            odds: [
                'Irish Lotto', 'UK Thunderball', 'France Loto', 'Spiel 77', 'California SuperLottoPlus', 'New York Lotto', 'Florida Lotto', 'Hot Lotto', 'Hoosier Lotto', 'Canadian Lotto 6/49', 'Polish Lotto', 'LOTTO 6/49 (South Africa)'
            ]
        };


        // Load products
        $iwProducts.load().then(function success() {

            // 
            vm.syndicates = SyndicateProducts.all();
            vm.products = SingleLineProducts.all();
            vm.games = vm.syndicates.concat(vm.products);

            // Loop through each game and add filter options
            angular.forEach(vm.games, function(game) {

                // Filter classes for isotop sorting
                //var filterClasses = [];

                // Add values for sorting and filtering
                if(game.productType === IWConstants.PRODUCT_TYPES.SYNDICATE) {
                    game._jackpot = game.jackpot.value / 100000;
                    game._date = game.drawDate;
                    //filterClasses.push('syndicate');
                }
                else if(game.productType === IWConstants.PRODUCT_TYPES.SINGLE_LINE) {
                    game._jackpot = game.jackpot.local.value / 100000;
                    game._date = game.drawDates.next;

                    // 
                    //filterClasses.push('singleline');

                    // Huge jackpot filter values
                    if(vm.lottoConfig.huge.indexOf(game.name) > -1) {
                        game._huge = true;
                        //filterClasses.push('huge');
                    }
                    else {
                        game._huge = false;
                    }

                    // European filter values
                    if(vm.lottoConfig.european.indexOf(game.name) > -1) {
                        game._european = true;
                        //filterClasses.push('european');
                    }
                    else {
                        game._european = false;
                    }

                    // Great odd filter values
                    if(vm.lottoConfig.odds.indexOf(game.name) > -1) {
                        game._odds = true;
                        //filterClasses.push('odds');
                    }
                    else {
                        game._odds = false;
                    }
                }

                // Set filter classes for isotop
                //game._classes = filterClasses.join(' ');
            });

            $log.log(vm.games);

            // Open modal if we need to
            if(angular.isDefined($stateParams.productId) && $stateParams.productId) {
                ModalService.singleLine($stateParams.productId);
            }
        });


        // // Load single line product
        // SingleLineProducts.load().then(function success(response) {

        //     // Set the products
        //     vm.products = SingleLineProducts.all();

        //     // 
        //     angular.forEach(vm.products, function(product) {
        //         $log.log(product);
        //         product._jackpot = product.jackpot.local.value / 100000;
        //         product._date = product.drawDates.next
        //     });

        //     // Open modal if we need to
        //     if(angular.isDefined($stateParams.productId) && $stateParams.productId) {
        //         ModalService.singleLine($stateParams.productId);
        //     }
        // });

        // // Load syndicate products
        // SyndicateProducts.load().then(
        //     function success(response) {
        //         //$log.log(response);
        //         vm.syndicates = SyndicateProducts.all();
        //     },
        //     function error(response) {
        //         //$log.log(response);
        //     }
        // );

        function playSingle(productId) {
            ModalService.singleLine(productId);
        }

        function playSyndicate(productId) {
            $log.log('Playing syndicate', productId);
        }

        function setOrder(order) {
            if(order == 'next') {
                vm.sortType = '_date';
            }
            else if(order == 'name') {
                vm.sortType = 'name';
            }
            else if(order == 'jackpot') {
                vm.sortType = '_jackpot';
            }
            vm.sortReverse = !vm.sortReverse;
        }

        function setFilter(filter) {
            if(filter == 'syndicates') {
                vm.sortFilter = { productType: IWConstants.PRODUCT_TYPES.SYNDICATE };
            }
            else if(filter == 'single_line') {
                vm.sortFilter = { productType: IWConstants.PRODUCT_TYPES.SINGLE_LINE };
            }
            else if(filter == 'european') {
                vm.sortFilter = { _european: true };
            }
            else if(filter == 'huge') {
                vm.sortFilter = { _huge: true };
            }
            else if(filter == 'odds') {
                vm.sortFilter = { _odds: true };
            }
            else {
                vm.sortFilter = '';
            }
        }
    }

})();
