(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .controller('LotteryPrizeBreakdown', LotteryPrizeBreakdown);

  /** @ngInject */
  function LotteryPrizeBreakdown($log, SingleLineProducts, $filter, $rootScope, $mdMedia,$stateParams, $getLotteryHistory) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;
    $getLotteryHistory.getBreakDown($stateParams.name, $stateParams.drawDate).then(function(data) {
      vm.lotteryName = $stateParams.name;
      vm.lotteryDrawHistory = data.history;
      vm.lotteryDrawIndex = data.index;
    });

  }

})();
