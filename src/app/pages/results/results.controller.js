(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .controller('ResultsPageController', ResultsPageController);

  /** @ngInject */
  function ResultsPageController($log, SingleLineProducts, $filter, $rootScope,$mdMedia,$state) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;

    vm.results = [];

    //go to function
    vm.goTo = function (path,results) {
      var data = {
        name : results[0].lotteryName,
        history : results
      };
      //localStorage.setItem('historyItem',JSON.stringify(data));
      $state.go(path,data);
    };


    // Load the results from service
    SingleLineProducts.results().then(
      function success(response) {
        // Set results
        vm.results = response;
        $log.log(vm.results);
      },
      function error(response) {
        // vm.results = response;
        $log.log('Results error', response);
        //$log.log(vm.results);
      }
    );

    //format the date to be more readable
    vm.formatDate = function (/* lottery */) {
      //var date = new Date(lottery.nextDrawDateTimeList[lottery.nextDrawDateTimeList.length - 1]);
        //var yyyy = date.getFullYear().toString();
        //var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
        //var dd = date.getDate().toString();
        //return yyyy + '/' +  (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
    };

    // Function to calculate time difference between dates - dd/hh/mm
    vm.calculateTime = function(lottery) {
      //console.log(lottery.lotteryName, lottery.nextDrawDateTimeList[0]);
      var today = new Date(); // today's date
      var nextDraw = new Date(lottery.nextDrawDateTimeList[0]); // next draw date for the lottery
      var difference = nextDraw.getTime() - today.getTime();

      //calculate days
      var daysLeft = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysLeft * 1000 * 60 * 60 * 24;
      daysLeft = (daysLeft.toString().length < 2) ? "0"+ daysLeft : daysLeft;

      //calcualte hours
      var hoursLeft = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursLeft * 1000 * 60 * 60;
      hoursLeft = (hoursLeft.toString().length < 2) ? "0"+ hoursLeft : hoursLeft;

      //calculate minutes
      var minutesLeft = Math.floor(difference / 1000 / 60);
      difference -= minutesLeft * 1000 * 60;
      minutesLeft = (minutesLeft.toString().length < 2) ? "0"+ minutesLeft : minutesLeft;
      //console.log(daysLeft, hoursLeft, minutesLeft);

      return daysLeft + " : " + hoursLeft + " : " + minutesLeft;
    };
  }

})();
