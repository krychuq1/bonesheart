(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .controller('LotteryHistoryResult', LotteryHistoryResult);

  /** @ngInject */
  function LotteryHistoryResult($log, SingleLineProducts, $filter,$stateParams, $rootScope,$mdMedia,$getLotteryHistory) {
    var vm = this;
    //bind $mdMEdia to the scope so we can access it in the view
    $rootScope.$mdMedia = $mdMedia;
    $getLotteryHistory.getHistoryForLottery($stateParams.name).then(function (data) {
      vm.lotteryHistory = data;
      vm.lottery = vm.lotteryHistory[0];
    });

    //console.log(vm.lotteryHistory, vm.lottery);
  }

})();
