(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('arrowBar', ArrowBar);

    /** @ngInject */
    function ArrowBar() {
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/arrow-bar/arrow-bar.html',
            template: '<span class="arrow-bar-arrow"></span>',
            scope: {
                direction: '@',
                scrollTo: '@'
            },
            link: ArrowBarLink,
            transclude: true
        };

        return directive;

        /** @ngInject */
        function ArrowBarLink(scope, iElement, iAttrs, controller, transclude) {

            // Add the transcluded content
            iElement.prepend(transclude());

            // Arrow
            var arrow = iElement.find('.arrow-bar-arrow');

            // Target scroll element
            var target = iAttrs.scrollTo ? angular.element(iAttrs.scrollTo) : false;

            // If the string 'top' was passed then scroll to top of page
            if(iAttrs.scrollTo && iAttrs.scrollTo === 'top') {
                target = angular.element('body');
            }

            // If scroll target exists attach a click handler
            if(target && target.length) {
                arrow.addClass('action');
                arrow.bind('click', function() {
                    angular.element('html,body').animate({ scrollTop: target.offset().top }, 'slow');
                });
            }
        }
    }

})();
