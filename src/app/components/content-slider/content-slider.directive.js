(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('contentCarousel', contentCarousel);

    /** @ngInject */
    function contentCarousel() {
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/content-carousel/content-carousel.html',
            template: '<slick settings="vm.carouselConfig"></slick>',
            scope: {
                settings: '='
            },
            link: ContentCarousellLink,
            transclude: true,
            controller: ContentCarouselController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function ContentCarousellLink(scope, iElement, iAttrs, controller, transclude) {

            // Add class
            iElement.addClass('content-carousel');

            // Add the transcluded content
            iElement.find('slick').append(transclude());
        }

        /** @ngInject */
        function ContentCarouselController() {
            var vm = this;

            // Carousel settings
            vm.carouselConfig = {
                prevArrow: '<button class="slick-prev"><i class="material-icons">&#xE314;</i></button>',
                nextArrow: '<button class="slick-next"><i class="material-icons">&#xE315;</i></button>',
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                dots: false
            };

            // Combine user settings with our default settings
            angular.extend(vm.carouselConfig, vm.settings);
        }
    }

})();
