(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('nextDraw', nextDraw);

    /** @ngInject */
    function nextDraw() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/next-draw/next-draw.html',
            scope: {
                productId: '=?',
                product: '=?'
            },
            controller: NextDrawController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        /** @ngInject */
        function NextDrawController($log, $locale, ModalService, SingleLineProducts, $timeout, IWConstants, $iwSession) {
            var vm = this;

            // Product type constants
            vm.productTypes = IWConstants.PRODUCT_TYPES;
            vm.session = $iwSession.getSession();

            // Public functions
            vm.play = play;

            init();

            /**
             * Initialise next draw (Grab product if got passed id)
             * 
             * @return {void}
             */
            function init() {
                if(angular.isDefined(vm.productId) && angular.isNumber(vm.productId)) {
                    $log.log('Loading the product', vm.productId);
                    vm.product = SingleLineProducts.one(vm.productId);
                }
            }

            /**
             * Open the play modal
             * 
             * @return {vopid}
             */
            function play() {
                if(vm.product.productType === IWConstants.PRODUCT_TYPES.SINGLE_LINE) {
                    ModalService.singleLine(vm.productId);
                }
                else if(vm.product.productType === IWConstants.PRODUCT_TYPES.SYNDICATE) {
                    ModalService.syndicate(vm.productId);
                }
            }
        }
    }

})();
