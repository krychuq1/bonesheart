(function() {
    'use strict';

    /**
     * This directive is pretty much a clone of the mdSidenav directive. This allows
     * us to use our own code which fixes the issues mdSidenav has with Internet
     * Explorer and mobile devices while still maintaining the functionality
     * mdSidenav provides. Some features have been stripped out.
     */
    angular
        .module('sevenLottos')
        .directive('slSidenav', slSidenav);

    /** @ngInject */
    function slSidenav($mdUtil, $document, $animate, $q, $mdConstant) {
        var directive = {
            restrict: 'E',
            //templateUrl: 'app/components/sidenav/sidenav.html',
            template: '<div ng-transclude></div>',
            scope: {
                isOpen: '=?isOpen'
            },
            transclude: true,
            controller: SidenavController,
            controllerAs: 'vm',
            bindToController: true,
            compile: function(element) {
                element.addClass('sl-sidenav');
                return postLink;
            }
        };

        return directive;

        // Postlink function
        function postLink (scope, element, attr, sidenavCtrl) {

            // Default body overflow value
            var bodyOverflow = undefined;

            // Backdrop
            var backdrop;

            // Promise object
            var promise = $q.when(true);

            // Body element we insert our backdrop into
            var body = $document.find('body').eq(0);

            // Create the backdrop only if we want to
            if(angular.isUndefined(attr.disableBackdrop)) {
                backdrop = angular.element('<div class="sl-sidenav-backdrop"></div>');
            }

            // Watch for the isOpen changes and show/hide menu & backdrop
            scope.$watch('isOpen', changeIsOpen);

            // Publish special accessor for the Controller instance
            sidenavCtrl.$toggleOpen = toggleOpen;

            // Change opens status of menu
            function changeIsOpen(isOpen) {

                // Attache keypress event to close nav when 'escape' is pressed
                body[isOpen ? 'on' : 'off']('keydown', onKeyDown);

                // Attach close event to backdrop on click
                if(backdrop) {
                    backdrop[isOpen ? 'on' : 'off']('click', close);
                }

                // Disable body scroll
                disableScroll(isOpen);

                // Return the promise object
                return promise = $q.all([
                    // Show/hide backdrop
                    isOpen && backdrop ? $animate.enter(backdrop, body) : backdrop ? $animate.leave(backdrop) : $q.when(true),

                    // Add / Remove open class
                    $animate[!isOpen ? 'removeClass' : 'addClass'](element, 'sl-sidenav-open')
                ]);
            }

            // Disable scrolling of the body element
            function disableScroll(disabled) {
                if(disabled && !bodyOverflow) {
                    bodyOverflow = body.css('overflow');
                    body.css('overflow', 'hidden')
                    angular.element('html').addClass('sidenav-open');
                }
                else if(angular.isDefined(bodyOverflow)) {
                    body.css('overflow', bodyOverflow)
                    angular.element('html').removeClass('sidenav-open');
                    bodyOverflow = undefined;
                }
            }

            // Toggle the open/close status of the menu
            function toggleOpen(isOpen) {

                // Check if the menu is already at the state the user wants it at
                if(scope.isOpen == isOpen) {
                    return $q.when(true);
                }
                else {
                    return $q(function(resolve) {
                        // Toggle value to force an async 'changeIsOpen()' to run
                        scope.isOpen = isOpen;

                        // When the current navigation animation finishes resolve
                        $mdUtil.nextTick(function() {
                            promise.then(function(result) {
                                resolve(result);
                            });
                        });
                    });
                }
            }

            // Close nav with the scape key is pressed
            function onKeyDown(ev) {
              var isEscape = (ev.keyCode === $mdConstant.KEY_CODE.ESCAPE);
              return isEscape ? close(ev) : $q.when(true);
            }

            // Close
            function close(ev) {
                ev.preventDefault();
                sidenavCtrl.close();
            }
        }

        /** @ngInject */
        function SidenavController($scope, $log, $mdComponentRegistry, $attrs) {
            var vm = this;

            // Open the menu
            vm.open = function() {
                return vm.$toggleOpen(true);
            };

            // Close the menu
            vm.close = function() {
                return vm.$toggleOpen(false);
            };

            // Toggle the menu
            vm.toggle = function() {
                return vm.$toggleOpen(!$scope.isOpen);
            };

            // Toggle open
            vm.$toggleOpen = function(value) {
                return $q.when($scope.isOpen = value);
            };

            // Register component so we can use it with $mdSidenav service
            $mdComponentRegistry.register(vm, $attrs.componentId);
        }
    }

})();