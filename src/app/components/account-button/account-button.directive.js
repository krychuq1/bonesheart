(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('accountButton', accountButton);

    /** @ngInject */
    function accountButton() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/account-button/account-button.html',
            scope: {
                icon: '@'
            },
            transclude: true
            //controller: AccountButtonController,
            //controllerAs: 'vm',
            //bindToController: true
        };

        return directive;

        // function AccountButtonController($log) {
        //     var vm = this;
        // }
    }

})();
