(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .directive('lotteryDrawPrizeBreakdown', lotteryDrawPrizeBreakdown);

  /** @ngInject */
  function lotteryDrawPrizeBreakdown() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/lotteryDrawPrizeBreakdown/draw-prize.html',
      controller: LotteryPrizeBreakdownController,
      controllerAs: 'prizeCtrl',
      bindToController: true,
      scope: {
        index: "=",
        history: "="
      }
    };

    return directive;

    /** @ngInject */
    function LotteryPrizeBreakdownController($scope, SingleLineProducts, $getPrizeBreakdown,$mdMedia) {
      $scope.$mdMedia = $mdMedia;
      var vm = this;
      //var lotteryNext;
      vm.query = {
        order: 'name',
        limit: 5,
        page: 1
      };
      //console.log('from the draw prize breakdown directive ', vm.history, vm.index);
      vm.currentDraw = vm.history[vm.index];
      var nextDrawIndex = vm.index - 1;
      var previousDrawIndex = vm.index + 1;
      vm.winningNumbers = {
        primaryNumbers: [],
        secondaryNumbers: []
      };
      vm.nextDraw = vm.history[nextDrawIndex];
      vm.previousDraw = vm.history[previousDrawIndex];
      $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
        //console.log('done', prizes);
        vm.drawPrizes = prizes;
        vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
          return parseInt(item, 10);
        });
        vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
          return parseInt(item, 10);
        });
      });

      //go thourgh the array and get next draw
      vm.getNextDraw = function() {
        if (vm.index > 0) {
          vm.index = vm.index - 1;
          //console.log(vm.index, vm.history.length);
          vm.currentDraw = vm.history[vm.index];
          nextDrawIndex = vm.index - 1;
          previousDrawIndex = vm.index + 1;
          vm.nextDraw = vm.history[nextDrawIndex];
          vm.previousDraw = vm.history[previousDrawIndex];
          $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
            //console.log('done', prizes);
            vm.drawPrizes = prizes;
            vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
            vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
          });
        }
      };

      //go thourgh the array and get prev draw
      vm.getPrevDraw = function() {
        if (vm.index < vm.history.length - 1) {
          vm.index = vm.index + 1;
          //console.log(vm.index, vm.history.length);
          vm.currentDraw = vm.history[vm.index];
          nextDrawIndex = vm.index - 1;
          previousDrawIndex = vm.index + 1;
          vm.nextDraw = vm.history[nextDrawIndex];
          vm.previousDraw = vm.history[previousDrawIndex];
          $getPrizeBreakdown.getPrizes(vm.currentDraw.drawDateTime, vm.currentDraw.drawId).then(function(prizes) {
            //console.log('done',prizes);
            vm.drawPrizes = prizes;
            vm.winningNumbers.primaryNumbers = prizes.lottoWinningNumber.primaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
            vm.winningNumbers.secondaryNumbers = prizes.lottoWinningNumber.secondaryNumbers.split('-').map(function(item) {
              return parseInt(item, 10);
            });
          });
        }
      };

      // go to select ticket page
      vm.play = function() {
          // do something here @Ben
      };
    }

  }

})();
