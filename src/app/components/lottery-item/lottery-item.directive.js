(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('lotteryItem', lotteryItem);

    /** @ngInject */
    function lotteryItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/lottery-item/lottery-item.html',
            scope: {
                data: '='
            },
            link: LotteryItemLink,
            replace: true,
            controller: LotteryItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function LotteryItemLink(/*scope, iElement, iAttrs, controller, transclude*/) {

            // 
        }

        /** @ngInject */
        function LotteryItemController($log, ModalService) {
            var vm = this;

            vm.play = play;

            function play() {
                ModalService.singleLine(vm.data.id);
            }
            //$log.log('Lottery controller', vm.data);
        }
    }

})();
