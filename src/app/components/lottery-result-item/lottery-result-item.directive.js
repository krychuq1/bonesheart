(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('lotteryResultItem', lotteryResultItem);

    /** @ngInject */
    function lotteryResultItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/lottery-result-item/lottery-result-item.html',
            scope: {
                data: '='
            },
            link: LotteryResultItemLink,
            replace: true,
            controller: LotteryResultItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function LotteryResultItemLink(/* scope, iElement, iAttrs, controller, transclude */) {

            // 
        }

        /** @ngInject */
        function LotteryResultItemController($log, ModalService, SingleLineProducts) {
            var vm = this;

            vm.play = play;

            function play() {
                var productId = SingleLineProducts.convertId(vm.data.drawTypeId);
                if(productId) {
                    ModalService.singleLine(productId);
                }
            }
        }
    }

})();
