(function() {
  'use strict';

  angular
    .module('sevenLottos')
    .directive('lotteryDrawHistory', lotteryDrawHistory);

  /** @ngInject */
  function lotteryDrawHistory() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/lotteryDrawHistory/draw-history.html',
      controller: LotteryDrawHistory,
      controllerAs: 'drawCtrl',
      bindToController: true,
      scope: {
        history: "="
      }
    };

    return directive;

    /** @ngInject */
    function LotteryDrawHistory($scope,SingleLineProducts, $filter,$getPrizeBreakdown,$state, $interval) {
      var vm = this;
      var lotteryNext;
      //console.log('from the draw history directive ', vm.history);
      angular.forEach(vm.history, function(draw) {
        $getPrizeBreakdown.getPrizes(draw.drawDateTime, draw.drawId).then(function(prizes) {
            draw.prize = prizes;
        }, function() {
            //console.log('error geting prizes for draw', error);
        });
      });
      vm.lotteryName = vm.history[0].lotteryName;
      vm.nextDraw = vm.history[0].nextDrawDateTimeList;
      vm.drawTypeId = vm.history[0].drawTypeId;
      // pagination
      vm.curPage = 0;
      vm.pageSize = 10;

      vm.numberOfPages = function() {
        return Math.ceil(vm.history.length / vm.pageSize);
      };

      //prev function
      vm.prev = function() {
        vm.curPage = vm.curPage - 1;
      };

      //prev function
      vm.next = function() {
        vm.curPage = vm.curPage + 1;
      };

      // go to prize breakdown page
      vm.prizeBreakdown = function(drawTime, drawId) {
        //console.log(drawTime, drawId);
        var index = vm.history.map(function(obj, index) {
          if (obj.drawId == drawId) {
            return index;
          }
        }).filter(isFinite)[0];
        //console.log(index);
        var data = {
          name: vm.lotteryName,
          drawDate: drawTime,
          index: index,
          history: vm.history
        };
        //console.log(data);
        //localStorage.setItem('prizeItem', JSON.stringify(data));
        $state.go('prizebreakdown', data);
      };

      //compare dates
      var today = $filter('formatDate')(new Date());
      vm.compareDates = function(dateArr) {
        var keepGoing = true;
        var endDate;
        angular.forEach(dateArr, function(date) {
          var lotteryDate = $filter('formatDate')(date);
          if (keepGoing) {
            if (lotteryDate > today) {
              endDate = date;
              keepGoing = false;
            }
          }
        });
        return endDate;
      };
      //time left to next draw
      vm.calculateTime = function(date) {
        lotteryNext = date;
        var today = new Date(); // today's date
        var nextDraw = new Date(date); // next draw date for the lottery
        var difference = nextDraw.getTime() - today.getTime();

        //calculate days
        var daysLeft = Math.floor(difference / 1000 / 60 / 60 / 24);
        difference -= daysLeft * 1000 * 60 * 60 * 24;
        daysLeft = (daysLeft.toString().length < 2) ? "0" + daysLeft : daysLeft;

        //calcualte hours
        var hoursLeft = Math.floor(difference / 1000 / 60 / 60);
        difference -= hoursLeft * 1000 * 60 * 60;
        hoursLeft = (hoursLeft.toString().length < 2) ? "0" + hoursLeft : hoursLeft;

        //calculate minutes
        var minutesLeft = Math.floor(difference / 1000 / 60);
        difference -= minutesLeft * 1000 * 60;
        minutesLeft = (minutesLeft.toString().length < 2) ? "0" + minutesLeft : minutesLeft;


        var secondsLeft = Math.floor(difference / 1000);
        secondsLeft = (secondsLeft.toString().length < 2) ? "0" + secondsLeft : secondsLeft;

        //console.log(daysLeft, hoursLeft, minutesLeft, secondsLeft);

        return {
          daysLeft: daysLeft,
          hoursLeft: hoursLeft,
          minutesLeft: minutesLeft,
          secondsLeft: secondsLeft
        };
      };

      $interval(function(){
      //setInterval(function() {
        //if (lotteryNext !== undefined) {
        if(angular.isDefined(lotteryNext)) {
          vm.calculateTime(lotteryNext);
          //$scope.$apply();
        }
      }, 1000);
    }

  }

})();
