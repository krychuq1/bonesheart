(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('pageHeader', pageHeader);

    /** @ngInject */
    function pageHeader() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/header/header.html',
            scope: {
            },
            link: PageHeaderLink,
            controller: PageHeaderController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function PageHeaderLink(scope, iElement) {

            // Navigation bar
            var navbar = iElement.find('nav');

            // Navigation bar buttons width optional widget areas
            var buttons = iElement.find('nav .md-button-t');

            // Loop through each button and add handlers for widgets
            angular.forEach(buttons, function(value/*, index*/) {

                // Button
                var button = angular.element(value);

                // Widget area
                var widget = angular.element(button.next('.widget'));

                // If we have a widget area add click handler
                if(widget.length) {

                    // Add the dropdown arrow
                    button.append('<span class="toggle-icon"><i class="material-icons">&#xE313;</i></span>');

                    // Show / hide widget area
                    button.on('click', function() {
    
                        // Close open widgets
                        navbar.find('.open').not(button.parent()).removeClass('open').find('.widget').stop().slideUp();

                        // Show / hide this widget
                        widget.stop().slideToggle();

                        // Add / remove toggle class
                        widget.parent().toggleClass('open');
                    });
                }
            });


            // Outside header click then close widgets
            angular.element('body').on('click', function(e) {
                if(!angular.element(e.target).closest(iElement).length) {
                    navbar.find('.open').removeClass('open').find('.widget').slideUp();
                }
            });
        }

        /** @ngInject */
        function PageHeaderController($log, $mdSidenav, ModalService, $iwSession, $iwAccount, $state, $iwCart) {
            var vm = this;

            vm.cart = $iwCart.getCart();
            vm.menuOpen = false;
            vm.togglePlayslip = togglePlayslip;
            vm.toggleMenu = toggleMenu;

            // Get session object
            vm.session = $iwSession.getSession();
            vm.account = $iwAccount.getAccount();

            // Exposed methods
            vm.login = login;
            vm.logout = logout;
            vm.register = register;

            function toggleMenu() {
                vm.menuOpen = !vm.menuOpen;
            }

            function togglePlayslip() {
                $mdSidenav('mobile-payslip').toggle();
            }

            function register(/*ev*/) {
                ModalService.register().then(registerSuccess, registerFailed);

                function registerSuccess(resp) {
                    $log.log('Reg success', resp);
                }

                function registerFailed() {
                    //$log.log('Reg failed', resp);
                }
            }

            function login(/*ev*/) {
                // Open the login modal
                ModalService.login().then(loginSuccess, loginFailed);

                function loginSuccess() {
                    $state.go('home');
                }

                function loginFailed() {}
            }

            function logout(/*ev*/) {
                $iwAccount.logout();
                $state.go('play');
            }
        }
    }

})();
