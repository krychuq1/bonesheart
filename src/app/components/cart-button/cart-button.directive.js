(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('cartButton', CartButton);

    /** @ngInject */
    function CartButton() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/cart-button/cart-button.html',
            scope: {
                direction: '@',
                scrollTo: '@'
            },
            link: CartButtonLink,
            controller: CartButtonController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function CartButtonLink(scope, iElement/*, iAttrs, controller, transclude*/) {

            var dropdown = iElement.find('.dropdown-content');

            iElement.on('click', function() {
                dropdown.toggle();
            });


            // Outside header click then close widgets
            angular.element('body').on('click', function(e) {
                if(!angular.element(e.target).closest(iElement).length) {
                    dropdown.hide();
                }
            });
        }

        function CartButtonController($iwCart) {
            var vm = this;
            vm.cart = $iwCart.getCart();
        }
    }

})();
