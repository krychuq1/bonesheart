(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('accountInfo', accountInfo);

    /** @ngInject */
    function accountInfo() {
        var directive = {
            restrict: 'EA',
            controller: AccountInfoController
        };

        return directive;

        /** @ngInject */
        function AccountInfoController($scope) {

            $scope.firstName = 'Ben';
            $scope.lastName = 'Thomson';
            $scope.fullName = $scope.firstName + ' ' + $scope.lastName;
            $scope.loggedIn = true;
        }
    }

})();
