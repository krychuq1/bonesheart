(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('syndicateItem', syndicateItem);

    /** @ngInject */
    function syndicateItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/syndicate-item/syndicate-item.html',
            scope: {
                data: '='
            },
            link: SyndicateItemLink,
            replace: true,
            controller: SyndicateItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function SyndicateItemLink(/*scope, iElement, iAttrs, controller, transclude*/) {
            
            // 
        }

        function SyndicateItemController($log, ModalService) {
            var vm = this;

            vm.play = play;

            function play() {
                $log.log('Playing syndicate', vm.data);
                ModalService.syndicate(vm.data.id);
            }
        }
    }

})();
