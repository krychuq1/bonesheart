(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('playslip', playslip);

    /** @ngInject */
    function playslip() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/playslip/playslip.html',
            scope: {
                showCheckout: '@'
            },
            link: PlayslipLink,
            controller: PlayslipController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function PlayslipLink(/*scope, iElement, iAttrs*/) {

        }

        /** @ngInject */
        function PlayslipController($log, $iwCart, $iwSession, IWConstants) {
            var vm = this;

            if(angular.isUndefined(vm.showCheckout)) {
                vm.showCheckout = true;
            }
            else {
                vm.showCheckout = vm.showCheckout == true ? true : false;
                vm.showCheckout = true;
            }

            // Get the cart object
            vm.cart = $iwCart.getCart();

            // Session
            vm.session = $iwSession.getSession();

            // Constants for HTLM access
            vm.PLAYING_PERIODS = IWConstants.PLAYING_PERIODS;
            vm.BILLING_PERIODS = IWConstants.BILLING_PERIODS;

            vm.removeAllProducts = removeAllProducts;
            vm.removeItem = removeItem;
            
            vm.logCart = function() {
                $log.log(vm.cart);
            }

            function removeAllProducts() {
                $iwCart.empty();
            }

            function removeItem(index) {
                $iwCart.removeItem(index);
            }
        }
    }

})();
