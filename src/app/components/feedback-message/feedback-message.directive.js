(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('feedbackMessage', feedbackMessage);

    /** @ngInject */
    function feedbackMessage() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/feedback-message/feedback-message.html',
            scope: {
                type: '='
            },
            link: FeedbackMessageLink,
            transclude: true,
            replace: true,
            controller: FeedbackMessageController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function FeedbackMessageLink(/*scope, iElement, iAttrs, controller, transclude*/) {

        }

        /** @ngInject */
        function FeedbackMessageController() {
            //var vm = this;
        }
    }

})();
