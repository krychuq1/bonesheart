(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('cashier', cashier);

    /** @ngInject */
    function cashier() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/cashier/cashier.html',
            scope: {
            },
            link: CashierLink,
            controller: CashierController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        function CashierLink(/*scope, iElement, iAttrs*/) {

        }

        /** @ngInject */
        function CashierController($log,$scope) {
            //var vm = this;
            $log.log('Cashier loaded');
            $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
            $scope.card = {
              number: "0000 0000 0000 xxxx",
              expiry : {
                month : $scope.months[3],
                date : new Date()
              },
              cvv : 215,
              cardholder : "Mr john smith"
            };
        }
    }

})();
