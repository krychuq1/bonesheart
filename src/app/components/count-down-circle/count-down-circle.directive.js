(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('countDownCircle', countDownCircle);

    /** @ngInject */
    function countDownCircle() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/count-down-circle/count-down-circle.html',
            scope: {
                endDate: '@'
            },
            controller: CountDownController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function CountDownController($interval, $log, moment) {
            var vm = this;

            vm.maxProgress = 59;
            vm.currentProgress = 0;
            vm.currentText = '-';

            // End date in a moment object
            var endDate = moment(new Date(vm.endDate));

            // Update time and progress every second
            $interval(function() {
                // Get the difference from now and the end date
                var difference = endDate.diff(moment());

                // Get the duration
                var duration = moment.duration(difference);

                // We bit hacky, please chance this
                // vm.currentText = duration.humanize().replace(/[0-9]/g, '');

                // Show days
                if(duration.days() > 0) {
                    vm.maxProgress = 7;
                    vm.currentProgress = duration.days();
                    vm.currentText = duration.days() > 1 ? 'days' : 'day';
                }

                // Show hours
                else if(duration.hours() > 0) {
                    vm.maxProgress = 24;
                    vm.currentProgress = duration.hours();
                    vm.currentText = duration.hours() > 1 ? 'hours' : 'hour';
                }

                // Show minutes
                else if(duration.minutes() > 0) {
                    vm.maxProgress = 59;
                    vm.currentProgress = duration.minutes();
                    vm.currentText = duration.minutes() > 1 ? 'minutes' : 'minute';
                }

                // Show seconds
                else if(duration.seconds() > 0) {
                    vm.maxProgress = 59;
                    vm.currentProgress = duration.seconds();
                    vm.currentText = duration.seconds() > 1 ? 'seconds' : 'second';
                }
                else {
                    vm.maxProgress = 1;
                    vm.currentProgress = 1;
                    vm.currentText = '-';
                }
            }, 1000);
        }
    }

})();
