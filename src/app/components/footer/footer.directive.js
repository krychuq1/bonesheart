(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .directive('pageFooter', slFooter);

    /** @ngInject */
    function slFooter() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/footer/footer.html',
            scope: {
            },
            controller: FooterController,
            controllerAs: 'vm',
            bindToController: true,
            replace: true
        };

        return directive;

        /** @ngInject */
        function FooterController($log) {
            var vm = this;

            vm.chat = chat;

            function chat() {
                $log.log('Chat button clicked');
            }
        }
    }

})();
