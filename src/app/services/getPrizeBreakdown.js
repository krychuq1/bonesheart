(function() {

    angular
        .module('sevenLottos')
        .service('$getPrizeBreakdown', getPrizeBreakdown);

    //get prizes bt draw ID
    function getPrizeBreakdown($http, $q, SingleLineProducts, $log) {
        var getPrizes = function(drawDate, drawId) {
            var deferred = $q.defer();
            var data = {
                "startDate": drawDate,
                "endDate": drawDate,
                "addAuth" : false
            };

            SingleLineProducts.drawDetails(drawDate, drawDate)
            .then(function (prizes) {
                //find the prizes where draw id matches the one we are looking for
                var prizeForDraw = prizes.filter(function(obj) {
                    return obj.drawingId == drawId;
                })[0];
                deferred.resolve(prizeForDraw); // resolve
            },function (error) {
                //console.log('error getting prizes ', error);
                deferred.reject(error); //reject
            });

            //promise
            return deferred.promise;
        };

        return {
            getPrizes: getPrizes
        };
    }

})();
