(function() {

  angular
    .module('sevenLottos')
    .service('$getLotteryHistory', getLotteryHistory);

  //get prizes bt draw ID
  function getLotteryHistory($http, $q, SingleLineProducts) {
    var getHistoryForLottery = function(name) {
      var deferred = $q.defer();
      SingleLineProducts.results().then(function(history) {
        //console.log('service ----- results', history);
        angular.forEach(history, function(data) {
          //console.log('arrays', data);
          var lotteryHistoryArray = data.filter(function(obj) {
            return obj.lotteryName == name;
          });
          //console.log(lotteryHistoryArray);
          if (lotteryHistoryArray.length > 0) {
            deferred.resolve(lotteryHistoryArray);
          }
        });
      });
      return deferred.promise;
    };

    var getBreakDown = function(name, drawDate) {
      var deferred = $q.defer();
      var endData = {
        history: [],
        index: ''
      };
      getHistoryForLottery(name).then(function(history) {
        //console.log('asdasdsadsdasdsadsasa-asd-s-s-das-dsa-d-asd-s', history, drawDate);
        endData.history = history;
        history.filter(function(obj,index) {
          if(obj.drawDateTime === drawDate){
            endData.index = index;
          }
        });
        deferred.resolve(endData);
      });
      return deferred.promise;
    };

    return {
      getHistoryForLottery: getHistoryForLottery,
      getBreakDown: getBreakDown
    };
  }
})();
