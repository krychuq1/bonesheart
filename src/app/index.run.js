(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $rootScope, $mdSidenav, tmhDynamicLocale, $iwSession) {
        $log.debug('runBlock end');

        // tmhDynamicLocale.set('da-dk');
        //tmhDynamicLocale.set($iwSession.getLanguage());
        //$log.log( $iwSession.getSession() );

        $rootScope.$on('$stateChangeSuccess', function() {
            $mdSidenav('mobile-payslip').close();
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            //$document.find('body').eq(0).scrollTop = $document.documentElement.scrollTop = 0;
        });
    }

})();
