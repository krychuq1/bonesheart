(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/pages/home/home.html',
                controller: 'HomePageController',
                controllerAs: 'homePage'
            })
            .state('play', {
                url: '/play-lottery/:productId',
                templateUrl: 'app/pages/play/play.html',
                controller: 'PlayPageController',
                controllerAs: 'playPage'
            })
            .state('results', {
                url: '/lottery-results',
                templateUrl: 'app/pages/results/results.html',
                controller: 'ResultsPageController',
                controllerAs: 'ResultsPage'
            })
            .state('lotteryresults', {
                url: '/lottery-results/:name',
                templateUrl: 'app/pages/lotteryHsitoryResult/lottery-results.html',
                controller: 'LotteryHistoryResult',
                params: {name: null, history : null },
                controllerAs: 'lotteryResult'
            })
            .state('prizebreakdown', {
                url: '/lottery-results/:name/:drawDate',
                templateUrl: 'app/pages/lotteryDrawPrizebreakdown/prize-breakdown.html',
                params: {name: null, drawDate: null,index:null,history:null},
                controller: 'LotteryPrizeBreakdown',
                controllerAs: 'prizeCtrl'
            })
            .state('how', {
                url: '/how-to-play',
                templateUrl: 'app/pages/how/how.html',
                controller: 'HowPageController',
                controllerAs: 'howPage'
            })
            .state('contact', {
                url: '/contact-us',
                templateUrl: 'app/pages/contact/contact.html',
                controller: 'ContactPageController',
                controllerAs: 'contactPage'
            })
            .state('account', {
                url: '/account',
                templateUrl: 'app/pages/account/account.html',
                controller: 'AccountPageController',
                controllerAs: 'AccountPage'
            })
            .state('checkout', {
                url: '/checkout?txnId',
                templateUrl: 'app/pages/checkout/checkout.html',
                controller: 'CheckoutPageController',
                controllerAs: 'vm'
            });

        $urlRouterProvider.otherwise('/');
    }

})();
