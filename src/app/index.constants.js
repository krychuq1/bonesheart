/* global moment:false */
(function() {
    'use strict';

    angular
        .module('sevenLottos')
        .constant('moment', moment)
        .constant('APP_CONFIG', {
            domain: '7lottos.com',
            brand: '7lottos',
            frontEnd: '7lottos',
            frontEndId: 1,
            defaultLanguage: 'en',
            defaultCurrency: 'EUR'
        });

})();
